var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
const isEmpty = require("is-empty");
var url = "/admin_panel";

router.use(async function (req, res, next) {
    const token = req.session.bh_token;

    if (token) {
        jwt.verify(token, global.config.secretKey,
            {
                algorithm: global.config.algorithm

            }, async function (err, decoded) {
                if (err) {
                    let errordata = {
                        message: err.message,
                        expiredAt: err.expiredAt
                    };

                    req.session.destroy(() => {
                        console.log("Session distroyed");
                    });

                    return res.render('login', {
                        "page": "login",
                        "success": false,
                        "message": "Timeout Login Fast"
                    });
                }
                req.decoded = decoded;
                next();
            });
    } else {
        res.redirect(url + '/login');
    }
});

module.exports = router;