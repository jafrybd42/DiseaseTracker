var express = require('express');
var router = express.Router();

router.use(function (req, res, next) {

    const userData = req.decoded;
    // console.log(userData);

    if (userData === undefined || userData.role_name == "Team Member" || userData.role_id == 2) {
        
        return res.send({
            "success": false,
            "message": "You are not eligible on this route."
        });
    } else next();
});

module.exports = router;