var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
const isEmpty = require("is-empty");
const organization_model = require('../../../model/organization');
const user_model = require('../../../model/user');
const common_model = require('../../../common');

router.use(async function (req, res, next) {
    const token = req.headers['x-access-token'];
    
    if (token) {
        jwt.verify(token, global.config.secretKey,
            {
                algorithm: global.config.algorithm

            }, async function (err, decoded) {
                if (err) {
                    let errordata = {
                        message: err.message,
                        expiredAt: err.expiredAt
                    };

                    return res.send({
                        "success": false,
                        "message": "Timeout Login Fast"
                    });
                }


                if (decoded.role_id == 1) {
                    decoded.role = "MT Admin";
                } else {
                    // Check user profile & organization
                    // Get organization info
                    const organization_id = decoded.organization_id;
                    let organizationInfo = await organization_model.getOrganizationByID(organization_id);

                    if (isEmpty(organizationInfo)) {
                        return res.send({
                            "success": true,
                            "message": "You can't login, as unknown organization."
                        });
                    } else if (organizationInfo[0].status == 1) {
                        return res.send({
                            "success": true,
                            "message": "You can't login, as organization is delete."
                        });
                    }


                    let userData = await user_model.getUserById(decoded.id);

                    if (isEmpty(userData)) {
                        return res.send({
                            "success": false,
                            "message": "Unknown Account."
                        });
                    } else if (userData[0].status == 1) {
                        return res.send({
                            "success": true,
                            "message": "Your account is Delete. You can't access this System."
                        });
                    } else if (userData[0].status == 2) {
                        return res.send({
                            "success": true,
                            "message": "Your account is postponed. You can't access this System."
                        });
                    }
                }

                // Get Member Ship Packages
                let memberShipInfo = await common_model.getOrganizationMemberShipPlan(decoded.organization_id);
                // Check Member Ship plan
                await common_model.updateAllAccountUsingMemberShipPolicy(decoded.organization_id, memberShipInfo.runing_member_ship_package);
                req.decoded = decoded;
                req.decoded.memberShipInfo = memberShipInfo;
                next();
            });
    } else {
        return res.send({
            "success": false,
            "message": "Unauthorize Request"
        });
    }
});

module.exports = router;