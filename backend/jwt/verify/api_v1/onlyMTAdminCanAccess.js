var express = require('express');
var router = express.Router();



router.use(function (req, res, next) {

    const userData = req.decoded;

    if (userData === undefined || userData.role_id == 1) {

        next()
        
    } else{
        return res.send({
            "success": false,
            "message": "You are not eligible on this route."
        });
    } ;
});

module.exports = router;