var fs = require('fs');
const isEmpty = require('is-empty');
const moment = require("moment");
const enroll_membership_model = require('./model/enroll_membership');
const package_model = require('./model/package');
const role_model = require('./model/role');
const user_model = require('./model/user');

let getRendomData = async (min, max) => {
    if (max == 0) return 0;

    max = max + 1;
    return Math.floor(Math.random() * (max - min) + min);
}

let deleteFile = async (path, fileName) => {
    try {
        fs.unlinkSync(path + fileName);
        return { success: true, message: "file Delete" };

    } catch (err) {
        console.error(err)
        return { success: false, message: "file Delete Fail" };
    }
}

let getRandomDate = async (date1, date2) => {
    function randomValueBetween(min, max) {
        return Math.random() * (max - min) + min;
    }
    var date1 = date1 || '01-01-1970'
    var date2 = date2 || new Date().toLocaleDateString()
    date1 = new Date(date1).getTime()
    date2 = new Date(date2).getTime()
    if (date1 > date2) {
        date1 = new Date(randomValueBetween(date2, date1)).toLocaleDateString()
    } else {
        date1 = new Date(randomValueBetween(date1, date2)).toLocaleDateString()
    }

    // console.log(date1)
    date1 = (new Date(date1))

    return date1.getFullYear() + '-' +
        ('00' + (date1.getMonth() + 1)).slice(-2) + '-' +
        ('00' + date1.getDate()).slice(-2);
}

let getTodayDateTime = async () => {
    let date = new Date(new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Dhaka'
    }));

    let toDayDateWithTime = date.getFullYear() + '-' +
        ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
        ('00' + (date.getDate())).slice(-2) + ' ' +
        ('00' + date.getHours()).slice(-2) + ':' +
        ('00' + date.getMinutes()).slice(-2) + ':' +
        ('00' + date.getSeconds()).slice(-2);

    return toDayDateWithTime;
}


let addFiveMinutes = async ( ) => {
    
    let date = new Date(new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Dhaka'
    }));
    var minutesToAdd=30;

    let addFiveMinutes = date.getFullYear() + '-' +
        ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
        ('00' + (date.getDate())).slice(-2) + ' ' +
        ('00' + date.getHours()).slice(-2) + ':' +
        ('00' + date.getMinutes()).slice(-2)  + ':' +
        ('00' + date.getSeconds()).slice(-2);

    return addFiveMinutes;
}

let getTodayDate = async () => {
    let date = new Date(new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Dhaka'
    }));

    let toDayDate = date.getFullYear() + '-' +
        ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
        ('00' + (date.getDate())).slice(-2);

    return toDayDate;
}

let getCustomDate = async (date = "20/12/2012", extraDay = 0, extraMonth = 0, extraYear = 0) => {
    try {
        let customDate = new Date(date);
        customDate.setDate(customDate.getDate() + (extraDay));
        customDate.setMonth(customDate.getMonth() + (extraMonth));
        customDate.setFullYear(customDate.getFullYear() + (extraYear));

        date = customDate.getFullYear() + '-' +
            ('00' + (customDate.getMonth() + 1)).slice(-2) + '-' +
            ('00' + (customDate.getDate())).slice(-2);
        return date;

    } catch (error) {
        return await getTodayDate();
    }
}

let getMonthLastDate = async (date = "2012-12-12") => { // give a date and generate last date of this month
    try {
        let customDate = new Date(date);
        customDate.setDate(1);
        customDate.setMonth(customDate.getMonth() + 1);
        customDate.setDate(customDate.getDate() - 1);

        date = customDate.getFullYear() + '-' +
            ('00' + (customDate.getMonth() + 1)).slice(-2) + '-' +
            ('00' + (customDate.getDate())).slice(-2);

        return date;

    } catch (error) {
        return await getTodayDate();
    }
}

let compareTwoDate = async (date1 = "2012-12-12", date2 = "2012-12-1", want_true_false = true) => { // give if true if last day is getter then or equal 2
    try {
        const fast_date = moment(await getCustomDate(date1)); // formate the data and convert ISO Formate
        const last_date = moment(await getCustomDate(date2));
        const duration = moment.duration(fast_date.diff(last_date));
        const days = duration.asDays();
        // console.log(days)
        if (want_true_false === true) {
            return days > -1 ? true : false;
        } else {
            return days;
        }
    } catch (error) {
        return 0;
    }
}

let getNextMemberShipEndDate = async (date = "2012-1-27") => {
    let givenDate;
    let customDate = new Date();
    if (date == undefined || date == null) givenDate = new Date();
    else givenDate = new Date(await getCustomDate(date));

    try {

        if (customDate.getMonth() == givenDate.getMonth()) {
            customDate.setMonth(givenDate.getMonth() + 1);
        }

        customDate.setDate(givenDate.getDate());

        date = customDate.getFullYear() + '-' +
            ('00' + (customDate.getMonth() + 1)).slice(-2) + '-' +
            ('00' + (customDate.getDate())).slice(-2);
        return date;

    } catch (error) {
        return await getTodayDate();
    }
}

let getOrganizationMemberShipPlan = async (organization_id) => {
    // return 0;
    let nowEnroleMemberShip = await enroll_membership_model.getInrollMember_ShipByOnganizationId(organization_id);
    let getLowestPackage = await package_model.getLowestPackage();

    if (isEmpty(getLowestPackage)) {
        return { "next_member_ship_package": {}, "runing_member_ship_package": {} };
    }

    if (isEmpty(nowEnroleMemberShip)) {
        return {
            "next_member_ship_package": getLowestPackage[0],
            "runing_member_ship_package": getLowestPackage[0]
        }

    } else if (nowEnroleMemberShip.length == 1) {
        let nowRunningMemberShip = await package_model.getPackageByID(nowEnroleMemberShip[0].package_id);

        return {
            "next_member_ship_package": isEmpty(nowRunningMemberShip) ? getLowestPackage[0] : nowRunningMemberShip[0],
            "runing_member_ship_package": isEmpty(nowRunningMemberShip) ? getLowestPackage[0] : nowRunningMemberShip[0]
        }

    } else {
        let nowRunningMemberShip = await package_model.getPackageByID(nowEnroleMemberShip[0].package_id);
        let nextMemberShip = await package_model.getPackageByID(nowEnroleMemberShip[1].package_id);
        let today = await getTodayDate();

        if (await compareTwoDate(today, nowEnroleMemberShip[0].end_date)) {
            let updateNowEnrollData = {
                "status": 1,
                "update_at": await getTodayDateTime()
            }
            result = await enroll_membership_model.updateMembershipInfoByID(nowEnroleMemberShip[0].id, updateNowEnrollData);
            nowRunningMemberShip = nextMemberShip;
        }

        return {
            "next_member_ship_package": isEmpty(nextMemberShip) ? getLowestPackage[0] : { ...nextMemberShip[0] },
            "runing_member_ship_package": isEmpty(nowRunningMemberShip) ? getLowestPackage[0] : { ...nowRunningMemberShip[0] }
        }
    }
}

let updateAllAccountUsingMemberShipPolicy = async (organization_id, memberShipDetails) => {


    async function checkAndUpdateUserIdWithType(organization_id = 0, role_id = 0, number_of_user_permision = 0, active_number_of_user = 0, postpone_number_of_user = 0) {
        if (number_of_user_permision === -1) {  // -1 make all suspend accout on
            await user_model.updateUserStatusByRole_IdAndOnganization_Id(0, role_id, organization_id);

        } else if (number_of_user_permision < active_number_of_user) { // postpone all extra account

            let userListResult = await user_model.getUserListByRoleTypeStatusAndOrganizationId(role_id, 0, organization_id);
            for (let i = number_of_user_permision; i < userListResult.length; i++) {
                await user_model.updateUserStatusByIDAndOnganizationId(2, userListResult[i].id, organization_id);
            }
        } else if (number_of_user_permision > active_number_of_user) { // on posposd account which is limited in package
            let userListResult = await user_model.getUserListByRoleTypeStatusAndOrganizationId(role_id, 2, organization_id);
            let maxActiveAccount = number_of_user_permision - active_number_of_user;
            for (let i = 0; i < userListResult.length && maxActiveAccount > 0; i++, maxActiveAccount--) {
                await user_model.updateUserStatusByIDAndOnganizationId(0, userListResult[i].id, organization_id);
            }
        }
    }

    let organization_active_user_list_count = await role_model.getRoleWiseActiveUserCountByOrganizationId(organization_id);
    let organization_sospond_user_list_count = await role_model.getRoleWisepostponeUserCountByOrganizationId(organization_id);

    let active_user_count = {
        number_of_team_member: 0,
        number_of_management: 0,
        number_of_participant: 0,
        number_of_admin: 0
    }

    let postpone_user_count = {
        number_of_team_member: 0,
        number_of_management: 0,
        number_of_participant: 0,
        number_of_admin: 0
    }

    for (let i = 0; i < organization_active_user_list_count.length; i++) {
        if (organization_active_user_list_count[i].roles_id == 2) {
            active_user_count.number_of_team_member = organization_active_user_list_count[i].total_user;
        } else if (organization_active_user_list_count[i].roles_id == 3) {
            active_user_count.number_of_management = organization_active_user_list_count[i].total_user;
        } else if (organization_active_user_list_count[i].roles_id == 4) {
            active_user_count.number_of_participant = organization_active_user_list_count[i].total_user;
        } else if (organization_active_user_list_count[i].roles_id == 5) {
            active_user_count.number_of_admin = organization_active_user_list_count[i].total_user;
        }
    }

    for (let i = 0; i < organization_sospond_user_list_count.length; i++) {
        if (organization_sospond_user_list_count[i].roles_id == 2) {
            postpone_user_count.number_of_team_member = organization_sospond_user_list_count[i].total_user;
        } else if (organization_sospond_user_list_count[i].roles_id == 3) {
            postpone_user_count.number_of_management = organization_sospond_user_list_count[i].total_user;
        } else if (organization_sospond_user_list_count[i].roles_id == 4) {
            postpone_user_count.number_of_participant = organization_sospond_user_list_count[i].total_user;
        } else if (organization_sospond_user_list_count[i].roles_id == 5) {
            postpone_user_count.number_of_admin = organization_sospond_user_list_count[i].total_user;
        }
    }

    await checkAndUpdateUserIdWithType(2, 2, memberShipDetails.number_of_team_member, active_user_count.number_of_team_member,
        postpone_user_count.number_of_team_member);

    await checkAndUpdateUserIdWithType(2, 3, memberShipDetails.number_of_management, active_user_count.number_of_management,
        postpone_user_count.number_of_management);

    await checkAndUpdateUserIdWithType(2, 4, memberShipDetails.number_of_participant, active_user_count.number_of_participant,
        postpone_user_count.number_of_participant);

    await checkAndUpdateUserIdWithType(2, 5, memberShipDetails.number_of_admin, active_user_count.number_of_admin,
        postpone_user_count.number_of_admin);


}




module.exports = {
    getRendomData,
    deleteFile,
    getRandomDate,
    getTodayDateTime,
    getTodayDate,
    getCustomDate,
    addFiveMinutes,
    getMonthLastDate,
    compareTwoDate,
    getNextMemberShipEndDate,
    getOrganizationMemberShipPlan,
    updateAllAccountUsingMemberShipPolicy
}