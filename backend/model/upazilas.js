const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');



let getUpazilasByName = async (upazila_name = "") => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUpazilasByName(), [upazila_name,upazila_name], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUpazilasList = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUpazilasList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUpazilasByDistrictId = async (district_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUpazilasByDistrictId(), [district_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUpazilasByIDAndDistrictId = async (id = 0, district_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUpazilasByIDAndDistrictId(), [id, district_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getUpazilasByDisID = async (district_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUpazilasByDisID(district_id), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getUpazilasByName,
    getUpazilasByDistrictId,
    getUpazilasByDisID,
    getUpazilasList,
    getUpazilasByIDAndDistrictId
}