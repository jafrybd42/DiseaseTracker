const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');



let getAdminByEmail = async (email = "") => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getAdminByEmail(), [email], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewAdmin = async (info = {}) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.addNewAdmin(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getAdminByEmail,
    addNewAdmin
}