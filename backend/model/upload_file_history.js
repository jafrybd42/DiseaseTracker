const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


// Promices Method

let addNew = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.addBulkFileUploadHistory(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}
let getAllFilesByOrganizationId = async (organization_id) => { 
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getAllFilesByOrganizationId(), [organization_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getAllFilesByUserIDAndOrganizationId = async (organization_id,user_id) => { 
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getAllFilesByUserIDAndOrganizationId(), [organization_id,user_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}




module.exports = {
    addNew,
    getAllFilesByOrganizationId,
    getAllFilesByUserIDAndOrganizationId
}