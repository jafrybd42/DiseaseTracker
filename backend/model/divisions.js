const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


let getDivisionByID = async (division_id = 0) => {
    
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDivisionByID(), [division_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getDivisionByName = async (disease_name = "") => {
    // console.log(disease_name);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDivisionByName(), [disease_name, disease_name], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDivisionList = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDivisionList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getDivisionByID,
    getDivisionByName,
    getDivisionList
}

//Chattagram, Barisal
