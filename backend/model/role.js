const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


// Promices Method

let getRoleById = async (id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getRoleById(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getRoleByIdAndOrganizationId = async (id = 0, organization_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getRoleByIdAndOrganizationId(), [id, organization_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getRoleWiseActiveUserCountByOrganizationId = async (organization_id = 0, status = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getRoleWiseUserCountByOrganizationIdAndStatus(), [organization_id, status], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getRoleWisepostponeUserCountByOrganizationId = async (organization_id = 0, status = 2) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getRoleWiseUserCountByOrganizationIdAndStatus(), [organization_id, status], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

module.exports = {
    getRoleById,
    getRoleByIdAndOrganizationId,
    getRoleWiseActiveUserCountByOrganizationId,
    getRoleWisepostponeUserCountByOrganizationId
}