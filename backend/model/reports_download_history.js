const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


// Promices Method

let addNew = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.addNewReportsDownloadHistory(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}
let getTotalReportsDownloadHistoryByOgranizationIdAndDate = async (organization_id, start_date, end_date) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getTotalReportsDownloadHistoryByOgranizationIdAndDate(), [organization_id, start_date, end_date], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


module.exports = {
    addNew,
    getTotalReportsDownloadHistoryByOgranizationIdAndDate
}