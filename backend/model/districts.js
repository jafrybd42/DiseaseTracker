const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');

let getDistrictByID = async (district_id = 0) => {
    
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDistrictByID(), [district_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDistrictByName = async (disease_name = "") => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDistrictByName(), [disease_name, disease_name], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDistrictList = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDistrictList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDistrictByDivisionID = async (division_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDistrictByDivisionID(), [division_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDistrictByIDAndDivisionID = async (id = 0, division_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDistrictByIDAndDivisionID(), [id, division_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getDisByDivID = async (division_id = []) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDisByDivID(division_id), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getDistrictByID,
    getDistrictByName,
    getDistrictList,
    getDistrictByDivisionID,
    getDisByDivID,
    getDistrictByIDAndDivisionID
}