const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


// Promices Method

let getUserByPhoneOrEmail = async (phone = "") => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUserByPhoneOrEmail(), [phone, phone], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserByEmail = async (email) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUserByEmail(), [email], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let addNewUser = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.registerUserAccount(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserById = async (id = 0) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUserById(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserByIdAndOnganizationId = async (id = 0, organization_id = 0) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUserByIdAndOnganizationId(), [id, organization_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserListByRoleType = async (role_id = 0) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUserListByRoleType(), [role_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserListByRoleTypeStatusAndOrganizationId = async (role_id = 0, status = 0, organization = 0) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUserListByRoleTypeStatusAndOrganizationId(), [role_id, status, organization], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getUserListByRoleTypeAndOrganizationId = async (role_id = 0, organization = 0) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUserListByRoleTypeAndOrganizationId(), [role_id, organization], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateUserStatusByIDAndOnganizationId = async (status = 1, id = 0, organizationId = 0) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updateUserStatusByIDAndOnganizationId(), [status, id , organizationId], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateUserStatusByRole_IdAndOnganization_Id = async (status = 1, role_id = 0, organizationId = 0) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updateUserStatusByRole_IdAndOnganization_Id(), [status, role_id , organizationId], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateUserStatusByID = async (status = 1, id = 0) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updateUserStatusByID(), [status, id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getUserList = async () => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUserList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let updateProfileByID = async (id,name,organization_id) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updateProfileByID(), [name,id,organization_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateUser_PasswordByIDAndOrganizationId = async (id, password, organization_id) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updateUser_PasswordByIDAndOrganizationId(), [password, id, organization_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let updateTokenAndExpiredTime = async (id,user_id,resetToken) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updateTokenAndExpiredTime(), [resetToken,user_id,id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let updateUser_PasswordByIDAndToken = async (id,token) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updateUser_PasswordByIDAndToken(), [token , id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getUserByToken = async (token) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUserByToken(), [token], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let insertTokenAndExpiredTime = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.insertTokenAndExpiredTime(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}





let getUserByOTP = async (otp) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getUserByOTP(), [otp], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let insertOTPAndExpiredTime = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.insertOTPAndExpiredTime(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateOTPAndExpiredTime = async (id,user_id,otp) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updateOTPAndExpiredTime(), [otp,user_id,id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getUserByPhoneOrEmail,
    getUserByEmail,
    addNewUser,
    getUserById,
    getUserListByRoleType,
    updateUserStatusByIDAndOnganizationId,
    updateUserStatusByID,
    getUserByIdAndOnganizationId,
    getUserListByRoleTypeStatusAndOrganizationId,
    getUserListByRoleTypeAndOrganizationId,
    getUserList,
    updateProfileByID,
    updateUser_PasswordByIDAndOrganizationId,
    updateUserStatusByRole_IdAndOnganization_Id,
    updateTokenAndExpiredTime,
    updateUser_PasswordByIDAndToken,
    getUserByToken,
    insertTokenAndExpiredTime,
    getUserByOTP,
    insertOTPAndExpiredTime,
    updateOTPAndExpiredTime
}

