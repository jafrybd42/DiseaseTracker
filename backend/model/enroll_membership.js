const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


// Promices Method

let getInrollMember_ShipByOnganizationId = async (organization_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getInrollMember_ShipByOnganizationId(), organization_id, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let addNewMembership = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.addNewMembership(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let deleteMembershipById = async (id) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.deleteMembershipById(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let updateMembershipInfoByID = async (id, update_data) => {
    let keys = Object.keys(update_data);
    let dataParametter = [];

    for (let index = 0; index < keys.length; index++) {
        dataParametter.push(update_data[keys[index]]);
    }

    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updateMemberShipID(update_data), [...dataParametter, id], function (error, results, fields) {
            if (error) reject(error)
            else resolve(results)
        });
    });
}

let updatePreviousMembershipAddNewMembership = async (id, update_data, new_data) => {
    let keys = Object.keys(update_data);
    let dataParametter = [];

    for (let index = 0; index < keys.length; index++) {
        dataParametter.push(update_data[keys[index]]);
    }

    return new Promise((resolve, reject) => {

        connectionBHM.beginTransaction(function (err) {
            if (err) { throw err; }

            connectionBHM.query(quaries.updateMemberShipID(update_data), [...dataParametter, id], function (error, results, fields) {
                if (error) {
                    return connectionBHM.rollback(function () {
                        // console.trace("Error");
                        // reject(error)
                        resolve([]);
                    });
                }

                connectionBHM.query(quaries.addNewMembership(), new_data, function (error, results, fields) {
                    if (error) {
                        return connectionBHM.rollback(function () {
                            // console.trace("Error");
                            resolve([]);
                        });
                    }
                    connectionBHM.commit(function (err) {
                        if (err) {
                            return connectionBHM.rollback(function () {
                                // console.trace("Error");
                                resolve([]);
                            });
                        }
                        resolve(results);
                    });
                });
            });
        });

    });
}





module.exports = {
    getInrollMember_ShipByOnganizationId,
    addNewMembership,
    updatePreviousMembershipAddNewMembership,
    updateMembershipInfoByID,
    deleteMembershipById
}