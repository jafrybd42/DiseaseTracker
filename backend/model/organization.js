const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


// Promices Method

let getOrganizationList = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getOrganizationList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getAllOrganizationList = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getAllOrganizationList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getOrganizationByID = async (organization_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getOrganizationByID(),[organization_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDeactiveOrganizationList = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDeactiveOrganizationList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getActiveOrganizationList = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getActiveOrganizationList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getOrganizationByName = async (name) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getOrganizationByName(), [name], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewOrganization = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.addNewOrganization(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let updateOrganizationByID = async (id, name,updated_at) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updateOrganizationByID(), [name,updated_at, id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}



let deactiveOrganization = async (id, updated_at) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.deactiveOrganization(), [updated_at,id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let activateOrganization = async (id, updated_at) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.activateOrganization(), [updated_at,id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}









module.exports = {
    getOrganizationList,
    getAllOrganizationList,
    getOrganizationByID,
    getDeactiveOrganizationList,
    getActiveOrganizationList,
    getOrganizationByName,
    addNewOrganization,
    updateOrganizationByID,
    deactiveOrganization,
    activateOrganization
    
}