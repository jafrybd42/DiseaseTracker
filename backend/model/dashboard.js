const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


let getDataForDashboard = async (searchFieldObject = {}) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDataForDashboard(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}


module.exports = {
    getDataForDashboard
}