const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


let addNew = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.addHealthData(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDataBySearching = async (searchFieldObject = {}) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDataBySearching(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}

/// AGE

let getMaxMinAgeBySelectingDiseaseID = async (disease_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getMaxMinAgeBySelectingDiseaseID(), [disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}

let getMaxMinAgeBySelectingDiseaseIDandDivisionID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getMaxMinAgeBySelectingDiseaseIDandDivisionID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}

let getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}


let getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}

/// Gender

let getGenderBySelectingDiseaseID = async (disease_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getGenderBySelectingDiseaseID(), [disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}

let getGenderBySelectingDiseaseIDandDivisionID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getGenderBySelectingDiseaseIDandDivisionID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}


let getGenderBySelectingDiseaseIDandDivisionIDandDistrictID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getGenderBySelectingDiseaseIDandDivisionIDandDistrictID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}

let getGenderBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getGenderBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}






/// Season

let getSeasonBySelectingDiseaseID = async (disease_id) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getSeasonBySelectingDiseaseID(), [disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}

let getSeasonBySelectingDiseaseIDandDivisionID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getSeasonBySelectingDiseaseIDandDivisionID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}


let getSeasonBySelectingDiseaseIDandDivisionIDandDistrictID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getSeasonBySelectingDiseaseIDandDivisionIDandDistrictID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}


let getSeasonBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getSeasonBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}


/// Age Gender Chart



let getAgeGenderChartBySelectingDiseaseID = async (disease_id) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getAgeGenderChartBySelectingDiseaseID(), [disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}

let getAgeGenderChartBySelectingDiseaseIDandDivisionID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getAgeGenderChartBySelectingDiseaseIDandDivisionID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}



let getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}



let getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}


/// Affected People on Current Year - Showing Month Wise Data



let getMonthlyDataBySelectingDiseaseID = async (disease_id) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getMonthlyDataBySelectingDiseaseID(), [disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}



let getMonthlyDataBySelectingDiseaseIDandDivisionID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getMonthlyDataBySelectingDiseaseIDandDivisionID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}



let getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}



let getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID = async (searchFieldObject = {}) => {
    //console.log(searchFieldObject);
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID(searchFieldObject), [searchFieldObject.disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });

}


module.exports = {
    getDataBySearching,
    addNew,

    /// Age

    getMaxMinAgeBySelectingDiseaseID,
    getMaxMinAgeBySelectingDiseaseIDandDivisionID,
    getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictID,
    getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID,

    /// Gender

    getGenderBySelectingDiseaseID,
    getGenderBySelectingDiseaseIDandDivisionID,
    getGenderBySelectingDiseaseIDandDivisionIDandDistrictID,
    getGenderBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID,

    /// Season 

    getSeasonBySelectingDiseaseID,
    getSeasonBySelectingDiseaseIDandDivisionID,
    getSeasonBySelectingDiseaseIDandDivisionIDandDistrictID,
    getSeasonBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID,


    /// Age Gender Chart

    getAgeGenderChartBySelectingDiseaseID,
    getAgeGenderChartBySelectingDiseaseIDandDivisionID,
    getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictID,
    getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID,
    


    /// Affected People on Current Year - Showing Month Wise Data

    getMonthlyDataBySelectingDiseaseID,
    getMonthlyDataBySelectingDiseaseIDandDivisionID,
    getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictID,
    getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID
}