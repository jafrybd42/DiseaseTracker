const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


let getDiseagesByID = async (disease_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDiseagesByID(), [disease_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDiseagesByName = async (disease_name = "") => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDiseagesByName(), [disease_name], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getDiseagesList = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getDiseagesList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let topTenDisease = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.topTenDisease(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let totalDiseasePeople = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.totalDiseasePeople(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



module.exports = {
    getDiseagesByID,
    getDiseagesByName,
    getDiseagesList,
    topTenDisease,
    totalDiseasePeople
}