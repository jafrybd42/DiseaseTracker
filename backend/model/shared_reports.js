const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


// Promices Method

let addNew = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.addSharedNewReport(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getReportListByUserID = async (organization_id,role_type,user_id) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getReportListByUserID(), [organization_id,role_type,user_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getSharedResultByReportId = async (report_id) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getSharedResultByReportId(), [report_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

module.exports = {
    addNew,
    getReportListByUserID,
    getSharedResultByReportId
}