const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


// Promices Method

let addNew = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.addNewReport(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getReportById = async (reportId,organization_id,role_type,user_id) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getReportById(), [reportId,organization_id,role_type,user_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let getReportByCreatedBy = async (user_id) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getReportByCreatedBy(), [user_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

module.exports = {
    addNew,
    getReportById,
    getReportByCreatedBy
}