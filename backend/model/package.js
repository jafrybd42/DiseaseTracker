const connectionBHM = require('../connection/connection').connectionBHM;
const quaries = require('../query/query');


// Promices Method

let getPackageList = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getPackageList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getLowestPackage = async () => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getLowestPackage(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getPackageByID = async (organization_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getPackageByID(),[organization_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getPackageByTitle = async (title) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.getPackageByTitle(), [title], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewPackage = async (info) => {
    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.addNewPackage(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let updatePackageByID = async (id, data) => {
    let keys = Object.keys(data);
    let dataParametter = [];

    for (let index = 0; index < keys.length; index++) {
        dataParametter.push(data[keys[index]]);
    }

    return new Promise((resolve, reject) => {
        connectionBHM.query(quaries.updatePackageByID(data), [ ...dataParametter, id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}


module.exports = {
    updatePackageByID,
    addNewPackage,
    getPackageByTitle,
    getPackageByID,
    getPackageList,
    getLowestPackage
}