const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const user_model = require('../../model/user');
const role_model = require('../../model/role');
const organization_model = require('../../model/organization');
const verifyToken = require('../../jwt/verify/api_v1/verifyToken');
const onlyMTAdminCanAccess = require('../../jwt/verify/api_v1/onlyMTAdminCanAccess');


// router.get('/active_list', [verifyToken], async (req, res) => {
//     let admin_id = 5; // admin id 5 is fixed;
//     let result = await user_model.getUserListByRoleType(admin_id);

//     return res.send({
//         "success": true,
//         "message": "Admin List.",
//         "data": result
//     });
// });

// router.get('/active_list', [verifyToken], async (req, res) => {
//     let admin_id = 5; // admin id 5 is fixed;
//     let result = await user_model.getUserListByRoleType(admin_id);

//     return res.send({
//         "success": true,
//         "message": "Admin List.",
//         "data": result
//     });
// });

// router.get('/list', [verifyToken], async (req, res) => {
//     let admin_id = 5; // admin id 5 is fixed;
//     let result = await user_model.getUserListByRoleType(admin_id);

//     return res.send({
//         "success": true,
//         "message": "Admin List.",
//         "data": result
//     });
// });


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

module.exports = router;