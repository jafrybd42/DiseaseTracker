const express = require("express");
const router = express.Router();
const isEmpty = require("is-empty");
const disease_model = require('../../model/diseases');
const division_model = require('../../model/divisions');
const district_model = require('../../model/districts');
const upazila_model = require('../../model/upazilas');
const dashboard_model = require('../../model/dashboard');
const verifyToken = require('../../jwt/verify/api_v1/verifyToken');
const onlyMTAdminCanAccess = require('../../jwt/verify/api_v1/onlyMTAdminCanAccess');



router.post('/dashboardData',[verifyToken], async (req, res) => {

    let searchField = {};

    if (isEmpty(req.body.disease_id) ) {
        return res.send({
            "success": false,
            "message": "At First You have to select a Disease "
        });
    }

    if (req.body.disease_id !== undefined && req.body.disease_id !== "") {
        searchField.disease_id = req.body.disease_id;
    }

     

    let diseaseInfo = await disease_model.getDiseagesByID(searchField.disease_id);

    if (isEmpty(diseaseInfo)) {
        return res.send({
            "success": false,
            "message": "Invalid Disease"
        });
    }


    if (req.body.division_id !== undefined && Array.isArray(req.body.division_id) && req.body.division_id.length > 0) {
        searchField.division_id = req.body.division_id;
    }

    if (req.body.district_id !== undefined && Array.isArray(req.body.district_id) && req.body.district_id.length > 0) {
        searchField.district_id = req.body.district_id;
    }

    if (req.body.upazila_id !== undefined && Array.isArray(req.body.upazila_id) && req.body.upazila_id.length > 0) {
        searchField.upazila_id = req.body.upazila_id;
    }

    if (req.body.season_name !== undefined && Array.isArray(req.body.season_name) && req.body.season_name.length > 0) {
        searchField.season_name = req.body.season_name;
    }



    /// Main Result

    let result = await dashboard_model.getDataForDashboard(searchField);
    return res.send({
        "success": true,
        "message": "Dashboard Data.",
        "count":result.length,
        "data": result
        
    })


});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});


module.exports = router;