const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const user_model = require('../../model/user');
const role_model = require('../../model/role');
const organization_model = require('../../model/organization');


router.post('/login', async (req, res) => {

    let loginData = {
        "password": req.body.password,
        "email": req.body.email
    }

    let errorMessage = "";
    let isError = 0;
    let tryToLoginBy = "";

    // Check Password Validation
    if (loginData.password == undefined || loginData.password.length < 6) {
        isError = 1;
        errorMessage += "Need valid password.";
    }

    // Check Email or phone number validation
    if (loginData.email === undefined || isEmpty(loginData.email)) {
        isError = 1;
        errorMessage += "Give valid phone number or email.";
    } else if (!isNaN(loginData.email)) {
        tryToLoginBy = "phone";
        if (loginData.email.length != 11) {
            isError = 1;
            errorMessage += "Give valid phone number or email.";
        }
    } else {
        const re = /\S+@\S+\.\S+/;
        tryToLoginBy = "email";
        if (!re.test(loginData.email)) {
            isError = 1;
            errorMessage += " Give a valid email.";
        }
    }

    if (isError == 1) {
        return res.send({
            "success": false,
            "message": errorMessage
        });
    }

    // Get User data from user table.
    let userData = await user_model.getUserByPhoneOrEmail(loginData.email);

    if (isEmpty(userData)) {
        return res.send({
            "success": false,
            "message": "No user found."
        });
    }

    const organization_id = userData[0].organization_id;

    // Check Password
    if (bcrypt.compareSync(loginData.password, userData[0].password)) {

        let profileData = {};

        //Check Role 
        let roleData = await role_model.getRoleById(userData[0].roles_id);

        if (isEmpty(roleData)) {
            return res.send({
                "success": false,
                "message": " Unknown User role.",
            });
        }

        let loginRole = roleData[0].use_for_login.split("-");

        let canLoging = false;

        for (let i = 0; i < loginRole.length; i++) {
            if (loginRole[i] === "all") {
                canLoging = true;
                break;
            } else if (loginRole[i] === tryToLoginBy) {
                canLoging = true;
                break;
            }
        }

        if (canLoging === false) {
            errorMessage = " You can loging this system using '";
            errorMessage += tryToLoginBy == "phone" ? "Phone no" : tryToLoginBy + "'.";
            return res.send({
                "success": false,
                "message": errorMessage
            });
        }

        // Get organization info
        let organizationInfo = await organization_model.getOrganizationByID(organization_id);

        if (isEmpty(organizationInfo)) {
            return res.send({
                "success": true,
                "message": "You can't login as unknown organization."
            });
        } else if (organizationInfo[0].status == 1) {
            return res.send({
                "success": true,
                "message": "You can't login as organization is deleted."
            });
        }


        // Generate profile data
        profileData.id = userData[0].id;
        profileData.email = userData[0].email;
        profileData.phone_number = userData[0].phone_number;
        profileData.role_id = roleData[0].id;
        profileData.role_name = roleData[0].name;
        profileData.permission = roleData[0].permission;
        profileData.organization_id = organizationInfo[0].id;
        profileData.organization_name = organizationInfo[0].name;

        //  "Generate Token"
        let token = jwt.sign(profileData, global.config.secretKey, {
            algorithm: global.config.algorithm,
            expiresIn: '1440m' // one day
        });


        profileData.token = token;

        return res.send({
            "success": true,
            "message": "Welcome to the system.",
            'data': profileData
        });

    } else {
        return res.send({
            "success": false,
            "message": "Wrong Password"
        });
    }

});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

module.exports = router;