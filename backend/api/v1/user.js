const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const user_model = require('../../model/user');
const role_model = require('../../model/role');
const organization_model = require('../../model/organization');
const verifyToken = require('../../jwt/verify/api_v1/verifyToken');
const onlyMTAdminCanAccess = require('../../jwt/verify/api_v1/onlyMTAdminCanAccess');
const common = require('../../common');


/// API KEY 
//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
const nodemailer = require('nodemailer')
const sendgridTransport = require('nodemailer-sendgrid-transport')
const crypto = require('crypto') // For Token

const transporter = nodemailer.createTransport(sendgridTransport({
    auth:{
        api_key:"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"
    }
}))



router.post('/registration', [verifyToken], async (req, res) => {

    let reqUserData = {
        "name": req.body.name,
        "organization_id": req.body.organization_id === undefined ? 0 : req.body.organization_id,
        "roles_id": req.body.roles_id === undefined ? 0 : req.body.roles_id,
        "email": req.body.email,
        "phone_number": req.body.phone_number,
        "password": "$2b$10$0ycKNk2E9HqNt/kdGiSF1OW4mb3qs.R4ugyaq/FjF9UIT8TZ8p0wW",
        "status": 0,
        "created_by": req.decoded.id
    }


    // Check who request MT Admin or other,
    // if other request , organization_id will set her/his organization_id
    if (!(req.decoded.role_id == 1 && req.decoded.hasOwnProperty('role'))) {
        reqUserData.organization_id = req.decoded.organization_id;
    }

    // Check organization
    let organizationInfo = await organization_model.getOrganizationByID(reqUserData.organization_id);

    if (isEmpty(organizationInfo)) {
        return res.send({
            "success": true,
            "message": "Unknown organization."
        });
    } else if (organizationInfo[0].status == 1) {
        return res.send({
            "success": true,
            "message": "Organization already deleted."
        });
    }

    //Check roles, and role loging method

    // let roleData = await role_model.getRoleByIdAndOrganizationId(reqUserData.roles_id, reqUserData.organization_id);
    let roleData = await role_model.getRoleById(reqUserData.roles_id);

    if (isEmpty(roleData)) {
        return res.send({
            "success": false,
            "message": "Unknown user role.",
        });
    }

    let organization_user_list_count = await role_model.getRoleWiseActiveUserCountByOrganizationId(reqUserData.organization_id);

    let already_regester_user = 0;
    for (let i = 0; i < organization_user_list_count.length; i++) {
        if (organization_user_list_count[i].roles_id == reqUserData.roles_id) {
            already_regester_user = organization_user_list_count[i].total_user;
        }
    }

    let registration_limit = 0;

    try {
        if (isEmpty(req.decoded.memberShipInfo.runing_member_ship_package)) {
            return res.send({
                "success": false,
                "message": "Please Fast Change your membership. Or Contract to Support Team.",
            });
        }
        if (reqUserData.roles_id == 2) {
            registration_limit = req.decoded.memberShipInfo.runing_member_ship_package.number_of_team_member;
        } else if (reqUserData.roles_id == 3) {
            registration_limit = req.decoded.memberShipInfo.runing_member_ship_package.number_of_management;
        } else if (reqUserData.roles_id == 4) {
            registration_limit = req.decoded.memberShipInfo.runing_member_ship_package.number_of_participant;
        } else if (reqUserData.roles_id == 5) {
            registration_limit = req.decoded.memberShipInfo.runing_member_ship_package.number_of_admin;
        }


    } catch (error) {
        return res.send({
            "success": false,
            "message": "Something wrong please try again.",
        });
    }

    if ( registration_limit <= already_regester_user && registration_limit != -1  ){
        return res.send({
            "success": false,
            "message": "You already cross the limit ("+ registration_limit +" account) for add "+ roleData[0].name + " accout. Please Update Membership for add more",
        });
    }


    if (roleData[0].name === "Admin" || roleData[0].name === "MT Admin") {
        if (req.decoded.role_id != 1) {
            return res.send({
                "success": false,
                "message": "Only have no permission to create Admin.",
            });
        }
    }

    let loginRole = roleData[0].use_for_login.split("-");
    let mustNeedEmail = 0;
    let mustNeedPhone = 0;
    let mustNeedAll = 0;

    for (let i = 0; i < loginRole.length; i++) {
        if (loginRole[i] === "all") {
            mustNeedAll = true;
            break;
        } else if (loginRole[i] === "email") {
            mustNeedEmail = 1;
        } else if (loginRole[i] === "phone") {
            mustNeedPhone = 1;
        }
    }


    let errorMessage = "";
    let isError = 0;

    if (reqUserData.name == undefined || reqUserData.name.length < 2) {
        isError = 1;
        errorMessage = "Need valid name and length should be greater than 2. ";
    }

    if (mustNeedAll === 1 || mustNeedEmail === 1) {
        const re = /\S+@\S+\.\S+/;
        if (!re.test(reqUserData.email)) {
            isError = 1;
            errorMessage += " Email is invalid.";
        } else {
            let existingUser = await user_model.getUserByPhoneOrEmail(reqUserData.email);

            if (!isEmpty(existingUser)) {
                isError = 1;
                errorMessage += " Email already in Use.";
            }
        }
    } else {
        reqUserData.email = "";
    }

    if (mustNeedAll === 1 || mustNeedPhone === 1) {
        if (reqUserData.phone_number == undefined || reqUserData.phone_number.length != 11) {
            isError = 1;
            errorMessage += " Phone number invalid.";
        } else {
            let existingUser = await user_model.getUserByPhoneOrEmail(reqUserData.phone_number);

            if (!isEmpty(existingUser)) {
                isError = 1;
                errorMessage += " Phone number already in use.";
            }
        }
    } else {
        reqUserData.phone_number = "";
    }

    if (mustNeedEmail == 0 && mustNeedPhone == 0 && mustNeedAll == 0) {
        isError = 1;
        errorMessage += " check Organization role .";
    }


    if (isError == 1) {
        return res.send({
            "success": false,
            "message": errorMessage
        });
    }

    // Add new user
    let result = await user_model.addNewUser(reqUserData);

    if (isEmpty(result)) {
        return res.send({
            "success": false,
            "message": roleData[0].name + " Create fail, try again."
        });
    } else {
        return res.send({
            "success": true,
            "message": roleData[0].name + "  Created successfully ."
        });
    }

});

router.get('/active_list/:role_id', [verifyToken], async (req, res) => {
    const organization_id = req.decoded.organization_id;
    let role_id = req.params.role_id;
    let result = await user_model.getUserListByRoleTypeStatusAndOrganizationId(role_id, 0, organization_id);

    return res.send({
        "success": true,
        "message": "List.",
        "data": result
    });
});

router.get('/deactive_list/:role_id', [verifyToken], async (req, res) => {
    const organization_id = req.decoded.organization_id;
    let role_id = req.params.role_id;
    let result = await user_model.getUserListByRoleTypeStatusAndOrganizationId(role_id, 1, organization_id);

    return res.send({
        "success": true,
        "message": "List.",
        "data": result
    });
});


router.get('/list/:role_id', [verifyToken], async (req, res) => {
    const organization_id = req.decoded.organization_id;
    let role_id = req.params.role_id;
    let result = await user_model.getUserListByRoleTypeAndOrganizationId(role_id, organization_id);

    return res.send({
        "success": true,
        "message": "List.",
        "data": result
    });
});

router.get('/all_list', [verifyToken, onlyMTAdminCanAccess], async (req, res) => { // onlu admin can access
    let result = await user_model.getUserList();

    return res.send({
        "success": true,
        "message": "Admin List.",
        "data": result
    });
});


router.post('/updateProfile', [verifyToken], async (req, res) => {

    let reqUserData = {
        "name": req.body.name.trim(),
        "organization_id": req.decoded.organization_id,
        "id": req.decoded.id
    }

    if (isEmpty(reqUserData.name) || reqUserData.name == undefined || reqUserData.name.length < 3) {
        return res.send({
            "success": false,
            "message": "Need valid Name and length must be greater than 2.",
        });
    }


    // Check organization
    let organizationInfo = await organization_model.getOrganizationByID(reqUserData.organization_id);

    if (isEmpty(organizationInfo)) {
        return res.send({
            "success": true,
            "message": "Unknown organization."
        });
    } else if (organizationInfo[0].status == 1) {
        return res.send({
            "success": false,
            "message": "Organization already deleted."
        });
    }

    const existingProfileData = await user_model.getUserByIdAndOnganizationId(reqUserData.id, reqUserData.organization_id);

    if (isEmpty(existingProfileData)) {
        return res.send({
            "success": false,
            "message": "Unknown organization."
        });
    } else if (existingProfileData[0].status == 1) {
        return res.send({
            "success": false,
            "message": "Profile already deleted."
        });
    }

    let result = await user_model.updateProfileByID(reqUserData.id, reqUserData.name, reqUserData.organization_id);

    if (result.affectedRows == undefined || result.affectedRows == 0) {
        return res.send({
            "success": false,
            "message": "Something Wrong in system database."
        });
    }

    return res.send({
        "success": true,
        "message": "Profile has been Updated Successfully."
    });
});

router.post('/passwordChange', [verifyToken], async (req, res) => {

    let reqUserData = {
        "old_password": req.body.old_password,
        "new_password": req.body.new_password.trim(),
        "confirm_password": req.body.confirm_password.trim(),
        "organization_id": req.decoded.organization_id,
        "id": req.decoded.id
    }


    // Check organization
    let organizationInfo = await organization_model.getOrganizationByID(reqUserData.organization_id);

    if (isEmpty(organizationInfo)) {
        return res.send({
            "success": true,
            "message": "Unknown organization."
        });
    } else if (organizationInfo[0].status == 1) {
        return res.send({
            "success": false,
            "message": "Organization already deleted."
        });
    }


    const existinUserInfo = await user_model.getUserById(reqUserData.id);
    if (isEmpty(existinUserInfo)) {
        return res.send({
            "success": false,
            "message": "Unknown organization."
        });
    } else if (existinUserInfo[0].status == 1) {
        return res.send({
            "success": false,
            "message": "Profile already deleted."
        });
    }


    if (bcrypt.compareSync(reqUserData.old_password, existinUserInfo[0].password)) {
        if (reqUserData.new_password !== reqUserData.confirm_password) {
            return res.send({
                "success": false,
                "message": "Password and Confirm Password not match."
            });
        } else if (isEmpty(reqUserData.new_password) || reqUserData.new_password == undefined || reqUserData.new_password.length < 6) {
            return res.send({
                "success": false,
                "message": "Give Valid Password and Length should be greater than 6."
            });

        } else {

            reqUserData.new_password = bcrypt.hashSync(reqUserData.new_password, 10); // hasing Password
            let result = await user_model.updateUser_PasswordByIDAndOrganizationId(reqUserData.id, reqUserData.new_password, reqUserData.organization_id);

            if (!isEmpty(result) && result.affectedRows == 0) {
                return res.send({
                    "success": false,
                    "message": "Password Change Fail! Try again"
                });
            } else {
                return res.send({
                    "success": true,
                    "message": "Password Change Successfully done"
                });
            }

        }
    } else {
        return res.send({
            "success": false,
            "message": "Existing Password is wrong"
        });
    }
});


router.post('/deactive', [verifyToken], async (req, res) => {
    const organization_id = req.decoded.organization_id;
    const id = req.body.id;
    const role_id = req.body.role_id;

    if (req.decoded.role_id === 1) {  /// MT admin can deactivate others account
        let result = await user_model.updateUserStatusByID(1, id);

        
    } else if (req.decoded.role_id === 5) { /// Admin Can deactivate other users account
        let userData = await user_model.getUserByIdAndOnganizationId(id, organization_id);

        

        if (isEmpty(userData)) {
            return res.send({
                "success": false,
                "message": "User not Found."
            });
        } else if (userData[0].roles_id !== role_id) {  /// if role id does not match
            return res.send({
                "success": false,
                "message": "User not Found."
            });
        } else {
            let result = await user_model.updateUserStatusByIDAndOnganizationId(1, id, organization_id);
            return res.send({
                "success": true,
                "message": `${userData[0].name}'s Account has been  deactivated`
            });
        }
    } else {
        return res.send({
            "success": true,
            "message": "You have no permission for this route"
        });
    }

    return res.send({
        "success": true,
        "message": ` Account has been  deactivated`
    });

    
});


router.post('/active', [verifyToken], async (req, res) => {
    const organization_id = req.decoded.organization_id;
    const id = req.body.id;
    const role_id = req.body.role_id;

    if (req.decoded.role_id === 1) { // System admin can active and de active account,/// MT admin can deactivate others account
        let result = await user_model.updateUserStatusByID(0, id);
    } else if (req.decoded.role_id === 5) { //  Admin can active and de active account
        let userData = await user_model.getUserByIdAndOnganizationId(id, organization_id);


        if (isEmpty(userData)) {
            return res.send({
                "success": false,
                "message": "User not Found"
            });
        } else if (userData[0].roles_id !== role_id) {
            return res.send({
                "success": false,
                "message": "User not Found."
            });
        } else {
            let result = await user_model.updateUserStatusByIDAndOnganizationId(0, id, organization_id);
            return res.send({
                "success": true,
                "message": `${userData[0].name}'s Account has been  Ativated`
            });
        }

    } else {
        return res.send({
            "success": false,
            "message": "You have no permission for this route"
        });
    }

    return res.send({
        "success": true,
        "message": "Account has been  Ativated"
    });
});

/// Reset Password
router.post('/resetPasswordRequest', async (req, res) => {

    let reqData = {
        "email" : req.body.email
    }  

    let errorMessage = "";
    let isError = 0;
    let tryToLoginBy = "";

    // Check Email or phone number validation
    if (reqData.email === undefined || isEmpty(reqData.email)) {
        isError = 1;
        errorMessage += "Give valid phone number or email.";
    } else if (!isNaN(reqData.email)) {
        tryToLoginBy = "phone";
        if (reqData.email.length != 11) {
            isError = 1;
            errorMessage += "Give valid phone number or email.";
        }
    } else {
        const re = /\S+@\S+\.\S+/;
        tryToLoginBy = "email";
        if (!re.test(reqData.email)) {
            isError = 1;
            errorMessage += " Give a valid email.";
        }
    }

    if (isError == 1) {
        return res.send({
            "success": false,
            "message": errorMessage
        });
    }

    // Get User data from user table.
    let existingData = await user_model.getUserByPhoneOrEmail(reqData.email);

    if (isEmpty(existingData)) {
        return res.send({
            "success": false,
            "message": "No user found."
        });
    }  
    

    if(existingData[0].roles_id != 4){
        crypto.randomBytes(32,async (err,buffer)=>{
            if(err){
                console.log(err)
            }
            const token = buffer.toString("hex")
            let expired_time = await  common.addFiveMinutes();
            

    
             
             let resetPassData = {
                 "user_id" : existingData[0].id,
                 "resetToken" : token 
                 //"expired_time" : expired_time
             }
            
            //let expireToken = Date.now() + 3600000
    
            let passResetData = await user_model.insertTokenAndExpiredTime(resetPassData)
            
                let mailSend = transporter.sendMail({
                    to:reqData.email,
                    from:"shovon@medinatech.co",
                    subject:"password reset",
                    html:`
                    <p>You requested for password reset do</p>
                    <h5>click in this <a href="${reqData.email}/reset/${token}">link</a> to reset password</h5>
                    `
                })
                
            return res.send({
                "success": true,
                "message": "Check Email",
               
            });
    
            })
    }
    else{
        
         
       // const otp = bcrypt.hashSync("1234", 10); //hashing
        const otp = "1234"
        let expired_time = await  common.addFiveMinutes();
   
        let otpData = {
                 "user_id" : existingData[0].id,
                 "otp" : otp 
                 //"expired_time" : expired_time
             }
    
        let passResetData = await user_model.insertOTPAndExpiredTime(otpData);
        return res.send({
            "success": true,
            "message": "Check SMS",
           
        });
    }  
    });

/// Verify Token
router.post('/verifyPassResetToken',async(req,res)=>{

    let reqUserData = {
        "resetToken":req.body.resetToken        
    }
   
   let existingData = await user_model.getUserByToken(reqUserData.resetToken);

   if(isEmpty(existingData)){
    return res.send({
        "success": false,
        "message": "User not Found"
    });
    }

    if(existingData)
    {
        let userData = await user_model.getUserById(existingData[0].user_id);
        return res.send({
            "success": true,
            "message": userData
        });
    }
});


router.post('/resetPasswordToken',async(req,res)=>{

    let reqUserData = {
        
        "new_password": req.body.new_password.trim(),
        "confirm_password": req.body.confirm_password.trim(),
        "resetToken":req.body.resetToken       
    }
   

   let existingData = await user_model.getUserByToken(reqUserData.resetToken);

   if(isEmpty(existingData)){
    return res.send({
        "success": false,
        "message": "No data found"
    });
}

    let userData = await user_model.getUserById(existingData[0].user_id);
    
    

    if(userData[0].status == 0)
    {
        if(existingData[0].resetToken == reqUserData.resetToken && reqUserData.resetToken != "000000"){

            existingData[0].resetToken = "000000";
         
            const userId = existingData[0].user_id;
            
             
             if (reqUserData.new_password !== reqUserData.confirm_password) {
                 return res.send({
                     "success": false,
                     "message":  "New Password and Confirm Password not match."
                 });
             } else if (isEmpty(reqUserData.new_password) || reqUserData.new_password == undefined || reqUserData.new_password.length < 6) {
                 return res.send({
                     "success": false,
                     "message": "Give Valid Password and Length should be greater than 6."
                 });
         
             } else {
         
                 reqUserData.new_password = bcrypt.hashSync(reqUserData.new_password, 10); // hasing Password
                 let result = await user_model.updateUser_PasswordByIDAndOrganizationId(userId, reqUserData.new_password, userData[0].organization_id);
                 let updateResetToken = await user_model.updateTokenAndExpiredTime(existingData[0].id,userId, existingData[0].resetToken);
         
                 if (!isEmpty(result) && result.affectedRows == 0) {
                     return res.send({
                         "success": false,
                         "message": "Password Change Fail! Try again"
                     });
                 } else {
                     return res.send({
                         "success": true,
                         "message": "Password Change Successfully done"
                     });
                 }
         
             }
            }
            else{
                return res.send({
                    "success": false,
                    "message": "Not eligible"
                });
            }
            
    } 
 
});

/// Verify OTP
router.post('/verifyOTP',async(req,res)=>{

    let reqUserData = {
        "otp":req.body.otp        
    }

    //let otp = bcrypt.hashSync(reqUserData.otp , 10); //hashing

    
   
   let existingData = await user_model.getUserByOTP(reqUserData.otp);


   if(isEmpty(existingData)){
    return res.send({
        "success": false,
        "message": "User not Found"
    });
    }

    if(existingData)
    {
        let userData = await user_model.getUserById(existingData[0].user_id);
        return res.send({
            "success": true,
            "message": userData
        });
    }
});


router.post('/resetPasswordOTP',async(req,res)=>{

    let reqUserData = {
        
        "new_password": req.body.new_password.trim(),
        "confirm_password": req.body.confirm_password.trim(),
        "otp":req.body.otp       
    }
   

   let existingData = await user_model.getUserByOTP(reqUserData.otp);

   if(isEmpty(existingData)){
    return res.send({
        "success": false,
        "message": "No data found"
    });
}

 

    let userData = await user_model.getUserById(existingData[0].user_id);
    
    

    if(userData[0].status == 0)
    {
        if(existingData[0].otp == reqUserData.otp && reqUserData.otp != "000000"){

            existingData[0].otp = "000000";
         
            const userId = existingData[0].user_id;
            
             
             if (reqUserData.new_password !== reqUserData.confirm_password) {
                 return res.send({
                     "success": false,
                     "message":  "New Password and Confirm Password not match."
                 });
             } else if (isEmpty(reqUserData.new_password) || reqUserData.new_password == undefined || reqUserData.new_password.length < 6) {
                 return res.send({
                     "success": false,
                     "message": "Give Valid Password and Length should be greater than 6."
                 });
         
             } else {
         
                 reqUserData.new_password = bcrypt.hashSync(reqUserData.new_password, 10); // hasing Password
                 let result = await user_model.updateUser_PasswordByIDAndOrganizationId(userId, reqUserData.new_password, userData[0].organization_id);
                 let updateResetOTP = await user_model.updateOTPAndExpiredTime(existingData[0].id,userId, existingData[0].otp);
         
                 if (!isEmpty(result) && result.affectedRows == 0) {
                     return res.send({
                         "success": false,
                         "message": "Password Change Fail! Try again"
                     });
                 } else {
                     return res.send({
                         "success": true,
                         "message": "Password Change Successfully done"
                     });
                 }
         
             }
            }
            else{
                return res.send({
                    "success": false,
                    "message": "Not eligible"
                });
            }
            
    } 
 
});





router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

module.exports = router;