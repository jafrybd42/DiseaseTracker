const express = require("express");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const isEmpty = require("is-empty");
const router = express.Router();
const user_model = require('../../model/user');
const reports_model = require('../../model/reports');
const shared_reports_model = require('../../model/shared_reports');
const enroll_membership_model = require('../../model/enroll_membership');
const reports_download_history_model = require('../../model/reports_download_history');
const organization_model = require('../../model/organization');

const verifyToken = require('../../jwt/verify/api_v1/verifyToken');
const verifyParticipantsCantAccess = require('../../jwt/verify/api_v1/verifyParticipantsCantAccess');
const common_model = require('../../common');

router.post('/share_reports', [verifyToken, verifyParticipantsCantAccess], async (req, res) => {
    let reqData = {
        "filter": req.body.filter,
        "data": req.body.data,
        "shared_management_id": req.body.shared_management,
        "shared_team_member_id": req.body.shared_team_member,
        "shared_admin_id": req.body.shared_admin,
        "report_type": req.body.report_type
    }

    if (reqData.report_type === undefined || !(reqData.report_type === 1 || reqData.report_type === 2)) {
        return res.send({
            "success": false,
            "message": "Please give reports type ( 1 = Dashboard, 2 = Datatable)."
        });
    }

    let get_error = false;
    let message = "";

    if (reqData.filter === undefined || reqData.filter == "") {
        get_error = true;
        message = "Filter not found. ";
    } else {
        try {
            reqData.filter = JSON.stringify(reqData.filter);
            
        } catch (error) {
            get_error = true;
            message = "Filter is not a Correct Format.";
        }
    }


    if (reqData.data === undefined || reqData.data == "") {
        get_error = true;
        message = "Data not found. ";
    } else {
        if (reqData.data.success !== undefined && reqData.data.success == false) {
            return res.send({
                "success": false,
                "message": "Data result must be success and must be true."
            });
        } else {
            try {
                reqData.data = JSON.stringify(reqData.data);
            } catch (error) {
                get_error = true;
                message = "Data is not a correct format.";
            }
        }
    }

    const organization_id = req.decoded.organization_id === undefined ? 0 : req.decoded.organization_id;
    let shared_reports = [];

    let shared_report = {
        "organization_id": organization_id,
        "report_id": 0,
        "role_type": 0,
        "shared_user_id": 0
    }

    let has_management = false;
    let has_team_membert = false;
    let has_admin = false;

    if (reqData.shared_management_id !== undefined && Array.isArray(reqData.shared_management_id) && reqData.shared_management_id.length > 0) {

        shared_report.role_type = 3;
        shared_report.shared_user_id = 0;

        if (reqData.shared_management_id[0] === 0) {
            shared_reports.push({ ...shared_report });
            has_management = true;
        } else {
            for (let i = 0; i < reqData.shared_management_id.length; i++) {
                let userInfo = await user_model.getUserByIdAndOnganizationId(reqData.shared_management_id[i], organization_id);

                if (!isEmpty(userInfo)) {
                    if (userInfo[0].roles_id == 3) {
                        shared_report.shared_user_id = reqData.shared_management_id[i];
                        shared_reports.push({ ...shared_report });
                        has_management = true;
                    }
                }
            }
        }
    }

    if (reqData.shared_team_member_id !== undefined && Array.isArray(reqData.shared_team_member_id) && reqData.shared_team_member_id.length > 0) {

        shared_report.role_type = 2;
        shared_report.shared_user_id = 0;

        if (reqData.shared_team_member_id[0] === 0) {
            shared_reports.push({ ...shared_report });
            has_team_membert = true;
        } else {
            for (let i = 0; i < reqData.shared_team_member_id.length; i++) {
                let userInfo = await user_model.getUserByIdAndOnganizationId(reqData.shared_team_member_id[i], organization_id);

                if (!isEmpty(userInfo)) {
                    if (userInfo[0].roles_id == 2) {
                        shared_report.shared_user_id = reqData.shared_team_member_id[i];
                        shared_reports.push({ ...shared_report });
                        has_team_membert = true;
                    }
                }
            }
        }
    }


    if (reqData.shared_admin_id !== undefined && Array.isArray(reqData.shared_admin_id) && reqData.shared_admin_id.length > 0) {

        shared_report.role_type = 5;
        shared_report.shared_user_id = 0;

        if (reqData.shared_admin_id[0] === 0) {
            shared_reports.push({ ...shared_report });
            has_admin = true;
        } else {
            for (let i = 0; i < reqData.shared_admin_id.length; i++) {
                let userInfo = await user_model.getUserByIdAndOnganizationId(reqData.shared_admin_id[i], organization_id);

                if (!isEmpty(userInfo)) {
                    if (userInfo[0].roles_id == 5) {
                        shared_report.shared_user_id = reqData.shared_admin_id[i];
                        shared_reports.push({ ...shared_report });
                        has_admin = true;
                    }
                }
            }
        }
    }

    if (!has_management && !has_team_membert && !has_admin) {
        return res.send({
            "success": false,
            "message": "Please Share with some one (Management, team_member, admin)",
        });
    } else {

        let reports = {
            "report_type": reqData.report_type,
            "organization_id": organization_id,
            "created_by": req.decoded.id,
            "filtered_data": reqData.filter,
            "result": reqData.data
        }


        let result = await reports_model.addNew(reports);

        if (result.affectedRows == undefined || result.affectedRows < 1) {
            return res.send({
                "success": false,
                "message": "Something Wrong in system database."
            });
        }

        let report_insert_id = result.insertId;


        for (let i = 0; i < shared_reports.length; i++) {
            shared_reports[i].report_id = report_insert_id;
            await shared_reports_model.addNew(shared_reports[i]);
        }

        return res.send({
            "success": true,
            "message": "Reports Successfully Shared."
        });
    }

});

router.get('/report_download', [verifyToken], async (req, res) => {

    let organization_id = req.decoded.organization_id;
    let running_package = req.decoded.memberShipInfo.runing_member_ship_package;

    let add_new_recodrd = {
        "organization_id": organization_id,
        "user_id": req.decoded.id
    };

    // if they have unlmited download access
    if (running_package.number_of_downloads === -1) {
        await reports_download_history_model.addNew(add_new_recodrd);
        return res.send({
            'message': "You can Download this Reports.",
            "success": true
        })
    } else {
        // get enroll package details
        let nowEnroleMemberShip = await enroll_membership_model.getInrollMember_ShipByOnganizationId(organization_id);
        let package_end_Data = "";

        if (isEmpty(nowEnroleMemberShip)) {

            // not found any packages then get start date from organization created date and generate package end date.
            let organization_info = await organization_model.getOrganizationByID(organization_id);
            package_end_Data = await common_model.getNextMemberShipEndDate(organization_info[0].created_at);
        } else {
            //  get enroll date as start date then generate package end date
            let enroll_start_date = nowEnroleMemberShip[0].start_date;
            package_end_Data = await common_model.getNextMemberShipEndDate(enroll_start_date);
        }


        let package_start_Data = await common_model.getCustomDate(package_end_Data, 0, -1, 0);
        let result = await reports_download_history_model.getTotalReportsDownloadHistoryByOgranizationIdAndDate(organization_id, package_start_Data, package_end_Data);

        if (result[0].total_download < running_package.number_of_downloads) {
            await reports_download_history_model.addNew(add_new_recodrd);
            return res.send({
                'message': "You can download  this Reports.",
                "success": true
            })
        } else {
            return res.send({
                'message': "Download limit over. You can't download more. Please update this Package.",
                "success": false
            })
        }
    }
});


router.get('/individualReportList', [verifyToken], async (req, res) => { 

    let  userId = req.decoded.id;

    let userInfo = await user_model.getUserByIdAndOnganizationId(userId, req.decoded.organization_id);
    
    if(isEmpty(userInfo)){
        return res.send({
            "success": true,
            "message": "No User Found."
        });
    }

    let reportList = await shared_reports_model.getReportListByUserID(req.decoded.organization_id,req.decoded.role_id,userId);

    if(isEmpty(reportList)){
        return res.send({
            "success": true,
            "message": "You have no reports"
        });
    }
    return res.send({
        "success": true,
        "message": reportList
    });
});


router.get('/reportDetailsById/:id', [verifyToken], async (req, res) => { 

    let  userId = req.decoded.id;
    let reportId = req.params.id;

    let userInfo = await user_model.getUserByIdAndOnganizationId(userId, req.decoded.organization_id);
    
    if(isEmpty(userInfo)){
        return res.send({
            "success": true,
            "message": "No User Found."
        });
    }

    let report = await reports_model.getReportById(reportId,req.decoded.organization_id,req.decoded.role_id,userId);

    if(isEmpty(report))
    {
        return res.send({
            "success": false,
            "message": "You are not eligible to see this report"
        });
    }

    report[0].result = JSON.parse(report[0].result);
    report[0].filtered_data = JSON.parse(report[0].filtered_data);    
    
    return res.send({
        "success": true,
        "message": report
    });
});


router.get('/reportListByCreator', [verifyToken], async (req, res) => { 

    let  userId = req.decoded.id;

    let userInfo = await user_model.getUserByIdAndOnganizationId(userId, req.decoded.organization_id);
    
    if(isEmpty(userInfo)){
        return res.send({
            "success": true,
            "message": "No User Found."
        });
    }

    let reportList = await reports_model.getReportByCreatedBy(userId);

    if(isEmpty(reportList))
    {
        return res.send({
            "success": false,
            "message": "You have not made any reports"
        });
    }
    for (let i=0 ; i<reportList.length; i++){
        reportList[i].result = JSON.parse(reportList[i].result);
        reportList[i].filtered_data = JSON.parse(reportList[i].filtered_data);
    }

    // for (let i=0 ; i<reportList.length; i++){
    //     let reportShared = await shared_reports_model.getSharedResultByReportId(reportList[i].id);
        
    //     reportList.push(reportShared);
    //     return res.send({
    //         "success": true,
            
    //         "message": reportList
    //     });


    // }

    // //let reportShared = await shared_reports_model.getSharedResultByReportId(reportList[0].id);

    return res.send({
        "success": true,
        "count" : reportList.length,
        "message": reportList
    });
});



router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});


module.exports = router;

