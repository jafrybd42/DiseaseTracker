const express = require("express");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const isEmpty = require("is-empty");
const router = express.Router();
const user_model = require('../../model/user');
const role_model = require('../../model/role');
const package_model = require('../../model/package');
const enroll_membership_model = require('../../model/enroll_membership');
const verifyToken = require('../../jwt/verify/api_v1/verifyToken');
const onlyMTAdminCanAccess = require('../../jwt/verify/api_v1/onlyMTAdminCanAccess');
const verifyParticipantsCantAccess = require('../../jwt/verify/api_v1/verifyParticipantsCantAccess');
const verifyManagementCantAccess = require('../../jwt/verify/api_v1/verifyManagementCantAccess');

const common_model = require('../../common');

router.get('/list', [verifyToken], async (req, res) => {

    let result = await package_model.getPackageList();

    return res.send({
        "success": true,
        "message": "Package Plan List.",
        "count": result.length,
        "data": result
    });
});

router.post('/create', [verifyToken, onlyMTAdminCanAccess], async (req, res) => {


    let reqData = {
        "title": req.body.title,
        "number_of_team_member": req.body.total_team_member,
        "number_of_management": req.body.total_management,
        "number_of_participant": -1,
        "number_of_admin": req.body.total_admin,
        "status": 0
    }

    if (reqData.number_of_team_member === undefined || isNaN(Number(reqData.number_of_team_member))) {
        return res.send({
            "success": false,
            "message": "Give valid total team member",
        });
    }

    if (reqData.number_of_management === undefined || isNaN(Number(reqData.number_of_management))) {
        return res.send({
            "success": false,
            "message": "Give valid total Management",
        });
    }

    if (reqData.number_of_admin === undefined || isNaN(Number(reqData.number_of_admin))) {
        return res.send({
            "success": false,
            "message": "Give valid total admin",
        });
    }


    if (reqData.title == undefined || reqData.title < 2) {
        return res.send({
            "success": false,
            "message": "Need valid Package Plan Name and length must be greater than 1.",
        });
    }

    let existingData = await package_model.getPackageByTitle(reqData.title);

    if (!isEmpty(existingData)) {
        return res.send({
            "success": false,
            "message": existingData[0].status == "0" ? "Package Plan Already Exist." : "Package Plan Exist But Deactivate."
        });
    }

    let result = await package_model.addNewPackage(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "success": false,
            "message": "Something Wrong in system database."
        });
    }

    return res.send({
        "success": true,
        "message": "Package Plan has been Added Successfully."
    });


});

router.post('/update', [verifyToken, onlyMTAdminCanAccess], async (req, res) => {

    const id = req.body.id;
    let reqData = {
        "title": req.body.title,
        "number_of_team_member": req.body.total_team_member,
        "number_of_management": req.body.total_management,
        "number_of_admin": req.body.total_admin,
        "updated_at": await common_model.getTodayDateTime()
    }

    if (id === undefined || isNaN(Number(id))) {
        return res.send({
            "success": false,
            "message": "Give a valid id",
        });
    }


    let existingData = await package_model.getPackageByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "success": false,
            "message": "Packages Not Exist."
        });
    }

    if (reqData.number_of_team_member === undefined || isNaN(Number(reqData.number_of_team_member))) {
        return res.send({
            "success": false,
            "message": "Give valid total team member",
        });
    }

    if (reqData.number_of_management === undefined || isNaN(Number(reqData.number_of_management))) {
        return res.send({
            "success": false,
            "message": "Give valid total Management",
        });
    }

    if (reqData.number_of_admin === undefined || isNaN(Number(reqData.number_of_admin))) {
        return res.send({
            "success": false,
            "message": "Give valid total admin",
        });
    }


    if (reqData.title == undefined || reqData.title.length < 2) {
        return res.send({
            "success": false,
            "message": "Need valid Packaget title and length must be greater than 1.",
        });
    }



    existingData = await package_model.getPackageByTitle(reqData.title);

    if (!isEmpty(existingData) && existingData[0].id != id) {
        return res.send({
            "success": false,
            "message": existingData[0].status == "0" ? "Package Plan Already Exist." : "Package Plan Exist but Deactivate."
        });
    }



    let result = await package_model.updatePackageByID(id, reqData);

    if (result.affectedRows == undefined || result.affectedRows == 0) {
        return res.send({
            "success": false,
            "message": "Something Wrong in system database."
        });
    }

    return res.send({
        "success": true,
        "message": "Packages has been Updated Successfully."
    });

});

router.post('/upgratePackage', [verifyToken, verifyParticipantsCantAccess, verifyManagementCantAccess], async (req, res) => {

    const today = await common_model.getTodayDate();

    let reqData = {
        "organization_id": req.decoded.organization_id === undefined ? 0 : req.decoded.organization_id,
        "package_id": req.body.package_id,
        "user_id": req.decoded.id,
        "start_date": today,
        "created_at": today,
        "status": 0
    }

    let packageInfo = await package_model.getPackageByID(reqData.package_id);

    if (isEmpty(packageInfo) || packageInfo[0].status == 1) {
        return res.send({
            "success": false,
            "message": "Unknown package."
        });
    }

    let nowEnroleMemberShip = await enroll_membership_model.getInrollMember_ShipByOnganizationId(reqData.organization_id);
    let result;
    let nowEnrolePackage = [{ "title": "Basic" }]

    // Fast Check Packages
    // 1, if empty then enroll in request Member ship plan
    // 2, if not empty then check length, 
    //   2.2 if length 1 then change packages

    if (isEmpty(nowEnroleMemberShip)) {
        let getLowestPackage = await package_model.getLowestPackage();

        if (isEmpty(getLowestPackage)) {
            return res.send({
                "success": false,
                "message": " No Membership plan available now."
            });
        }

        if (packageInfo[0].id == getLowestPackage[0].id) {
            return res.send({
                "success": false,
                "message": "You are already in " + getLowestPackage[0].title + " Membership plan"
            });
        } else {
            result = await enroll_membership_model.addNewMembership(reqData);
        }

    } else {

        // check that he has no request for change package in next month
        if (nowEnroleMemberShip.length === 1) {

            if (packageInfo[0].id == nowEnroleMemberShip[0].package_id) {
                return res.send({
                    "success": false,
                    "message": "You are already in " + packageInfo[0].title + " plan"
                });
            } else {

                nowEnrolePackage = await package_model.getPackageByID(nowEnroleMemberShip[0].package_id);

                updateNowEnrollData = {
                    "is_cancel_request": 1,
                    "cancel_req_date": today,
                    "status": 0,
                    "update_at": await common_model.getTodayDateTime()
                }

                if (packageInfo[0].priority < nowEnrolePackage[0].priority) {
                    // Active from today
                    updateNowEnrollData.is_cancel_request = 0;
                    updateNowEnrollData.status = 1;
                    updateNowEnrollData.end_date = today;
                } else {
                    // As they go down Active from Next Month
                    updateNowEnrollData.end_date = await common_model.getNextMemberShipEndDate(nowEnroleMemberShip[0].start_date);
                    reqData.start_date = new Date(await common_model.getCustomDate(updateNowEnrollData.end_date, 1, 0, 0));
                }
                result = await enroll_membership_model.updatePreviousMembershipAddNewMembership(nowEnroleMemberShip[0].id, updateNowEnrollData, reqData);

            }

        } else {

            // He has already package change request 
            nowEnrolePackage = await package_model.getPackageByID(nowEnroleMemberShip[1].package_id);

            if (await common_model.compareTwoDate(today, nowEnroleMemberShip[0].end_date)) {
                // console.log(" ----- Compare True -----");
                let updateNowEnrollData = {
                    "status": 1,
                    "update_at": await common_model.getTodayDateTime()
                }
                // if date over we update this status
                result = await enroll_membership_model.updateMembershipInfoByID(nowEnroleMemberShip[0].id, updateNowEnrollData);

                if (packageInfo[0].id == nowEnroleMemberShip[1].package_id) {
                    return res.send({
                        "success": false,
                        "message": "You are already in " + packageInfo[0].title + " plan"
                    });
                } else {
                    nowEnrolePackage = await package_model.getPackageByID(nowEnroleMemberShip[1].package_id);

                    updateNowEnrollData = {
                        "is_cancel_request": 1,
                        "cancel_req_date": today,
                        "status": 0,
                        "update_at": await common_model.getTodayDateTime()
                    }

                    if (packageInfo[0].priority < nowEnrolePackage[0].priority) {
                        // Active from today
                        updateNowEnrollData.is_cancel_request = 0;
                        updateNowEnrollData.status = 1;
                        updateNowEnrollData.end_date = today;
                    } else {
                        // As they go down Active from Next Month
                        updateNowEnrollData.end_date = await common_model.getNextMemberShipEndDate(nowEnroleMemberShip[0].start_date);
                        reqData.start_date = new Date(await common_model.getCustomDate(updateNowEnrollData.end_date, 1, 0, 0));
                    }
                    result = await enroll_membership_model.updatePreviousMembershipAddNewMembership(nowEnroleMemberShip[1].id, updateNowEnrollData, reqData);
                }

            } else {

                // last package end date in not cove.
                // Now we check last Member ship to request member ship
                let nowEnrolePackage = await package_model.getPackageByID(nowEnroleMemberShip[1].package_id);
                previousEnrolePackage = await package_model.getPackageByID(nowEnroleMemberShip[0].package_id);

                if (packageInfo[0].id == nowEnroleMemberShip[1].package_id) {
                    return res.send({
                        "success": false,
                        "message": "You are already in " + packageInfo[0].title + " plan"
                    });
                } else {

                    //  as it's new member request, we check package priority
                    let targetEndDate = await common_model.getNextMemberShipEndDate(nowEnroleMemberShip[0].start_date);
                    let start_date = new Date(await common_model.getCustomDate(targetEndDate, 1, 0, 0));

                    if (packageInfo[0].priority == previousEnrolePackage[0].priority) {
                        // now compare to previous request, if true then we update 1st row and delete 2nd row  

                        updateNowEnrollData = {
                            "is_cancel_request": 0,
                            "cancel_req_date": today,
                            "status": 0,
                            "end_date": "",
                            "update_at": await common_model.getTodayDateTime()
                        }

                        result = await enroll_membership_model.updateMembershipInfoByID(nowEnroleMemberShip[0].id, updateNowEnrollData);
                        result = await enroll_membership_model.deleteMembershipById(nowEnroleMemberShip[1].id);

                    } else {
                        // as it's not a  new member request, we  update 1st row and 2nd row  
                        if (packageInfo[0].priority < nowEnrolePackage[0].priority) {

                            if (packageInfo[0].priority < previousEnrolePackage[0].priority) {

                                updateNowEnrollData = {
                                    "is_cancel_request": 0,
                                    "cancel_req_date": today,
                                    "status": 1,
                                    "end_date": today,
                                    "update_at": await common_model.getTodayDateTime()
                                }
                                start_date = today;

                            } else {
                                updateNowEnrollData = {
                                    "is_cancel_request": 1,
                                    "cancel_req_date": today,
                                    "end_date": await common_model.getNextMemberShipEndDate(nowEnroleMemberShip[0].start_date),
                                    "update_at": await common_model.getTodayDateTime()
                                }
                            }

                            result = await enroll_membership_model.updateMembershipInfoByID(nowEnroleMemberShip[0].id, updateNowEnrollData);
                            updateNowEnrollData = {
                                "is_cancel_request": 0,
                                "package_id": reqData.package_id,
                                "start_date": start_date,
                                "update_at": await common_model.getTodayDateTime()
                            }

                            result = await enroll_membership_model.updateMembershipInfoByID(nowEnroleMemberShip[1].id, updateNowEnrollData);

                        } else {

                            updateNowEnrollData = {
                                "is_cancel_request": 1,
                                "cancel_req_date": today,
                                "update_at": await common_model.getTodayDateTime()
                            }

                            result = await enroll_membership_model.updateMembershipInfoByID(nowEnroleMemberShip[0].id, updateNowEnrollData);

                            updateNowEnrollData = {
                                "is_cancel_request": 0,
                                "package_id": reqData.package_id,
                                "start_date": start_date,
                                "update_at": await common_model.getTodayDateTime()
                            }

                            result = await enroll_membership_model.updateMembershipInfoByID(nowEnroleMemberShip[1].id, updateNowEnrollData);

                        }
                    }
                }
            }
        }
    }


    if (result === undefined || result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "success": false,
            "message": "Something Wrong try again."
        });
    } else {
        return res.send({
            "success": true,
            "message": "You Successfully enroll from " + nowEnrolePackage[0].title + " Membership to " + packageInfo[0].title + " Membership."
        });
    }
});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});


module.exports = router;

