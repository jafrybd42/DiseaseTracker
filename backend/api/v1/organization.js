const express = require("express");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const isEmpty = require("is-empty");
const router = express.Router();
const user_model = require('../../model/user');
const role_model = require('../../model/role');
const organization_model = require('../../model/organization');
const verifyToken = require('../../jwt/verify/api_v1/verifyToken');
const onlyMTAdminCanAccess = require('../../jwt/verify/api_v1/onlyMTAdminCanAccess');



const common = require('../../common');

router.get('/list', [verifyToken, onlyMTAdminCanAccess], async (req, res) => { 

    let result = await organization_model.getOrganizationList();

    return res.send({
        "success": true,
        "message": "Organization List.",
        "count":result.length,
        "data": result
    });
});


router.get('/allList', [verifyToken, onlyMTAdminCanAccess], async (req, res) => {

    let result = await organization_model.getAllOrganizationList();

    return res.send({
        "success": true,
        "message": "Organization List.",
        "count":result.length,
        "data": result
    });
});

router.get('/deactiveList', [verifyToken, onlyMTAdminCanAccess], async (req, res) => {

    
    let result = await organization_model.getDeactiveOrganizationList();

    return res.send({
        "success": true,
        "message": "Deactive Organization List.",
        "count":result.length,
        "data": result
    });
});

router.get('/activeList', [verifyToken, onlyMTAdminCanAccess], async (req, res) => {

    
    let result = await organization_model.getActiveOrganizationList();

    return res.send({
        "success": true,
        "message": "Active Organization  List.",
        "data": result
    });
});



router.post('/add', [verifyToken, onlyMTAdminCanAccess], async (req, res) => {

   
    let reqData = {
        "name": req.body.name,
        "status": 0
       
    }


    if (reqData.name == undefined || reqData.name < 2) {
        return res.send({
            "success": false,
            "message": "Need valid Organization Name and length must be greater than 1.",
        });
    }

    let existingData = await organization_model.getOrganizationByName(reqData.name);

    if (!isEmpty(existingData)) {
        return res.send({
            "success": false,
            "message": existingData[0].status == "0" ? "Organization Already Exist." : "Organization Exist but Deactivate."
        });
    }

    let result = await organization_model.addNewOrganization(reqData);

    if (result.affectedRows == undefined || result.affectedRows < 1) {
        return res.send({
            "success": false,
            "message": "Something Wrong in system database."
        });
    }

    return res.send({
        "success": true,
        "message": "Organization has been Added Successfully."
    });


});

router.post('/update', [verifyToken, onlyMTAdminCanAccess], async (req, res) => {

    
    let name = req.body.name;
    let id = req.body.id;
    let updated_at = await  common.getTodayDateTime();

    if (name == undefined || name.length < 2) {
        return res.send({
            "success": false,
            "message": "Need valid Organization Name and length must be greater than 1.",
        });
    }

    let existingData = await organization_model.getOrganizationByID(id);

    if (isEmpty(existingData)) {
        return res.send({
            "success": false,
            "message": "Organization  Not Exist."
        });
    }

     existingData = await organization_model.getOrganizationByName(name);

    if (!isEmpty(existingData)) {
        return res.send({
            "success": false,
            "message": existingData[0].status == "0" ? "Organization Already Exist." : "Organization Exist but Deactivate."
        });
    }

    

    let result = await organization_model.updateOrganizationByID(id, name,updated_at);

    if (result.affectedRows == undefined || result.affectedRows == 0) {
        return res.send({
            "success": false,
            "message": "Something Wrong in system database."
        });
    }

    return res.send({
        "success": true,
        "message": "Organization has been Updated Successfully."
    });

});

router.post('/delete', [verifyToken, onlyMTAdminCanAccess], async (req, res) => {

    
    let id = req.body.id;
    let updated_at = await  common.getTodayDateTime();

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "success": false,
            "message": "invalid id."
        });
    }

    let existingData = await organization_model.getOrganizationByID(id);
    

    if (isEmpty(existingData)) {
        return res.send({
            "success": false,
            "message": "Organization  Not Exist."
        });
    }
    if (existingData[0].status === 1) {
        return res.send({
            "success": false,
            "message": "Already this is Deactive"
        });
    }

    let result = await organization_model.deactiveOrganization(id,updated_at);

    return res.send({
        "success": true,
        "message": `Organization '${existingData[0].name}' Deleted Successfully `,
        "data": []
    });
});

router.post('/active', [verifyToken, onlyMTAdminCanAccess], async (req, res) => {

    let id = req.body.id;
    let updated_at = await  common.getTodayDateTime();

    try {
        id = parseInt(id);
    } catch (e) {
        id = "";
    }

    if (!id || typeof (id) !== "number" || id < 1) {
        return res.send({
            "success": false,
            "message": "invalid id."
        });
    }

    let existingData = await organization_model.getOrganizationByID(id);
    

    if (isEmpty(existingData)) {
        return res.send({
            "success": false,
            "message": "Organization   Not Exist."
        });
    }


    if (existingData[0].status === 0) {
        return res.send({
            "success": false,
            "message": "Already this is Active"
        });
    }
    let result = await organization_model.activateOrganization(id,updated_at);

    return res.send({
        "success": true,
        "message": `Organization '${existingData[0].name}' Activated Sucessfully.`

    });
});



router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});


module.exports = router;

