const express = require("express");
const router = express.Router();
const isEmpty = require("is-empty");
const disease_model = require('../../model/diseases');
const division_model = require('../../model/divisions');
const district_model = require('../../model/districts');
const upazila_model = require('../../model/upazilas');
const health_data_model = require('../../model/health_data');

const authentication = require('./authentication');
const admin = require('./admin');
const user = require('./user');
const organization = require('./organization');
const health_data = require('./health_data');
const package = require('./package');
const dashboard = require('./dashboard');
const report = require('./report');
const upload_file_history = require('./upload_file_history');


router.use('/authentication', authentication);
router.use('/admin', admin);
router.use('/user', user);
router.use('/organization', organization);
router.use('/health_data', health_data);
router.use('/package', package);
router.use('/dashboard',dashboard);
router.use('/report',report);
router.use('/upload_file_history',upload_file_history);



///  disease list 
router.get('/diseaseList', async (req, res) => {
    let result = await disease_model.getDiseagesList();
    if (isEmpty(result)) {
        return res.send({
            "success": true,
            "message": "No Data Found.",
            "count": result.length,
            "data": result
        });
    }
    return res.send({
        "success": true,
        "message": "Disease List.",
        "count": result.length,
        "data": result
    });
});

/// Top 10 disease list based on Number of patients affected
router.get('/topTenDisease', async (req, res) => {
    let result = await disease_model.topTenDisease();

    if (isEmpty(result)) {
        return res.send({
            "success": true,
            "message": "No Data Found.",
            "count": result.length,
            "data": result
        });
    }


    return res.send({
        "success": true,
        "message": "Top 10 Diseases.",
        "count": result.length,
        "data": result
    });
});



/// Total Number of Patients Affected overall
router.get('/totalDiseasePeople', async (req, res) => {
    let result = await disease_model.totalDiseasePeople();

    if (isEmpty(result)) {
        return res.send({
            "success": true,
            "message": "No Data Found.",
            "count": result.length,
            "data": result
        });
    }

    return res.send({
        "success": true,
        "message": "Total Number of Patients Affected overall.",
        "data": result
    });
});

/// For Multiple Selection  District
router.post('/districtListByDivisionID', async (req, res) => {

    let division_id = req.body.division_id;
    // console.log(division_id)

    // for(let i = 0 ;i<division_id.length;i++){
    //     console.log(division_id[i]);

    //     let divisionInfo = await division_model.getDivisionByID(division_id[i]);
    //     console.log(divisionInfo);
    //     if (isEmpty(divisionInfo)) {
    //         return res.send({
    //             "success": false,
    //             "message": "Division Not Found."
    //         });
    //     }
    // }

    if (req.body.division_id !== undefined && Array.isArray(req.body.division_id) && req.body.division_id.length > 0) {

        let districtList = await district_model.getDisByDivID(division_id);

        return res.send({
            "success": true,
            "message": " District List",
            "count": districtList.length,
            "data": districtList
        });
    }

    else {
        return res.send({
            "success": true,
            "message": " No Data Found "
        });
    }
});

/// For Multiple Selection Upazila
router.post('/upazilaListByDistrictID', async (req, res) => {

    let district_id = req.body.district_id;

    if (req.body.district_id !== undefined && Array.isArray(req.body.district_id) && req.body.district_id.length > 0) {

        let upazilaList = await upazila_model.getUpazilasByDisID(district_id);

        return res.send({
            "success": true,
            "message": "Upazila District List",
            "count": upazilaList.length,
            "data": upazilaList
        });
    }

    else {
        return res.send({
            "success": true,
            "message": " No Data Found "
        });
    }



});

router.post('/searchHealthData', async (req, res) => {

    let searchField = {};

    if (req.body.disease_id !== undefined && req.body.disease_id !== "") {
        searchField.disease_id = req.body.disease_id;
    }

    let diseaseInfo = await disease_model.getDiseagesByID(searchField.disease_id);

    if (isEmpty(diseaseInfo)) {
        return res.send({
            "success": false,
            "message": "Invalid Disease"
        });
    }


    if (req.body.division_id !== undefined && Array.isArray(req.body.division_id) && req.body.division_id.length > 0) {
        searchField.division_id = req.body.division_id;
    }

    if (req.body.district_id !== undefined && Array.isArray(req.body.district_id) && req.body.district_id.length > 0) {
        searchField.district_id = req.body.district_id;
    }

    if (req.body.upazila_id !== undefined && Array.isArray(req.body.upazila_id) && req.body.upazila_id.length > 0) {
        searchField.upazila_id = req.body.upazila_id;
    }

    if (req.body.season_name !== undefined && Array.isArray(req.body.season_name) && req.body.season_name.length > 0) {
        searchField.season_name = req.body.season_name;
    }


    if (((searchField.disease_id) && (searchField.season_name)) && (isEmpty(searchField.division_id) && isEmpty(searchField.district_id) && isEmpty(searchField.district_id))) {
        return res.send({
            "success": false,
            "message": "You Should Select At Least One Location",

        });
    }

    /// Main Result

    let result = await health_data_model.getDataBySearching(searchField);

    /*return res.send({
            "success": true,
            "message": "Total Patient",
            "count": result.length,
            "data": isEmpty(result) ? [] : result
        });*/

    /// Filter with just disease id

    if (isEmpty(searchField.division_id) && isEmpty(searchField.district_id) && isEmpty(searchField.upazila_id)) {
        result.AgeRange = await health_data_model.getMaxMinAgeBySelectingDiseaseID(searchField.disease_id);
        result.Gender = await health_data_model.getGenderBySelectingDiseaseID(searchField.disease_id);
        result.Season = await health_data_model.getSeasonBySelectingDiseaseID(searchField.disease_id);
        result.AgeGenderChart = await health_data_model.getAgeGenderChartBySelectingDiseaseID(searchField.disease_id);
        result.MonthData = await health_data_model.getMonthlyDataBySelectingDiseaseID(searchField.disease_id);



        return res.send({
            "success": true,
            "message": "Total Patient",
            "ageRange": result.AgeRange,
            "Gender": result.Gender,
            "Season": result.Season,
            "ageGenderChart" : result.AgeGenderChart,
            "monthlyAffected" : result.MonthData,
            "count": result.length,
            "data": isEmpty(result) ? [] : result
        });
    }

    /// Filter With disease_id and division_id

    if (isEmpty(searchField.district_id) && isEmpty(searchField.upazila_id)) {
        result.AgeRange = await health_data_model.getMaxMinAgeBySelectingDiseaseIDandDivisionID(searchField);
        result.Gender = await health_data_model.getGenderBySelectingDiseaseIDandDivisionID(searchField);
        result.Season = await health_data_model.getSeasonBySelectingDiseaseIDandDivisionID(searchField);
        result.AgeGenderChart = await health_data_model.getAgeGenderChartBySelectingDiseaseIDandDivisionID(searchField);
        result.MonthData = await health_data_model.getMonthlyDataBySelectingDiseaseIDandDivisionID(searchField);


        return res.send({
            "success": true,
            "message": "Total Patient",
            "ageRange": result.AgeRange,
            "Gender": result.Gender,
            "Season": result.Season,
            "ageGenderChart" : result.AgeGenderChart,
            "monthlyAffected" : result.MonthData,
            "count": result.length,
            "data": isEmpty(result) ? [] : result
        });
    }

    /// Filter With disease_id and division_id and district_id

    if (isEmpty(searchField.upazila_id)) {
        result.AgeRange = await health_data_model.getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictID(searchField);
        result.Gender = await health_data_model.getGenderBySelectingDiseaseIDandDivisionIDandDistrictID(searchField);
        result.Season = await health_data_model.getSeasonBySelectingDiseaseIDandDivisionIDandDistrictID(searchField);
        result.AgeGenderChart = await health_data_model.getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictID(searchField);
        result.MonthData = await health_data_model.getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictID(searchField);


        return res.send({
            "success": true,
            "message": "Total Patient",
            "ageRange": isEmpty(result.AgeRange) ? [] : result.AgeRange,
            "Gender": isEmpty(result.Gender) ? [] : result.Gender,
            "Season": isEmpty(result.Season) ? [] : result.Season,
            "ageGenderChart" : result.AgeGenderChart,
            "monthlyAffected" : result.MonthData,
            "count": result.length,
            "data": isEmpty(result) ? [] : result
        });
    }

    /// Filter With disease_id and division_id and district_id and upazila_id

    if ((!isEmpty(searchField.division_id)) && (!isEmpty(searchField.district_id)) && (!isEmpty(searchField.upazila_id))) {
        
        result.AgeRange = await health_data_model.getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID(searchField);
        result.Gender = await health_data_model.getGenderBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID(searchField);
        result.Season = await health_data_model.getSeasonBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID(searchField);
        result.AgeGenderChart = await health_data_model.getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID(searchField);
        result.MonthData = await health_data_model.getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID(searchField);


        return res.send({
            "success": true,
            "message": "Total Patient",
            "ageRange": isEmpty(result.AgeRange) ? [] : result.AgeRange,
            "Gender": isEmpty(result.Gender) ? [] : result.Gender,
            "Season": isEmpty(result.Season) ? [] : result.Season,
            "ageGenderChart" : result.AgeGenderChart,
            "monthlyAffected" : result.MonthData,
            "count": result.length,
            "data": isEmpty(result) ? [] : result
        });
    }
});



// New Work on EJS File
router.get('/totalDisease', async (req, res) => {
    let result = await disease_model.getDiseagesList();

    return res.send({
        "success": true,
        "message": "Total Disease List",
        "data": result
    });
});


router.get('/divisionList', async (req, res) => {
    let result = await division_model.getDivisionList();

    return res.send({
        "success": true,
        "message": "Total Division list",
        "data": result
    });
});

router.post('/getDistrictByDivisionId', async (req, res) => {
    let division_id = req.body.division_id === undefined ? 0 : req.body.division_id;
    let result = await district_model.getDistrictByDivisionID(division_id);

    return res.send({
        "success": true,
        "message": "",
        "data": result
    });
});

router.post('/getUpzilaByDistrictId', async (req, res) => {
    let district_id = req.body.district_id === undefined ? 0 : req.body.district_id;
    let result = await upazila_model.getUpazilasByDistrictId(district_id);

    return res.send({
        "success": true,
        "message": "",
        "data": result
    });
});

router.post('/addHealthData', async (req, res) => {
    return res.send({
        "success": true,
        "message": "",
        "data": req.body
    });
});





router.get('/', (req, res) => {
    return res.send({
        "success": true,
        "message": "",
        "api v": 1
    });
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});



module.exports = router;