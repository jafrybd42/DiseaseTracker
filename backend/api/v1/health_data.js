const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const moment = require("moment");
const multer = require('multer');

const verifyToken = require('../../jwt/verify/api_v1/verifyToken');
const verifyParticipantsCantAccess = require('../../jwt/verify/api_v1/verifyParticipantsCantAccess');
const verifyManagementCantAccess = require('../../jwt/verify/api_v1/verifyManagementCantAccess');

const common_model = require('../../common');
const role_model = require('../../model/role');
const organization_model = require('../../model/organization');
const upload_file_history_model = require('../../model/upload_file_history');
const disease_model = require('../../model/diseases');
const division_model = require('../../model/divisions');
const district_model = require('../../model/districts');
const upazila_model = require('../../model/upazilas');
const health_data_model = require('../../model/health_data');


const reader = require('xlsx')
let fileFilderPath = __dirname.slice(0, __dirname.length - 7);


//File upload code

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, fileFilderPath + '/public/upload')
    },
    filename: function (req, file, cb) {
        let extArray = file.mimetype.split("/");
        let extension = extArray[extArray.length - 1];
        cb(null, Date.now() + '-' + file.originalname)
    }
})

const upload = multer({
    storage
});


router.post('/bulk_upload', [verifyToken, verifyParticipantsCantAccess, verifyManagementCantAccess, upload.single('update_bulk_File')], async (req, res) => {

    const file = req.file;

    if (file == undefined) {
        return res.send({
            "success": false,
            "message": "undefined"
        });
    }

    if (!file.originalname.match(/\.(xlsx)$/)) {

        await common_model.deleteFile(fileFilderPath + '/public/upload/', file.filename);
        return res.send({
            "success": false,
            "message": "Not xlsx File.",
            "error_data": []
        });
    }

    const currentFile = reader.readFile(fileFilderPath + '/public/upload/' + file.filename);
    const sheets = currentFile.SheetNames;
    const errorDataList = [];
    const insertDataList = [];
    let isErrorFound = 0;
    let message = "";



    for (let j = 0; j < sheets.length; j++) {

        let date = new Date();
        const nowDateTime = date.getUTCFullYear() + '-' +
            ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
            ('00' + date.getUTCDate()).slice(-2);

        const temp = reader.utils.sheet_to_json(currentFile.Sheets[currentFile.SheetNames[j]]);

        if (isEmpty(temp)) {
            await common_model.deleteFile(fileFilderPath + '/public/upload/', file.filename);
            return res.send({
                "message": "File is empty",
                "success": false
            });
        }

        const insertData = {
            "organization_id": req.decoded.organization_id,
            "user_id": req.decoded.id,
            "disease_id": "",
            "division_id": "",
            "district_id": "",
            "upazila_id": "",
            "name": "",
            "age": "",
            "gender": "",
            "duration": "",
            "season": "",
            "status": 0,
            "date": nowDateTime
        }

        let diseagesList = await disease_model.getDiseagesList();

        for (let i = 0; i < temp.length; i++) {


            message = "";

            let isErrorFoundthisLoop = 0;
            insertData.name = temp[i]["NAME"];
            insertData.date = await common_model.getRandomDate('12/31/2017', nowDateTime);

            // Gender Validation
            if (!temp[i].hasOwnProperty("Gender")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "Gender not found. ";
            } else {
                if (temp[i]["Gender"] === "M" || temp[i]["Gender"] === "F") {
                    insertData.gender = temp[i]["Gender"];
                } else {
                    message += "Gender Found : " + temp[i]["Gender"] + ", which is wrong. ";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                }
            }


            // Season Validation
            if (!temp[i].hasOwnProperty("Season")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "Season not found. ";
            } else {
                if (temp[i]["Season"] === "Summer" || temp[i]["Season"] === "Winter" || temp[i]["Season"] === "Rainy" || temp[i]["Season"] === "Autumn" || temp[i]["Season"] === "Spring") {
                    insertData.season = temp[i]["Season"];
                } else {
                    message += "Season Found : " + temp[i]["Season"] + ", which is Not Our List. ";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                }
            }

            // Disease Validation 
            if (!temp[i].hasOwnProperty("Types of Diseases")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "Types of Diseases not found. ";
            } else {
                for (let j = 0; j < diseagesList.length; j++) {
                    if (diseagesList[j].disease_name.toUpperCase() === temp[i]["Types of Diseases"].toUpperCase()) {
                        insertData.disease_id = diseagesList[j].id;
                        break;
                    }
                }
                if (insertData.disease_id === "") {
                    message += "Disease Found : " + temp[i]["Types of Diseases"] + ", which is unknown.";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                }
            }


            // Number Validation
            if (!temp[i].hasOwnProperty("AGE")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "AGE not found. ";
            } else {
                if (Number(temp[i]["AGE"]) === NaN) {
                    message += "AGE Found : " + temp[i]["AGE"] + ", which is Not a number.";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                    // 
                } else insertData.age = temp[i]["AGE"];
            }


            // Duration Validation
            if (!temp[i].hasOwnProperty("Duration")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "Duration not found. ";
            } else {
                if (Number(temp[i]["Duration"]) === NaN) {
                    message += "Duration Found : " + temp[i]["Duration"] + ", which is Not a number.";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                } else insertData.duration = temp[i]["Duration"];
            }

            if (!temp[i].hasOwnProperty("Divisions")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "Divisions not found. ";
            } else {
                let divisionList = await division_model.getDivisionByName(temp[i]["Divisions"]);

                if (isEmpty(divisionList)) {
                    message += "Division Found : " + temp[i]["Divisions"] + ", which is Not in list.";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                } else {

                    insertData.division_id = divisionList[0].id;

                    let districtList = await district_model.getDistrictByDivisionID(insertData.division_id);
                    let tmp_random_district = await common_model.getRendomData(1, districtList.length);
                    insertData.district_id = tmp_random_district == 0 ? 0 : districtList[tmp_random_district - 1].id;


                    let upazilaList = await upazila_model.getUpazilasByDistrictId(insertData.district_id);
                    let tmp_random_upazila = await common_model.getRendomData(1, upazilaList.length);
                    insertData.upazila_id = tmp_random_upazila == 0 ? 0 : upazilaList[tmp_random_upazila - 1].id;
                }
            }

            if (isErrorFoundthisLoop === 1) {
                errorDataList.push({
                    "row": i,
                    "error": message,
                    "data": temp[i]
                });
            } else {
                insertDataList.push({ ...insertData });
            }
        }
    }

    

    if (isErrorFound === 1) {
        await common_model.deleteFile(fileFilderPath + '/public/upload/', file.filename);
        return res.send({
            "success": false,
            "message": "Error Found Check Error_date. Notes Excle row name must be NAME, AGE, Gender, Types of Diseases, Duration, Divisions & Season",
            "error_data": errorDataList
        });

    } else {
        await upload_file_history_model.addNew({
            "file_name": file.filename,
            "organization_id": req.decoded.organization_id,
            "created_by" :  req.decoded.id
        })
        for (let index = 0; index < insertDataList.length; index++) {
            health_data_model.addNew(insertDataList[index]);
        }
        return res.send({
            "message": "Data Successfully loading ...",
            "success": true
        });
    }

});


router.post('/add', [verifyToken, verifyManagementCantAccess], async (req, res) => {

    let reqData = {
        "disease_id": req.body.disease_id, //
        "division_id": req.body.division_id, //
        "district_id": req.body.district_id, //
        "upazila_id": req.body.upazila_id, //
        "name": req.body.name, //
        "age": req.body.age, //
        "gender": req.body.gender, //
        "duration": req.body.duration,
        "date": req.body.date,
        "season": req.body.season, //
        "organization_id": req.decoded.organization_id, //
        "user_id": req.decoded.id //
    }

    let message = "";
    let isErrorFound = 0;

    // Gender Validation
    if (!(reqData.gender === "M" || reqData.gender === "F")) {
        message += "Gender Found : " + reqData.gender + ", which is wrong. ";
        isErrorFound = 1;
    }

    // Season Validation
    if (!(reqData.season === "Summer" || reqData.season === "Winter" || reqData.season === "Rainy" || reqData.season === "Autumn" || reqData.season === "Spring")) {
        message += "Season Found : " + reqData.season + ", which is Not Our List. ";
        isErrorFound = 1;
    }

    let existDiseaseList = await disease_model.getDiseagesByID(reqData.disease_id);

    if (isEmpty(existDiseaseList)) {
        message += "Disease unknown.";
        isErrorFound = 1;
    }

    let existDivisionList = await division_model.getDivisionByID(reqData.division_id);

    if (isEmpty(existDivisionList)) {
        message += "Division unknown. ";
        isErrorFound = 1;
    }

    let existDistrictList = await district_model.getDistrictByIDAndDivisionID(reqData.district_id, reqData.division_id);

    if (isEmpty(existDistrictList)) {
        message += "Division unknown. ";
        isErrorFound = 1;
    }

    let existUpZilatList = await upazila_model.getUpazilasByIDAndDistrictId(reqData.upazila_id, reqData.district_id);

    if (isEmpty(existUpZilatList)) {
        message += "Upozila is unknown. ";
        isErrorFound = 1;
    }

    if (reqData.name == undefined || reqData.name.trim() < 3) {
        message += "Need valid Patient Name and length must be greater than 2.";
        isErrorFound = 1;
    }

    if (reqData.age === undefined || isNaN(Number(reqData.age) || reqData.age < 0 || reqData.age > 120)) {
        message += "Give a valid age which length in (0 to 120) year.";
        isErrorFound = 1;
    }

    if (reqData.duration === undefined || isNaN(Number(reqData.duration) || reqData.duration < 1 || reqData.duration > 366)) {
        message += "Give a valid duration which length in (1 to 365) days.";
        isErrorFound = 1;
    }

    let m = moment(reqData.date, 'YYYY-MM-DD', true);

    if (!m.isValid()) {
        isErrorFound = 1;
        message += "Date is invalid"

    } else {
        let today = await common_model.getTodayDate();
        let todayDate = moment(today, "YYYY-MM-DD").add(1, 'days');
        // console.log("today: " + todayDate);
        if (!m.isBetween("1999-12-31", todayDate)) {
            isErrorFound = 1;
            message += "Date range must be  '2000-01-01' to '" + today + "'. ";
        }
    }

    if (isErrorFound === 1) {
        return res.send({
            "success": false,
            "message": message
        });
    } else {
        let result = await health_data_model.addNew(reqData);

        if (result.affectedRows == undefined || result.affectedRows < 1) {
            return res.send({
                "success": false,
                "message": "Something Wrong in system database."
            });
        } else {
            return res.send({
                "success": true,
                "message": "Info Data has been Added Successfully."
            });
        }
    }
});


router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

module.exports = router;