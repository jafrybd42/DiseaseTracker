const express = require("express");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const isEmpty = require("is-empty");
const router = express.Router();
const user_model = require('../../model/user');
const role_model = require('../../model/role');
const upload_file_history_model = require('../../model/upload_file_history');
const verifyToken = require('../../jwt/verify/api_v1/verifyToken');
const onlyMTAdminCanAccess = require('../../jwt/verify/api_v1/onlyMTAdminCanAccess');
const verifyParticipantsCantAccess = require('../../jwt/verify/api_v1/verifyParticipantsCantAccess');
const verifyManagementCantAccess = require('../../jwt/verify/api_v1/verifyManagementCantAccess');

const common_model = require('../../common');

router.get('/allFileList', [verifyToken], async (req, res) => {

    const organization_id = req.decoded.organization_id;
    
    let result = await upload_file_history_model.getAllFilesByOrganizationId(organization_id);

    if(isEmpty(result)){
        return res.send({
            "success": false,
            "message": "No files under this organization."
        });
    }

    return res.send({
        "success": true,
        "message": "File List.",
        "count": result.length,
        "data": result
    });
});

router.get('/allFileListByUser', [verifyToken], async (req, res) => {

    let organization_id = req.decoded.organization_id;
    let  user_id = req.decoded.id;
    
    
    let result = await upload_file_history_model.getAllFilesByUserIDAndOrganizationId(organization_id,user_id);

    if(isEmpty(result)){
        return res.send({
            "success": false,
            "message": "You have no files."
        });
    }

    return res.send({
        "success": true,
        "message": "File List.",
        "count": result.length,
        "data": result
    });
});





router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});

router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": false
    })
});


module.exports = router;

