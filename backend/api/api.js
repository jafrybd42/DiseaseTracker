const express = require("express");
const router = express.Router();
const api_v1 = require("./v1/v1")

const connectionBHM = require('../connection/connection').connectionBHM;


router.use('/v1', api_v1);


router.get('/', (req, res) => {
    return res.send({
        "message": "please select api version",
        "api v": 1
    });
});

router.get('/connection_check', (req, res) => {

    try {
        connectionBHM.connect(function(err) {
            if (err) {
                return res.send({
                    "message": "Connection create fail",
                    "error": err,
                    "api v": 1
                });
            }
           
            return res.send({
                "message": "Connection create success",
                "api v": 1
            });
          });

       
    } catch (error) {
        return res.send({
            "message": "Connection create fail",
            "api v": 1
        });
    }
    
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});



router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});



module.exports = router;