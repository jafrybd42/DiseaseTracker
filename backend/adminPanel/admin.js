const express = require("express");
const router = express.Router();

const isEmpty = require("is-empty");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var path = require('path');
const multer = require('multer');


const disease_model = require('../model/diseases');
const division_model = require('../model/divisions');
const district_model = require('../model/districts');
const upazila_model = require('../model/upazilas');
const health_data_model = require('../model/health_data');
const admin_model = require('../model/admin');

const common_model = require('../common');
const verifyTokenWeb = require('../jwt/verify/verifyTokenWeb');

const url = "http://65.0.202.196:3001";
const reader = require('xlsx')
let fileFilderPath = __dirname.slice(0, __dirname.length - 11);


router.get(['/home','/'], verifyTokenWeb, (req, res) => {
    return res.render('home', {
        message: "",
        page: "list",
        success: true
    });
});


router.get("/login", (req, res) => {
    if (req.session.is_login_admin === true) {
        return res.redirect(url + "/home");
    }
    return res.render('login', {
        page: "login",
        message: "",
        "success": false
    });
});


router.post("/login", async (req, res) => {
    let loginData = {
        email: req.body.email,
        password: req.body.password
    }

    let successMessage = "";
    let issuccess = 0;

    if (isEmpty(loginData.email) || loginData.email == undefined) {
        issuccess = 1;
        successMessage += "Email is invalid. ";
    } else {
        const re = /\S+@\S+\.\S+/;
        if (!re.test(loginData.email)) {
            issuccess = 1;
            successMessage += "Email is invalid. ";
        }
    }

    if (isEmpty(loginData.password) || loginData.password == undefined || loginData.password.length < 6) {
        issuccess = 1;
        successMessage += "Need valid password. ";
    }

    if (issuccess == 1) {
        return res.render(url + 'login', {
            "page": "Home",
            "success": true,
            "message": successMessage
        });
    }

    const adminInfo = await admin_model.getAdminByEmail(loginData.email);

    if (isEmpty(adminInfo)) {
        return res.render( url + 'login', {
            "page": "Home",
            "success": true,
            "message": "No user found."
        });
    } else if (adminInfo[0].status != 1) {
        return res.render(url + 'login', {
            "page": "Home",
            "success": true,
            "message": "Sorry Your account is delete."
        });
    }

    // return res.send(adminInfo[0])


    if (bcrypt.compareSync(loginData.password, adminInfo[0].password)) {

        let TokenData = {
            "id": adminInfo[0].id,
            "organization_id": adminInfo[0].organization_id,
            "email": adminInfo[0].email,
            "name": adminInfo[0].name
        }

        if (adminInfo[0].hasOwnProperty("password")) delete adminInfo[0].password;
        if (adminInfo[0].hasOwnProperty("status")) delete adminInfo[0].status;
        if (adminInfo[0].hasOwnProperty("created_at")) delete adminInfo[0].created_at;
        if (adminInfo[0].hasOwnProperty("updated_at")) delete adminInfo[0].updated_at;

        // JSON.parse(JSON.stringify(adminInfo[0]))
        let token = jwt.sign(TokenData, global.config.secretKey, {
            algorithm: global.config.algorithm,
            expiresIn: '300m' //
        });

        req.session.bh_token = token;
        req.session.is_login_admin = true;

        return res.redirect(url + "/home")
    } else {
        return res.render(url + 'login', {
            "page": "Home",
            "success": true,
            "message": "Wrong Password"
        });
    }
});


router.get('/logout', verifyTokenWeb, (req, res) => {
    req.session.destroy(() => {
        console.log("Session distroyed");
    });
    res.redirect(url + '/login');
});


//File upload code

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, fileFilderPath + '/public/upload')
    },
    filename: function (req, file, cb) {
        let extArray = file.mimetype.split("/");
        let extension = extArray[extArray.length - 1];
        cb(null, Date.now() + '-' + file.originalname)
    }
})

const upload = multer({
    storage
});




router.post('/upload_bulk_data', [verifyTokenWeb, upload.single('updateFile')], async (req, res) => {

    const file = req.file;

    if (!file.originalname.match(/\.(xlsx)$/)) {

        await common_model.deleteFile(fileFilderPath + '/public/upload/', file.filename);
        return res.render('Home', {
            "page": "Home",
            "success": false,
            "message": "Not xlsx File.",
            "error_data": []
        });
    }

    const currentFile = reader.readFile(fileFilderPath + '/public/upload/' + file.filename);
    const sheets = currentFile.SheetNames;
    const errorDataList = [];
    const insertDataList = [];
    let isErrorFound = 0;
    let message = "";

    for (let j = 0; j < sheets.length; j++) {


        let date = new Date();
        const nowDateTime = date.getUTCFullYear() + '-' +
            ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
            ('00' + date.getUTCDate()).slice(-2);


        const temp = reader.utils.sheet_to_json(currentFile.Sheets[currentFile.SheetNames[j]]);

        const insertData = {
            "organization_id": 1,
            "disease_id": "",
            "division_id": "",
            "district_id": "",
            "upazila_id": "",
            "name": "",
            "age": "",
            "gender": "",
            "duration": "",
            "season": "",
            "status": 0,
            "date": nowDateTime
        }

        let diseagesList = await disease_model.getDiseagesList();

        for (let i = 0; i < temp.length; i++) {

            message = "";

            let isErrorFoundthisLoop = 0;
            insertData.name = temp[i]["NAME"];
            insertData.date = await common_model.getRandomDate('12/31/2017', nowDateTime);

            // Gender Validation
            if (!temp[i].hasOwnProperty("Gender")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "Gender not found. ";
            } else {
                if (temp[i]["Gender"] === "M" || temp[i]["Gender"] === "F") {
                    insertData.gender = temp[i]["Gender"];
                } else {
                    message += "Gender Found : " + temp[i]["Gender"] + ", which is wrong. ";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                }
            }


            // Season Validation
            if (!temp[i].hasOwnProperty("Season")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "Season not found. ";
            } else {
                if (temp[i]["Season"] === "Summer" || temp[i]["Season"] === "Winter" || temp[i]["Season"] === "Rainy" || temp[i]["Season"] === "Autumn" || temp[i]["Season"] === "Spring") {
                    insertData.season = temp[i]["Season"];
                } else {
                    message += "Season Found : " + temp[i]["Season"] + ", which is Not Our List. ";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                }
            }

            // Disease Validation 
            if (!temp[i].hasOwnProperty("Types of Diseases")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "Types of Diseases not found. ";
            } else {
                for (let j = 0; j < diseagesList.length; j++) {
                    if (diseagesList[j].disease_name.toUpperCase() === temp[i]["Types of Diseases"].toUpperCase()) {
                        insertData.disease_id = diseagesList[j].id;
                        break;
                    }
                }
                if (insertData.disease_id === "") {
                    message += "Disease Found : " + temp[i]["Types of Diseases"] + ", which is unknown.";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                }
            }


            // Number Validation
            if (!temp[i].hasOwnProperty("AGE")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "AGE not found. ";
            } else {
                if (Number(temp[i]["AGE"]) === NaN) {
                    message += "AGE Found : " + temp[i]["AGE"] + ", which is Not a number.";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                    // 
                } else insertData.age = temp[i]["AGE"];
            }


            // Duration Validation
            if (!temp[i].hasOwnProperty("Duration")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "Duration not found. ";
            } else {
                if (Number(temp[i]["Duration"]) === NaN) {
                    message += "Duration Found : " + temp[i]["Duration"] + ", which is Not a number.";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                } else insertData.duration = temp[i]["Duration"];
            }

            if (!temp[i].hasOwnProperty("Divisions")) {
                isErrorFound = 1;
                isErrorFoundthisLoop = 1;
                message += "Divisions not found. ";
            } else {
                let divisionList = await division_model.getDivisionByName(temp[i]["Divisions"]);

                if (isEmpty(divisionList)) {
                    message += "Division Found : " + temp[i]["Divisions"] + ", which is Not in list.";
                    isErrorFound = 1;
                    isErrorFoundthisLoop = 1;
                } else {

                    insertData.division_id = divisionList[0].id;

                    let districtList = await district_model.getDistrictByDivisionID(insertData.division_id);
                    let tmp_random_district = await common_model.getRendomData(1, districtList.length);
                    insertData.district_id = tmp_random_district == 0 ? 0 : districtList[tmp_random_district - 1].id;


                    let upazilaList = await upazila_model.getUpazilasByDistrictId(insertData.district_id);
                    let tmp_random_upazila = await common_model.getRendomData(1, upazilaList.length);
                    insertData.upazila_id = tmp_random_upazila == 0 ? 0 : upazilaList[tmp_random_upazila - 1].id;
                }
            }

            if (isErrorFoundthisLoop === 1) {
                errorDataList.push({
                    "row": i,
                    "error": message,
                    "data": temp[i]
                });
            } else {
                insertDataList.push({ ...insertData });
            }
        }
    }

    await common_model.deleteFile(fileFilderPath + '/public/upload/', file.filename);

    if (isErrorFound === 1) {
        // console.log(errorDataList);
        return res.render('Home', {
            "page": "Home",
            "success": false,
            "message": "Error Found",
            "error_data": errorDataList
        });

    } else {

        for (let index = 0; index < insertDataList.length; index++) {
            health_data_model.addNew(insertDataList[index]);
        }

        return res.render('Home', {
            "page": "Home",
            "message": "",
            "success": true
        });

    }
});


router.get(['/entry'], verifyTokenWeb, (req, res) => {
    return res.render('dataEntry', {
        message: "",
        page: "entry"
    });
});



router.post('/AddMedicalInfo', (req, res) => {
    return res.send({
        "data": req.body
    })
});

router.get('/*', (req, res) => {
    return res.render('404', {
        message: "",
        page: "list"
    });
});



router.post('/*', (req, res) => {
    return res.render('404', {
        message: "",
        page: "list"
    });
});



module.exports = router;