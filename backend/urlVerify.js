var express = require('express');
var router = express.Router();
const requestIp = require('request-ip');

router.use(function (req, res, next) {

    let client_ip = requestIp.getClientIp(req);

    if(client_ip === "::ffff:192.168.0.108" || client_ip === "192.168.0.108"){
        next();
    } else {
        return res.send({
            "success": true,
            "message": "Unknown Request."
        });
    }
});

module.exports = router;