function searchCountries(){
    jQuery.ajax({
        url: "/search_countries",
        type: "POST",
        data: {country_name: jQuery("#countryName").val()},
        dataType: "JSON",
        beforeSend: function(){

        },
        success: function(countriesJson){
        
            jQuery("#accordion").html("");
            if(countriesJson.data.length == 0) {
                return;     
            }
          //  console.log("countriesJson.data.length", countriesJson);

            var i = 0;
            var countryListHtml = "";
            countriesJson.data.forEach(countryElement => {
         
               
               

                var countryRowHtml =  '<div class="card">' ;
                    countryRowHtml += '<div class="card-header" role="tab" id="heading'+ i +'">';
                    countryRowHtml += '<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse'+ i +'" aria-expanded="false" aria-controls="collapse'+ i +'">';
                    countryRowHtml += '<h6 class="mb-0">' +  countryElement.country_name +'<i class="fa fa-angle-down rotate-icon"></i>';
                    countryRowHtml += '</h6>';
                    countryRowHtml += '</a>';
                    countryRowHtml += '</div>';
                    countryRowHtml += '<div id="collapse'+ i +'" class="collapse" role="tabpanel" aria-labelledby="heading'+ i +'"  data-parent="#accordionEx1">';

                    countryRowHtml += '<div class="card-body">';
                   
                    countryRowHtml += ' <table class="table table-striped table-hover">';
                         countryRowHtml += "<tr>"+
                                                "<th>No</th>"+
                                                "<th>Month Name</th>"+
                                                "<th>Adjust Date</th>"+
                                                "<th>Update Date</th>"+
                                            "</tr>";
                          var n = 1 
                             for (j in countryElement.adjust_value[0]) { 
                              countryRowHtml += "<tr>"+
                                    "  <td>  " + n++ + " </td> " +
                                    "  <td>  " + countryElement.adjust_value[0][j].name + " </td> " +
                                    "  <td>  " + countryElement.adjust_value[0][j].adjust + " </td> " +
                                    "  <td>  " + countryElement.adjust_value[0][j].date + " </td> " +
                                 "  </tr> " ;
                             } 
                     countryRowHtml += "</table> </br>";    

                    countryRowHtml += '<form method="POST" action="resetAllData">' + 
                                        '<input type="hidden" value="'+countryElement.country_code +'" name="countryCode">' +
                                        '<input type="submit" value="Reset All Value" >' +
                                    '</form> </br> </br>';

                countryRowHtml += "</div>";
                countryRowHtml += "</div>"; 
                countryRowHtml += "</div>"; 

                countryListHtml += countryRowHtml;
                i++;
            });
            // console.log(countryListHtml);
            jQuery("#accordion").append(countryListHtml);
        }

    });
}

jQuery(document).ready(function(){
    jQuery("#countryName").keyup(function(){
        searchCountries();
    });
});