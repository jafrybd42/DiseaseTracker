# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

##### Backend Deployment #####

## Login to Putty or Bitvise
## Give Port and PPK File
## Login name ubuntu
## cd /opt/bhm/backend
## Install Node and NPM

* sudo  apt update
* sudo curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
* sudo bash nodesource_setup.sh
* sudo apt install nodejs -y
* sudo apt install npm -y

## Clone the project from bitbucket

* sudo git clone copy_the_https

## Install PM2 and Start Project

* sudo npm install -g pm2
* sudo pm2 start app.js
* sudo pm2 list

## Stop app

* sudo pm2 stop app.js

## If pull is needed

* cd /opt/bhm/backend
* sudo git pull
* sudo pm2 list
* sudo pm2 stop app.js
* sudo pm2 list
* sudo pm2 start app.js


### Database Connection File Configuration Backend

```const mysql = require('mysql');

const connectionBHM = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "ems@321",
    database: "bhm"
});



module.exports = {
    connectionBHM
}



### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact