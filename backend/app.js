const express = require("express");
const bodyParser = require('body-parser');
const urlVeryfy = require('./urlVerify');

const ejs = require('ejs');
const cors = require('cors');
const session = require('express-session');

const app = express();
const api = require('./api/api');
const admin = require('./adminPanel/admin');

const port = process.env.PORT || 3001;
const api_version = 1.0;

process.env.TZ = 'Asia/Dhaka' // here is the magical line
global.config = require('./jwt/config/config');





const url = ``;
const base_url = `http://localhost:${port}/admin_panel/`;


// now we get base_url Every There
app.use(function (req, res, next) {
    res.locals.url = base_url;
    next();
});


// set Sesson Data 
app.use(session(
    {
        secret: "Power by Medina tech",
        resave: true,
        saveUninitialized: true
    }
));

app.use(express.static(__dirname + '/public'));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());



app.set('view engine', 'ejs');
// app.set('views', path.join(__dirname, 'views'));

// only in json formate
app.use((err, req, res, next) => {
    if (err) {
        res.status(400).send(
            {
                'message': "error parsing data, Request is not a JSON Formate.",
                "success": true,
                "success": true
            }
        )
    } else {
        next();
    }
});

app.get([url + '/'], (req, res) => {
    return res.send({
        "Hork": "It's Work.",
        "api v": api_version
    });
});


app.use(url + "/api", api);
app.use(url + "/admin_panel", admin);


app.get('/*', (req, res) => {
    return res.status(400).send({
        'message': "unknown route",
        "success": true
    })
});

app.post('/*', (req, res) => {
    return res.send({
        'status': 200,
        'message': "unknown route",
        "success": true
    })
});

app.listen(port, () => {
    console.log(`App running port ${port}`);
});



