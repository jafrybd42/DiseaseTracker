const isEmpty = require("is-empty");
//health_data

let addHealthData = () => {
    return "INSERT INTO `health_data` SET ? ";
}

let getDataBySearching = (sercheingFieldObject = {}) => {

    let query;

    // let keys = Object.keys(sercheingFieldObject);

    /// if only disease_id in search field
    if (isEmpty(sercheingFieldObject.division_id) && isEmpty(sercheingFieldObject.district_id) && isEmpty(sercheingFieldObject.upazila_id)) {
        query = `SELECT  divisions.name as name,
        COUNT(health_data.name) as total_patient from health_data 
        JOIN divisions on divisions.id = health_data.division_id
        where health_data.disease_id = ? and health_data.status = 0 
        GROUP BY (divisions.id) order by health_data.division_id `;
        //console.log(query);
        return query;
    }

    /// if only disease_id and Division ID in search field
    else if (isEmpty(sercheingFieldObject.district_id) && isEmpty(sercheingFieldObject.upazila_id)) {
        query = `SELECT districts.name AS name,COUNT(health_data.name) AS total_patient
                    FROM health_data
                    JOIN diseases ON health_data.disease_id = diseases.id
                    JOIN divisions ON health_data.division_id = divisions.id
                    JOIN districts ON health_data.district_id = districts.id
                    WHERE health_data.status = 0 and
                    health_data.disease_id = ? `;

        if (sercheingFieldObject.season_name !== undefined) {
            // console.log(sercheingFieldObject.season_name.join("','"));
            query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
        }

        // generate where query by search field. 
        let where = ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';


        query += where + "  GROUP BY (health_data.district_id) ORDER BY (health_data.district_id)";
        //console.log(query);
        return query;
    }

    /// if disease_id , division_id , upazila_id in search field

    else if (isEmpty(sercheingFieldObject.upazila_id)) {

        query = `SELECT
                upazilas.name AS name,
                health_data.upazila_id,
                COUNT(health_data.name) AS total_patient
                FROM
                health_data
                JOIN diseases ON health_data.disease_id = diseases.id
                JOIN divisions ON health_data.division_id = divisions.id
                JOIN districts ON health_data.district_id = districts.id
                JOIN upazilas ON health_data.upazila_id = upazilas.id
                WHERE
                    health_data.status = 0 AND health_data.disease_id = ? `;

        if (sercheingFieldObject.season_name !== undefined) {
            // console.log(sercheingFieldObject.season_name.join("','"));
            query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
        }

        // generate where query by search field. 
        let where = ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';

        where += ' and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ') ';


        query += where + " GROUP BY (health_data.upazila_id) ORDER BY (health_data.upazila_id)";
        //console.log(query);
        return query;
    }

    else if ((!isEmpty(sercheingFieldObject.division_id)) && (!isEmpty(sercheingFieldObject.district_id)) && (!isEmpty(sercheingFieldObject.upazila_id))) {
        query = `SELECT
                upazilas.name AS name,
                health_data.upazila_id,
                COUNT(health_data.name) AS total_patient
                FROM
                health_data
                JOIN diseases ON health_data.disease_id = diseases.id
                JOIN divisions ON health_data.division_id = divisions.id
                JOIN districts ON health_data.district_id = districts.id
                JOIN upazilas ON health_data.upazila_id = upazilas.id
                WHERE
                    health_data.status = 0 AND health_data.disease_id = ? `;

        if (sercheingFieldObject.season_name !== undefined) {
            // console.log(sercheingFieldObject.season_name.join("','"));
            query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
        }

        // generate where query by search field. 
        let where = ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';

        where += ' and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ') ';
        where += ' and health_data.upazila_id IN (' + [...sercheingFieldObject.upazila_id] + ') ';


        query += where + " GROUP BY (health_data.upazila_id) ORDER BY (health_data.upazila_id)";
        //console.log(query);
        return query;
    }




}

/// Age Data

let getMaxMinAgeBySelectingDiseaseID = () => {
    return `SELECT  MIN(health_data.age) as patient_min_age , MAX(health_data.age)  as patient_max_age from health_data 
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    where  health_data.status = 0 and health_data.disease_id = ?  `;


}

let getMaxMinAgeBySelectingDiseaseIDandDivisionID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT
    MIN(health_data.age) AS patient_min_age,
    MAX(health_data.age) AS patient_max_age
FROM
    health_data
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    join districts on districts.id = health_data.district_id
WHERE health_data.status = 0  AND
    health_data.disease_id = ? `;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    let where = "";
    where += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';

    query += where;
    //console.log(query);
    return query;
}


let getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT
    MIN(health_data.age) AS patient_min_age,
    MAX(health_data.age) AS patient_max_age
FROM
    health_data
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    join districts on districts.id = health_data.district_id
    JOIN upazilas ON health_data.upazila_id = upazilas.id
WHERE health_data.status = 0  AND
    health_data.disease_id = ?`;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    let where = ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';

    where += ' and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ')  ';

    query += where;
    return query;
}

let getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT
    MIN(health_data.age) AS patient_min_age,
    MAX(health_data.age) AS patient_max_age
FROM
    health_data
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    join districts on districts.id = health_data.district_id
    JOIN upazilas ON health_data.upazila_id = upazilas.id
WHERE health_data.status = 0  AND
    health_data.disease_id = ?`;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    let where = ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';

    where += ' and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ')  ';
    where += ' and health_data.upazila_id IN (' + [...sercheingFieldObject.upazila_id] + ')  ';

    query += where;
    return query;
}


/// Gender Data

let getGenderBySelectingDiseaseID = () => {
    return `SELECT COUNT(health_data.id) as total_patient, health_data.gender from health_data 
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    where health_data.status = 0 and health_data.disease_id = ? 
    Group by health_data.gender`;
}

let getGenderBySelectingDiseaseIDandDivisionID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT COUNT(health_data.id) as total_patient, health_data.gender from health_data 
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    join districts on districts.id = health_data.district_id
    where health_data.status = 0 and health_data.disease_id = ? `;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    let where = "";
    where += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';

    query += where + " Group by health_data.gender";
    //console.log(query)
    return query;
}


let getGenderBySelectingDiseaseIDandDivisionIDandDistrictID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT COUNT(health_data.id) as total_patient, health_data.gender from health_data 
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    join districts on districts.id = health_data.district_id
    JOIN upazilas ON health_data.upazila_id = upazilas.id
    where health_data.status = 0 and health_data.disease_id = ? `;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    let where = "";
    where += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
    where += ' and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ')  ';

    query += where + " Group by health_data.gender";
    return query;
}



let getGenderBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT COUNT(health_data.id) as total_patient, health_data.gender from health_data 
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    join districts on districts.id = health_data.district_id
    JOIN upazilas ON health_data.upazila_id = upazilas.id
    where health_data.status = 0 and health_data.disease_id = ? `;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    let where = "";
    where += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
    where += ' and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ')  ';
    where += ' and health_data.upazila_id IN (' + [...sercheingFieldObject.upazila_id] + ')  ';

    query += where + " Group by health_data.gender";
    return query;
}

/// Season Data

let getSeasonBySelectingDiseaseID = () => {
    return `SELECT COUNT(health_data.id) as total_patient, health_data.season from health_data 
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    where health_data.status = 0 and health_data.disease_id = ? 
    Group by health_data.season`;
}



let getSeasonBySelectingDiseaseIDandDivisionID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT COUNT(health_data.id) as total_patient, health_data.season from health_data 
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    join districts on districts.id = health_data.district_id
    where health_data.status = 0 and health_data.disease_id = ?  `;

    if (sercheingFieldObject.season_name !== undefined) {
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
    query += " Group by health_data.season";
    return query;
}



let getSeasonBySelectingDiseaseIDandDivisionIDandDistrictID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT COUNT(health_data.id) as total_patient, health_data.season from health_data 
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    join districts on districts.id = health_data.district_id
    JOIN upazilas ON health_data.upazila_id = upazilas.id
    where health_data.status = 0 and health_data.disease_id = ?  `;

    if (sercheingFieldObject.season_name !== undefined) {
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
    query += 'and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ')';
    query += " Group by health_data.season";
    return query;
}




let getSeasonBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT COUNT(health_data.id) as total_patient, health_data.season from health_data 
    JOIN diseases on (diseases.id = health_data.disease_id)
    JOIN divisions on divisions.id = health_data.division_id
    join districts on districts.id = health_data.district_id
    JOIN upazilas ON health_data.upazila_id = upazilas.id
    where health_data.status = 0 and health_data.disease_id = ?  `;

    if (sercheingFieldObject.season_name !== undefined) {
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
    query += 'and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ')';
    query += 'and health_data.upazila_id IN (' + [...sercheingFieldObject.upazila_id] + ')';
    query += " Group by health_data.season";
    return query;
}

/// Age Gender Chart



let getAgeGenderChartBySelectingDiseaseID = () => {
    let query = ` SELECT
                    CONCAT(
                        10 * FLOOR(age / 10),
                        '-',
                        10 * FLOOR(age / 10) + 9
                    ) AS rnge,
                    health_data.gender,
                    COUNT(*) AS COUNT
                FROM health_data
                JOIN divisions on divisions.id = health_data.division_id  
                WHERE 
                health_data.status = 0 AND health_data.disease_id = ?
                    
                GROUP BY rnge,
                health_data.gender`


    return query;

}



let getAgeGenderChartBySelectingDiseaseIDandDivisionID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT
            CONCAT(
                10 * FLOOR(age / 10),
                '-',
                10 * FLOOR(age / 10) + 9
            ) AS rnge,
            health_data.gender,
            COUNT(*) AS COUNT
        FROM
            health_data
        JOIN divisions ON divisions.id = health_data.division_id
        JOIN districts ON districts.id = health_data.district_id
        WHERE
            health_data.status = 0 AND disease_id = ? `;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';


    query += " GROUP BY rnge,health_data.gender";
    //console.log(query)
    return query;
}




let getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT
            CONCAT(
                10 * FLOOR(age / 10),
                '-',
                10 * FLOOR(age / 10) + 9
            ) AS rnge,
            health_data.gender,
            COUNT(*) AS COUNT
        FROM
            health_data
        JOIN divisions ON divisions.id = health_data.division_id
        JOIN districts ON districts.id = health_data.district_id
        JOIN upazilas ON health_data.upazila_id = upazilas.id
        WHERE
            health_data.status = 0 AND disease_id = ? `;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
    query += 'and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ')';


    query += " GROUP BY rnge,health_data.gender";
    //console.log(query)
    return query;
}



let getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT
            CONCAT(
                10 * FLOOR(age / 10),
                '-',
                10 * FLOOR(age / 10) + 9
            ) AS rnge,
            health_data.gender,
            COUNT(*) AS COUNT
        FROM
            health_data
        JOIN divisions ON divisions.id = health_data.division_id
        JOIN districts ON districts.id = health_data.district_id
        JOIN upazilas ON health_data.upazila_id = upazilas.id
        WHERE
            health_data.status = 0 AND disease_id = ?`;

    if (sercheingFieldObject.season_name !== undefined) {
        console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
    query += 'and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ')';
    query += 'and health_data.upazila_id IN (' + [...sercheingFieldObject.upazila_id] + ')';

    query += " GROUP BY rnge,health_data.gender";
    //console.log(query)
    return query;
}

/// Affected People on Current Year - Showing Month Wise Data



let getMonthlyDataBySelectingDiseaseID = () => {
    return `SELECT
    COUNT(health_data.id) as total_patient,
    MONTH(health_data.date) AS 'month'
    FROM
    health_data
    JOIN divisions ON divisions.id = health_data.division_id
    WHERE
    YEAR(health_data.date) = YEAR(CURDATE()) AND health_data.disease_id = ? and health_data.status = 0
    GROUP BY MONTH`;
}



let getMonthlyDataBySelectingDiseaseIDandDivisionID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT
            COUNT(health_data.id) AS total_patient,
            MONTH(health_data.date) AS 'month'
            FROM
            health_data
        JOIN divisions ON divisions.id = health_data.division_id
        JOIN districts ON districts.id = health_data.district_id
            WHERE
            YEAR(health_data.date) = YEAR(CURDATE()) AND health_data.disease_id = 2 and health_data.status = 0 `;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';

    query += " Group by MONTH";
    //console.log(query)
    return query;
}



let getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT
            COUNT(health_data.id) AS total_patient,
            MONTH(health_data.date) AS 'month'
            FROM
            health_data
        JOIN divisions ON divisions.id = health_data.division_id
        JOIN districts ON districts.id = health_data.district_id
        JOIN upazilas ON health_data.upazila_id = upazilas.id
            WHERE
            YEAR(health_data.date) = YEAR(CURDATE()) AND health_data.disease_id = 2 and health_data.status = 0 `;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
    query += 'and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ')';


    query += " Group by MONTH";
    //console.log(query)
    return query;
}



let getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID = (sercheingFieldObject = {}) => {

    let query;
    query = `SELECT
            COUNT(health_data.id) AS total_patient,
            MONTH(health_data.date) AS 'month'
            FROM
            health_data
        JOIN divisions ON divisions.id = health_data.division_id
        JOIN districts ON districts.id = health_data.district_id
        JOIN upazilas ON health_data.upazila_id = upazilas.id
            WHERE
            YEAR(health_data.date) = YEAR(CURDATE()) AND health_data.disease_id = 2 and health_data.status = 0 `;

    if (sercheingFieldObject.season_name !== undefined) {
        //console.log(sercheingFieldObject.season_name.join("','"));
        query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
    }

    query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
    query += 'and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ')';
    query += 'and health_data.upazila_id IN (' + [...sercheingFieldObject.upazila_id] + ')';

    query += " Group by MONTH";
    //console.log(query)
    return query;
}

// Diseages

let getDiseagesByID = () => {
    return "SELECT * FROM `diseases` WHERE id = ? and status = 0 ";
}

let getDiseagesByName = () => {
    return "SELECT * FROM `diseases` WHERE disease_name = ? and status = 0 LIMIT 1";
}

let getDiseagesList = () => {
    return "SELECT * FROM `diseases` WHERE status = 0  order by disease_name ASC";
}


let topTenDisease = () => {
    return `SELECT health_data.disease_id,disease_name,COUNT(name) from health_data 
    JOIN diseases on (diseases.id = health_data.disease_id)
    where YEAR(health_data.date) = YEAR(CURDATE()) and health_data.status = 0 
    GROUP BY (health_data.disease_id) order by COUNT(name) DESC LIMIT 10`;
}



let totalDiseasePeople = () => {
    return `SELECT count(name) as NumberOfPeopleAffected from health_data WHERE status = 0`;
}





// Division

let getDivisionByID = () => {

    // console.log("SELECT * FROM `divisions` WHERE name = ? or bn_name = ?  LIMIT 1");
    return "SELECT * FROM `divisions` WHERE id  = ?";
}

let getDivisionByName = () => {

    // console.log("SELECT * FROM `divisions` WHERE name = ? or bn_name = ?  LIMIT 1");
    return "SELECT * FROM `divisions` WHERE name = ? or bn_name = ?  LIMIT 1";
}

let getDivisionList = () => {
    return "SELECT * FROM `divisions` order by name";
}

// Districts

let getDistrictByID = () => {

    // console.log("SELECT * FROM `divisions` WHERE name = ? or bn_name = ?  LIMIT 1");
    return "SELECT * FROM `districts` WHERE id  = ?";
}

let getDistrictByName = () => {
    return "SELECT * FROM `districts` WHERE name = ? or bn_name = ?  LIMIT 1";
}

let getDistrictByDivisionID = () => {
    return "SELECT * FROM `districts` WHERE division_id = ? ";
}

let getDistrictByIDAndDivisionID = () => {
    return "SELECT * FROM `districts` WHERE id = ? and division_id = ?";
}

let getDisByDivID = (division_id) => {
    return 'SELECT * FROM districts WHERE division_id IN (' + [...division_id] + ') ';
}

let getDistrictList = () => {
    return "SELECT * FROM `districts`";
}

// Upazilas
let getUpazilasByName = () => {
    return "SELECT * FROM `upazilas` WHERE name = ? or bn_name = ?  LIMIT 1";
}

let getUpazilasByDistrictId = () => {
    return "SELECT * FROM `upazilas` WHERE district_id = ? ";
}

let getUpazilasByIDAndDistrictId = () => {
    return "SELECT * FROM `upazilas` WHERE id = ? and district_id = ? ";
}

let getUpazilasByDisID = (district_id) => {
    return 'SELECT * FROM upazilas WHERE district_id IN (' + [...district_id] + ') ';
}

let getUpazilasList = () => {
    return "SELECT * FROM `upazilas`";
}


//Admin 

let getAdminByEmail = () => {
    return "SELECT * FROM `admins` where email = ? "
}


let addNewAdmin = () => {
    return "INSERT INTO `admins` SET ? ";
}


// User 

let getUserByPhoneOrEmail = () => {
    return "SELECT id, name, organization_id, roles_id, email, phone_number, password FROM `users` where ( phone_number = ? or email = ? ) and status = 0 ";
}

let getUserByEmail = () => {
    return "SELECT id, name, organization_id, roles_id, email, phone_number, password,status FROM `users` where  email = ?  and status = 0 ";
}

let getUserById = () => {
    return "SELECT id, name, organization_id, roles_id, email, phone_number, password,status FROM `users` where  id = ?  and status = 0";
}


let getUserByIdAndOnganizationId = () => {
    
    return "SELECT id, name, organization_id, roles_id, email, phone_number, status FROM `users` where  id = ? and organization_id = ? ";
}

let getUserList = () => {
    return "SELECT id, name, organization_id, roles_id, email, phone_number, status FROM `users`";
}



let getUserListByRoleTypeStatusAndOrganizationId = () => { // please never change it's order By ID ASC to other
    return "SELECT id, name, organization_id, roles_id, email, phone_number, status FROM `users` where  roles_id = ?  and status = ?  and organization_id = ? order BY id ASC";
}

let getUserListByRoleTypeAndOrganizationId = () => {
    return "SELECT id, name, organization_id, roles_id, email, phone_number, status FROM `users` where  roles_id = ?  and organization_id = ?";
}


let updateUserStatusByIDAndOnganizationId = () => {
    return "UPDATE `users` set status = ? where id = ? and organization_id = ?";
}

let updateUserStatusByRole_IdAndOnganization_Id = () => {
    return "UPDATE `users` set status = ? where roles_id = ? and organization_id = ?";
}



let updateUserStatusByID = () => {
    return "UPDATE `users` set status = ? where id = ?";
}



let registerUserAccount = () => {
    return "INSERT INTO `users` SET ? ";
}

let updateProfileByID = () => {
    return "UPDATE `users` set name = ? where id = ? and organization_id = ?";
}


let updateUser_PasswordByIDAndOrganizationId = () => {
    return "UPDATE `users` set password = ? where id = ? and organization_id = ?";
}



let updateTokenAndExpiredTime = () => {
    return "UPDATE `reset_password` set resetToken  = ? where user_id = ? and id = ? ";
}

let updateUser_PasswordByIDAndToken = () => {
    return "UPDATE `users` set  resetToken = ?,  password = ? where id = ?";
}

let getUserByToken = () => {
    return "SELECT id, user_id, resetToken,expired_time FROM `reset_password` where  resetToken = ?";
}

let insertTokenAndExpiredTime = () => {
    return "INSERT INTO `reset_password` SET ? ";
}

let insertOTPAndExpiredTime = () => {
    return "INSERT INTO `reset_password` SET ? ";
}

let getUserByOTP = () => {
    return "SELECT id, user_id, otp,expired_time FROM `reset_password` where  otp = ? ";
}

let updateOTPAndExpiredTime = () => {
    return "UPDATE `reset_password` set otp  = ? where user_id = ? and id = ? ";
}

// Roles 

let getRoleById = () => {
    return "SELECT * FROM `roles` where id = ? and status = 0";

}

let getRoleByIdAndOrganizationId = () => {
    return "SELECT * FROM `roles` where id = ? and organization_id = ? and status = 0";

}

let getRoleWiseUserCountByOrganizationIdAndStatus = () => {
    return "SELECT roles_id, count(id) as total_user FROM `users` WHERE organization_id = ? and status = ? and roles_id IN (2,3,4,5) GROUP By roles_id";
}

// organization part

let getOrganizationByID = () => {
    return "SELECT * FROM `organizations` where id = ?";
}



let getOrganizationList = () => {
    return "SELECT * FROM `organizations` where status = 0";
}



let getAllOrganizationList = () => {
    return "SELECT * FROM `organizations` ";
}

let getDeactiveOrganizationList = () => {
    return "SELECT * FROM `organizations` where status = 1";
}

let getActiveOrganizationList = () => {
    return "SELECT * FROM `organizations`  where status = 0";
}

let getOrganizationByName = () => {
    return "SELECT * FROM `organizations`  where name = ?";
}

let addNewOrganization = () => {
    return "INSERT INTO `organizations` SET ?";
}

let updateOrganizationByID = () => {
    return "UPDATE `organizations` set name = ? , updated_at = ? where id = ?  ";
}

let deactiveOrganization = () => {
    return "UPDATE `organizations` set status = 1, updated_at = ? where id = ? ";
}

let activateOrganization = () => {
    return "UPDATE `organizations` set status = 0, updated_at = ? where id = ? ";
}

// Package 

let getPackageByID = () => {
    return "SELECT * FROM `packages` where id = ?";
}


let getPackageList = () => {
    return "SELECT id, title, number_of_team_member, number_of_management, number_of_participant, number_of_admin, priority, status  FROM `packages` where status = 0";
}

let getLowestPackage = () => {
    return "SELECT * FROM `packages` where status = 0 ORDER By priority DESC limit 1";
}



let getPackageByTitle = () => {
    return "SELECT * FROM `packages`  where title = ?";
}

let addNewPackage = () => {
    return "INSERT INTO `packages` SET ?";
}

let updatePackageByID = (data) => {

    let keys = Object.keys(data);
    let query = "update `packages` set " + keys[0] + " = ? ";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = ? ";
    }

    query += " where id = ? ";
    return query;
}

// Member Ship enroll 

let addNewMembership = () => {
    return "INSERT INTO `enroll_membership` SET ?";
}

let deleteMembershipById = () => {
    return "Delete FROM `enroll_membership` WHERE id = ?";
}

let getInrollMember_ShipByOnganizationId = () => {
    return "SELECT * FROM `enroll_membership` where organization_id = ? and status = 0";
}

let updateMemberShipID = (data) => {

    let keys = Object.keys(data);
    let query = "update `enroll_membership` set " + keys[0] + " = ? ";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = ? ";
    }

    query += " where id = ? ";
    return query;
}

/// Dashboard Data

let getDataForDashboard = (sercheingFieldObject = {}) => {

    let query;

    query = `SELECT
            COUNT(health_data.id) AS total_case,
            divisions.name AS division_name,
            districts.name AS district_name,
            upazilas.name AS upazilas_name,
            diseases.disease_name AS diseases_name,
            CONCAT(
                10 * FLOOR(health_data.age / 10),
                '-',
                10 * FLOOR(health_data.age / 10) + 9
            ) AS age_range,
            SUM(
                CASE WHEN health_data.gender = 'M' THEN 1 ELSE 0
            END
        ) total_male,
        SUM(
            CASE WHEN health_data.gender = 'F' THEN 1 ELSE 0
        END
        ) total_female,
        health_data.season,health_data.duration,YEAR(health_data.date) as date
        FROM
            health_data
        JOIN divisions ON divisions.id = health_data.division_id
        JOIN districts ON districts.id = health_data.district_id
        JOIN upazilas ON upazilas.id = health_data.upazila_id
        JOIN diseases ON diseases.id = health_data.disease_id

        WHERE health_data.status = 0 AND health_data.disease_id = ? `;

    if (isEmpty(sercheingFieldObject.division_id) && isEmpty(sercheingFieldObject.district_id) && isEmpty(sercheingFieldObject.upazila_id)) {
        if (sercheingFieldObject.season_name !== undefined) {

            query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
        }
        else {
            query += "";
        }

    }
    else if (isEmpty(sercheingFieldObject.district_id) && isEmpty(sercheingFieldObject.upazila_id)) {
        if (sercheingFieldObject.season_name !== undefined) {

            query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
            query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
        }
        else {
            query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
        }

    }

    else if (isEmpty(sercheingFieldObject.upazila_id)) {
        if (sercheingFieldObject.season_name !== undefined) {

            query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
            query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
            query += ' and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ') ';
        }
        else {
            query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
            query += ' and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ') ';
        }
    }
    else if ((!isEmpty(sercheingFieldObject.division_id)) && (!isEmpty(sercheingFieldObject.district_id)) && (!isEmpty(sercheingFieldObject.upazila_id))) {
        if (sercheingFieldObject.season_name !== undefined) {

            query += " and health_data.season IN ('" + sercheingFieldObject.season_name.join("','").toString() + "') ";
            query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
            query += ' and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ') ';
            query += ' and health_data.upazila_id IN (' + [...sercheingFieldObject.upazila_id] + ') ';
        }
        else {
            query += ' and health_data.division_id IN (' + [...sercheingFieldObject.division_id] + ') ';
            query += ' and health_data.district_id IN (' + [...sercheingFieldObject.district_id] + ') ';
            query += ' and health_data.upazila_id IN (' + [...sercheingFieldObject.upazila_id] + ') ';
        }
    }
    query += " GROUP BY health_data.division_id,health_data.district_id,health_data.upazila_id,age_range";

    return query;
}

//   reports

let addNewReport = () => {
    return "INSERT INTO `reports` SET ?";
}

let getReportById = () => {

    let query = `SELECT reports.id,reports.report_type,users.name as createdBy,reports.filtered_data,reports.result FROM reports
    JOIN users ON users.id = reports.created_by
    where reports.id In (SELECT distinct report_id FROM shared_reports WHERE report_id = ? and organization_id = ? and role_type = ?  and shared_user_id IN (?,0)) `;
    return query;

}


let getReportByCreatedBy = () => {

    let query = `SELECT * FROM reports where reports.created_by = ?  `;
    return query;

}


//shared_reports

let addSharedNewReport = () => {
    return "INSERT INTO `shared_reports` SET ?";
}

let getReportListByUserID = () => {

    let query = `SELECT reports.id,reports.report_type,users.name as createdBy FROM reports
    JOIN users ON users.id = reports.created_by
    where reports.id In (SELECT distinct report_id FROM shared_reports WHERE  organization_id = ? and role_type = ?  and shared_user_id IN (?,0)) `;
    return query;

}


let getSharedResultByReportId = () => {

    let query = `SELECT * from shared_reports where report_id = ? `;
    return query;

}

//Reports Downloads History 
let getTotalReportsDownloadHistoryByOgranizationIdAndDate = () => {
    return "SELECT COUNT(id) as total_download FROM `reports_download_history` where organization_id = ? and download_date_time >= ? and download_date_time <= ? ";
}

let addNewReportsDownloadHistory = () => {
    return "INSERT INTO `reports_download_history` SET ?";
}

/// Upload History of Excel Data 

let addBulkFileUploadHistory = () => {
    return "INSERT INTO `file_upload_history` SET ?";
}



let getAllFilesByOrganizationId = () => {
    return `SELECT  file_upload_history.id ,file_upload_history.file_name,users.name as createdBy,file_upload_history.created_at
    FROM file_upload_history 
    JOIN users ON users.id = file_upload_history.created_by
    WHERE file_upload_history.organization_id = ?`;
}

let getAllFilesByUserIDAndOrganizationId = () => {
    return `SELECT  file_upload_history.id,file_upload_history.file_name,users.name as createdBy,file_upload_history.created_at
    FROM file_upload_history 
    JOIN users ON users.id = file_upload_history.created_by
    WHERE file_upload_history.organization_id = ? and file_upload_history.created_by = ?`;
}


module.exports = {

    addHealthData,
    getDataBySearching,

    ///Age  

    getMaxMinAgeBySelectingDiseaseID,
    getMaxMinAgeBySelectingDiseaseIDandDivisionID,
    getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictID,
    getMaxMinAgeBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID,

    ///Gender

    getGenderBySelectingDiseaseID,
    getGenderBySelectingDiseaseIDandDivisionID,
    getGenderBySelectingDiseaseIDandDivisionIDandDistrictID,
    getGenderBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID,

    /// Season

    getSeasonBySelectingDiseaseID,
    getSeasonBySelectingDiseaseIDandDivisionID,
    getSeasonBySelectingDiseaseIDandDivisionIDandDistrictID,
    getSeasonBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID,

    /// Age Gender Chart

    getAgeGenderChartBySelectingDiseaseID,
    getAgeGenderChartBySelectingDiseaseIDandDivisionID,
    getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictID,
    getAgeGenderChartBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID,

    /// Affected People on Current Year - Showing Month Wise Data

    getMonthlyDataBySelectingDiseaseID,
    getMonthlyDataBySelectingDiseaseIDandDivisionID,
    getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictID,
    getMonthlyDataBySelectingDiseaseIDandDivisionIDandDistrictIDandUpazilaID,



    // Diseages
    getDiseagesByID,
    getDiseagesByName,
    getDiseagesList,
    topTenDisease,
    totalDiseasePeople,

    // Division
    getDivisionByID,
    getDivisionByName,
    getDivisionList,

    // districts
    getDistrictByID,
    getDistrictByName,
    getDistrictList,
    getDistrictByDivisionID,
    getDisByDivID,
    getDistrictByIDAndDivisionID,

    // upazilas
    getUpazilasByName,
    getUpazilasByDistrictId,
    getUpazilasByDisID,
    getUpazilasList,
    getUpazilasByIDAndDistrictId,

    //Admin 
    getAdminByEmail,
    addNewAdmin,
    updateUserStatusByID,

    //User 
    getUserByPhoneOrEmail,
    getUserByEmail,
    getUserById,
    registerUserAccount,
    getUserListByRoleTypeStatusAndOrganizationId,
    getUserListByRoleTypeAndOrganizationId,
    updateUserStatusByIDAndOnganizationId,
    getUserByIdAndOnganizationId,
    getUserList,
    updateProfileByID,
    updateUser_PasswordByIDAndOrganizationId,
    updateUserStatusByRole_IdAndOnganization_Id,
    updateTokenAndExpiredTime,
    updateUser_PasswordByIDAndToken,
    getUserByToken,
    insertTokenAndExpiredTime,
    getUserByOTP,
    insertOTPAndExpiredTime,
    updateOTPAndExpiredTime,

    // role 
    getRoleById,
    getRoleByIdAndOrganizationId,
    getRoleWiseUserCountByOrganizationIdAndStatus,

    //Organization
    getOrganizationByID,
    getOrganizationList,
    getAllOrganizationList,
    getDeactiveOrganizationList,
    getActiveOrganizationList,
    getOrganizationByName,
    addNewOrganization,
    updateOrganizationByID,
    deactiveOrganization,
    activateOrganization,

    // Package 
    addNewPackage,
    getPackageByTitle,
    getPackageByID,
    getPackageList,
    updatePackageByID,
    getLowestPackage,

    // Member Ship enroll 
    addNewMembership,
    getInrollMember_ShipByOnganizationId,
    updateMemberShipID,
    deleteMembershipById,


    /// Dashboard
    getDataForDashboard,

    // Report
    addNewReport,
    getReportById,
    getReportByCreatedBy,

    // shared_reports.
    addSharedNewReport,

    getReportListByUserID,
    getSharedResultByReportId,

    //Reports Downloads History 
    addNewReportsDownloadHistory,
    getTotalReportsDownloadHistoryByOgranizationIdAndDate,

    /// Upload History of Excel Data 

    addBulkFileUploadHistory,
    getAllFilesByOrganizationId,
    getAllFilesByUserIDAndOrganizationId 

}