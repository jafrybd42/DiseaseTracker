# BHM - Deployment

✨Login to Putty or BitVise

- Front End Development
- Give port and PPK file
- ✨Login name: ubuntu


## Changes need to Make

- Homepage URL in `package.json`
- Routes path from `src\app\RootRoutes.jsx`


## Build Procedure

```sh
npm run build
```
if it's not work , try :
```sh
npm run winBuild
```

## Add a .htaccess file

Copy the code and paste on .htaccess file

```sh
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteRule ^index\.html$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-l
RewriteRule . /index.html [L]
</IfModule>
```

## Dependecy

| node | v14 |


## Developed By

**BHM Team, Medina Tech**
