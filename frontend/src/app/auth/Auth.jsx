/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { setUserData } from "../redux/actions/UserActions";
import { getNavigationByUser } from "../redux/actions/NavigationAction";
import { loginToken } from "../redux/actions/LoginActions"
import jwtAuthService from "../services/jwtAuthService";
import localStorageService from "../services/localStorageService";
import history from "history.js";

const Auth = (props) => {

  useEffect(() => {

    if (!props.authenticated) {
      props.loginToken()
    }

  }, [props.loginToken, props.authenticated]);

  return (
    <Fragment>
      {props.children}
    </Fragment>
  )
};

const mapStateToProps = state => ({
  loginPhonePass: PropTypes.func.isRequired,
  setUserData: PropTypes.func.isRequired,
  getNavigationByUser: PropTypes.func.isRequired,
  login: state.login,
  authenticated: state.user.token
});

export default connect(mapStateToProps, { loginToken, setUserData, getNavigationByUser })(
  Auth
);
