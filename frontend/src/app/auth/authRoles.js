export const authRoles = {
  sa: ["SA"], // Only Super Admin has access
  admin: ["SA", "ADMIN", 2], // Only SA & Admin has access
  editor: ["SA", "ADMIN", "EDITOR", "SUB-ADMIN", "Management"], // Only SA & Admin & Editor has access
  guest: ["GUEST", "SALES-PERSON", "Participants"], // Everyone has access
  mtAdmin: ["Managing Director", "MT-Admin"],
  team: ["Team Member"],
};

// Check out app/views/dashboard/DashboardRoutes.js
// Only SA & Admin has dashboard access

// const dashboardRoutes = [
//   {
//     path: "/dashboard/analytics",
//     component: Analytics,
//     auth: authRoles.admin <----------------
//   }
// ];
