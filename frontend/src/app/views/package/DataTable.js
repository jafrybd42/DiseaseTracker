import React, { useState } from "react";
import { Button } from "reactstrap";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../auth/api";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import localStorageService from "app/services/localStorageService";
import Swal from "sweetalert2";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";

export default function DataTable(props) {
  const [form, setValues] = useState({
    name: "",
    status: "",
  });
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };
  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };
  const deleteItem = (id, name, status) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, deactive it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/api/v1/organization/delete",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((res) => {
            Swal.fire({
              icon: "success",
              title: "Deactivated",
              showConfirmButton: false,
              timer: 1000,
            });
          })
          .then((item) => {
            props.updateState({
              id,
              name,
              status: 1,
            });
          })
          .catch((err) => console.log(err));
      }
    });
  };

  const activeItem = (id, name, status) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#1a71b5",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, active it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/api/v1/organization/active",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((res) => {
            Swal.fire({
              icon: "success",
              title: "Activated",
              showConfirmButton: false,
              timer: 1000,
            });
          })
          .then((item) => {
            props.updateState({
              id,
              name,
              status: 0,
            });
          });
      }
    });
  };

  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();

  return (
    <>
      <TableContainer
        component={Paper}
        style={{
          boxShadow:
            "0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)",
          backgroundColor: "#fff",
        }}
      >
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "5%" }}
              >
                SL.
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "30%" }}
              >
                Name
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "35%" }}
              >
                Status
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "20%" }}
              >
                Edit
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.items &&
              props.items
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, id) => (
                  <TableRow key={id}>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "5%" }}
                    >
                      {page * rowsPerPage + id + 1}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "30%" }}
                    >
                      {item.name}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "35%" }}
                    >
                      {item.status === 0 ? (
                        <font color="green">Activate</font>
                      ) : (
                        <font color="red">Deactivated</font>
                      )}
                    </TableCell>
                    {item.status === 0 ? (
                      <TableCell
                        className="px-0 capitalize"
                        align="center"
                        style={{ width: "20%" }}
                      >
                        <ModalForm
                          buttonLabel="Edit"
                          item={item}
                          updateState={props.updateState}
                        />
                      </TableCell>
                    ) : (
                      <TableCell
                        className="px-0 capitalize"
                        align="center"
                        style={{ width: "20%" }}
                      >
                        <Button
                          color="btn btn-warning"
                          style={{ marginTop: "7px" }}
                          disabled
                        >
                          Edit
                        </Button>
                      </TableCell>
                    )}

                    {item.status === 0 ? (
                      <TableCell
                        className="px-0 capitalize"
                        align="center"
                        style={{ width: "10%" }}
                      >
                        <Button
                          color="danger"
                          style={{
                            backgroundColor: "#f41b35",
                            borderColor: "#f41b35",
                            width: "100px",
                          }}
                          onClick={() =>
                            deleteItem(item.id, item.name, item.status)
                          }
                        >
                          Deactive
                        </Button>
                      </TableCell>
                    ) : (
                      <TableCell
                        className="px-0 capitalize"
                        align="center"
                        style={{ width: "10%" }}
                      >
                        <Button
                          name="status"
                          color="success"
                          style={{
                            backgroundColor: "#00aa33",
                            borderColor: "#00aa33",
                            width: "100px",
                          }}
                          onClick={() =>
                            activeItem(item.id, item.name, item.status)
                          }
                        >
                          Active
                        </Button>
                      </TableCell>
                    )}
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </>
  );
}
