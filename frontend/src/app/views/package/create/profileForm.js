import React, { useState } from "react";
import axios from "axios";
import myApi from "../../../auth/api";
import { Button, Icon } from "@material-ui/core";
import Swal from "sweetalert2";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateUserProfile } from "app/redux/actions/UserActions";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import history from "../../../../history";

function ProfileForm(props) {
  const [form, setValues] = useState({
    title: "",
    team: "",
    management: "",
    admin: "",
  });

  const onChange = (e) => {
    ValidatorForm.addValidationRule("isValueMatch", (value) => {
      if (value <= -1) {
        return false;
      }

      return true;
    });
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
    console.log(form);
  };

  const handeCreateAction = (e) => {
    axios
      .post(
        myApi + "/api/v1/package/create",
        {
          title: form.title,
          total_team_member: form.team,
          total_management: form.management,
          total_admin: form.admin,
        },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )
      .then((res) => {
        if (!res.data.success) {
          Swal.fire({
            icon: "error",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
          throw new Error(res.data.message);
        } else {
          Swal.fire({
            icon: "success",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000,
          });

          history.push({
            pathname: "/membership",
          });
        }
        if (res.data.success) {
          setValues({
            ...form,
            title: "",
            team: "",
            management: "",
            admin: "",
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  let { title, team, management, admin } = form;
  return (
    <>
      <Grid item xs={12} style={{ margin: "30px" }}>
        <Paper style={{ boxShadow: "none" }}>
          <div>
            <div className="membershipHeader">Add New Membership</div>
            <ValidatorForm
              onSubmit={handeCreateAction}
              onError={(errors) => null}
            >
              <Grid container spacing={6} style={{ marginTop: "10px" }}>
                <Grid item lg={6} md={6} sm={12} xs={12}>
                  <TextValidator
                    autoComplete="off"
                    className="mb-4 w-full"
                    label="Membership Title"
                    onChange={onChange}
                    type="text"
                    name="title"
                    value={title}
                    validators={["required", "maxStringLength: 50"]}
                    errorMessages={["this field is required"]}
                  />

                  <TextValidator
                    autocomplete="off"
                    className="mb-4 w-full"
                    label="Total Team Member (ex : 5)"
                    onChange={onChange}
                    type="number"
                    name="team"
                    value={team}
                    validators={["required", "isValueMatch"]}
                    errorMessages={["this field is required"]}
                  />
                </Grid>
                <Grid item lg={6} md={6} sm={12} xs={12}>
                  <TextValidator
                    autocomplete="password"
                    className="mb-4 w-full"
                    label="Total Managements (ex : 5)"
                    onChange={onChange}
                    name="management"
                    type="number"
                    value={management}
                    validators={["required", "isValueMatch"]}
                    errorMessages={["this field is required"]}
                  />
                  <TextValidator
                    autoComplete="confirmPassword"
                    className="mb-4 w-full"
                    label="Total Admins (ex : 5)"
                    onChange={onChange}
                    name="admin"
                    type="number"
                    value={admin}
                    validators={["required", "isValueMatch"]}
                    errorMessages={["this field is required"]}
                  />
                </Grid>
              </Grid>
              <Button
                color="primary"
                variant="contained"
                type="submit"
                style={{ height: "25px" }}
              >
                <span className="pl-2 small">Create</span>
              </Button>
            </ValidatorForm>
          </div>
        </Paper>
      </Grid>
    </>
  );
}

const mapStateToProps = (state) => ({
  updateUserProfile: PropTypes.func.isRequired,
  user: state.user,
});

export default connect(mapStateToProps, { updateUserProfile })(ProfileForm);
