import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import myApi from "app/auth/api";
import { Button } from "@material-ui/core";
import localStorageService from "app/services/localStorageService";
import history from "../../../history";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: "50px",
    color: "#FFFFFF",
  },
  paper: {
    background: "#484848 0% 0% no-repeat padding-box",
    borderRadius: "15px",
    height: "300px",
    width: "300px",
    textAlign: "center",
    font: "normal normal normal 12px/20px Roboto",
    letterSpacing: "0px",
    color: "#FFFFFF",
  },
  control: {
    padding: theme.spacing(3),
  },
}));

export default function SpacingGrid() {
  const [items, setItems] = useState([]);

  const [spacing, setSpacing] = React.useState(2);
  const classes = useStyles();
  const getItems = () => {
    axios({
      method: "GET",
      url: myApi + "/api/v1/package/list",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        setItems(Array.isArray(response.data.data) ? response.data.data : []);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const onCreate = () => {
    history.push({
      pathname: "/membership/create",
    });
  };
  const onUpdate = () => {
    history.push({
      pathname: "/membership/update",
    });
  };
  const onUpgrade = () => {
    history.push({
      pathname: "/membership/upgrade",
    });
  };
  useEffect(() => {
    getItems();
  }, []);
  return (
    <>
      {localStorageService.getItem("auth_user").role_id === 1 && (
        <div style={{ display: "flex", width: "100%", marginTop: "50px" }}>
          <div style={{ width: "70%" }}></div>
          <div style={{ width: "30%" }}>

            <Button className="downloadBtn" onClick={onCreate}>
              Add New Package
            </Button>
            <Button className="shareBtn" onClick={onUpdate}>
              Update
            </Button>

          </div>
        </div>
      )}
      {localStorageService.getItem("auth_user").role_id !== 1 && (
        <Button className="rightFlex" onClick={onUpgrade}>
          Upgrade Package
        </Button>
      )}
      <div className="membershipTxt">Membership Plans</div>
      <Grid container className={classes.root} spacing={2}>
        <Grid item xs={12}>
          <Grid container justify="center" spacing={spacing}>
            {items.map((e) => (
              <Grid key={e} item>
                <Paper className={classes.paper}>
                  <font className="membershipHeaderTxt">{e.title}</font>
                  <div className="membershipBox">
                    <p>{e.number_of_admin} Admin</p>
                    <p>{e.number_of_team_member} Team Members</p>
                    <p>{e.number_of_management} Management</p>
                    <p>Unlimited Participants</p>
                  </div>
                </Paper>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
