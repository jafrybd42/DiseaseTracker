import React from "react";
import { Container, Row, Col } from "reactstrap";
import ProfileForm from "./profileForm";
import { Card } from "@material-ui/core";
import Breadcrumb from "matx/components/Breadcrumb";

function Sa(props) {
  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Breadcrumb
          routeSegments={[
            { name: "Membership Plan", path: "/membership" },
            { name: "Upgrade Membership", path: "/membership/upgrade" },
          ]}
        />
      </div>
      <div className="mb-sm-30">
        <Container className="App">
          <Row style={{ marginTop: "40px" }}>
            <Card className="px-6 pt-2 pb-4 width50">
              <Col>
                <ProfileForm />
              </Col>
            </Card>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Sa;
