import React, { useState, useEffect } from "react";
import axios from "axios";
import myApi from "../../../auth/api";
import { Button } from "@material-ui/core";
import Swal from "sweetalert2";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateUserProfile } from "app/redux/actions/UserActions";
import { ValidatorForm } from "react-material-ui-form-validator";
import Select from "react-select";

function ProfileForm(props) {
  const [membershipList, setMembershiplist] = useState([]);
  const [selectedID, setSelectedID] = useState([]);
  const [form, setValues] = useState({
    id: "",
    title: "",
    team: "",
    management: "",
    admin: "",
  });

  const handeUpdateAction = (e) => {
    axios
      .post(
        myApi + "/api/v1/package/upgratePackage",
        {
          package_id: form.id,
        },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )
      .then((res) => {
        if (!res.data.success) {
          Swal.fire({
            icon: "error",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
          throw new Error(res.data.message);
        } else {
          Swal.fire({
            icon: "success",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    // List
    axios({
      method: "GET",
      url: myApi + "/api/v1/package/list",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        console.log(response.data.data);
        setMembershiplist(
          Array.isArray(response.data.data)
            ? response.data.data.map((e) => {
                return {
                  value: e.id,
                  label: e.title,
                  number_of_admin: e.number_of_admin,
                  number_of_management: e.number_of_management,
                  number_of_participant: e.number_of_participant,
                  number_of_team_member: e.number_of_team_member,
                  priority: e.priority,
                  status: e.status,
                };
              })
            : []
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }, false);

  const onSelectedTitle = (value) => {
    if (value !== null) {
      setValues({
        ...form,
        id: value.value,
        title: value.label,
      });
    }

    if (value === null) {
      setValues({ ...form, title: "", team: "", admin: "", management: "" });
      setSelectedID("");
    }
  };

  let { title, team, management, admin } = form;

  return (
    <>
      <Grid item xs={12} style={{ margin: "30px" }}>
        <Paper style={{ boxShadow: "none" }}>
          <div>
            <div className="membershipHeader">Upgrade Membership</div>
            <ValidatorForm
              onSubmit={handeUpdateAction}
              onError={(errors) => null}
            >
              <Grid container spacing={6} style={{ marginTop: "10px" }}>
                <Grid item lg={6} md={6} sm={12} xs={12}>
                  <div style={{ marginBottom: "15px", marginTop: "10px" }}>
                    <strong>
                      <font color="black">Select Package</font>
                    </strong>
                  </div>
                  <Select
                    isClearable="true"
                    name="selectedOrganization"
                    options={membershipList}
                    className="basic-multi-select rSelect"
                    classNamePrefix="select"
                    onChange={onSelectedTitle}
                  />
                </Grid>
              </Grid>

              <Button
                color="primary"
                variant="contained"
                type="submit"
                style={{ marginTop: "20px", height: "30px" }}
              >
                <span className="pl-2 small">Upgrade</span>
              </Button>
            </ValidatorForm>
          </div>
        </Paper>
      </Grid>
    </>
  );
}

const mapStateToProps = (state) => ({
  updateUserProfile: PropTypes.func.isRequired,
  user: state.user,
});

export default connect(mapStateToProps, { updateUserProfile })(ProfileForm);
