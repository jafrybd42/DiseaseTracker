/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const MembershipRoutes = [
  {
    path: "/membership/create",
    component: React.lazy(() => import("./create/sa")),
  },
  {
    path: "/membership/update",
    component: React.lazy(() => import("./update/sa")),
  },
  {
    path: "/membership/upgrade",
    component: React.lazy(() => import("./upgrade/sa")),
  },
  {
    path: "/membership",
    component: React.lazy(() => import("./sa")),
  }
];

export default MembershipRoutes;
