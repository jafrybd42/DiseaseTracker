import React, { useState, useEffect } from "react";
import axios from "axios";
import myApi from "../../../auth/api";
import { Button, Icon } from "@material-ui/core";
import Swal from "sweetalert2";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateUserProfile } from "app/redux/actions/UserActions";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import Select from "react-select";
import Loader from "react-loader-spinner";

function ProfileForm(props) {
  const [membershipList, setMembershiplist] = useState([]);
  const [selectedTitle, setSelectedTitle] = useState([]);
  const [getSelectedData, setGetSelectedData] = useState([]);
  const [selectedID, setSelectedID] = useState([]);
  const [form, setValues] = useState({
    title: "",
    team: "",
    management: "",
    admin: "",
  });

  const onChange = (e) => {
    ValidatorForm.addValidationRule("isValueMatchh", (value) => {
      if (value <= -1) {
        return false;
      }

      return true;
    });
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
    console.log(form);
  };

  const handeUpdateAction = (e) => {
    axios
      .post(
        myApi + "/api/v1/package/update",
        {
          id: selectedID,
          title: form.title,
          total_team_member: form.team,
          total_management: form.management,
          total_admin: form.admin,
        },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )
      .then((res) => {
        if (!res.data.success) {
          Swal.fire({
            icon: "error",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
          throw new Error(res.data.message);
        } else {
          Swal.fire({
            icon: "success",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    // List
    axios({
      method: "GET",
      url: myApi + "/api/v1/package/list",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        console.log(response.data.data);
        setMembershiplist(
          Array.isArray(response.data.data)
            ? response.data.data.map((e) => {
                return {
                  value: e.id,
                  label: e.title,
                  number_of_admin: e.number_of_admin,
                  number_of_management: e.number_of_management,
                  number_of_participant: e.number_of_participant,
                  number_of_team_member: e.number_of_team_member,
                  priority: e.priority,
                  status: e.status,
                };
              })
            : []
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }, false);

  const onSelectedTitle = (value) => {
    if (value !== null) {
      axios({
        method: "GET",
        url: myApi + "/api/v1/package/list",
        headers: {
          "x-access-token": localStorage.getItem("jwt_token"),
        },
      })
        .then((response) => {
          setGetSelectedData(
            Array.isArray(response.data.data)
              ? response.data.data
                  .filter((x) => Number(x.id) === value?.value)
                  .map((e) => {
                    return {
                      value: e.id,
                      label: e.title,
                      number_of_admin: e.number_of_admin,
                      number_of_management: e.number_of_management,
                      number_of_participant: e.number_of_participant,
                      number_of_team_member: e.number_of_team_member,
                      priority: e.priority,
                      status: e.status,
                    };
                  })
              : []
          );
          setValues({
            ...form,
            title: value.label,
            team: Number(
              getSelectedData.map((e) => e.number_of_team_member).toString()
            ),
            management: Number(
              getSelectedData.map((e) => e.number_of_management).toString()
            ),
            admin: Number(
              getSelectedData.map((e) => e.number_of_admin).toString()
            ),
          });
        })
        .catch((e) => {
          console.log(e);
        });
    }

    if (value === null) {
      setValues({ ...form, title: "", team: "", admin: "", management: "" });
      setSelectedID("");
    }
  };

  let { title, team, management, admin } = form;

  return (
    <>
      <Grid item xs={12} style={{ margin: "30px" }}>
        <Paper style={{ boxShadow: "none" }}>
          <div>
          <div className="membershipHeader">Update Membership</div>
            <ValidatorForm
              onSubmit={handeUpdateAction}
              onError={(errors) => null}
            >
              <Grid container spacing={6} style={{ marginTop: "10px" }}>
                <Grid item lg={6} md={6} sm={12} xs={12}>
                  <div style={{ marginBottom: "15px", marginTop: "10px" }}>
                    <strong>
                      <font color="black">Select Package</font>
                    </strong>
                  </div>
                  <Select
                    isClearable="true"
                    name="selectedOrganization"
                    options={membershipList}
                    className="basic-multi-select rSelect"
                    classNamePrefix="select"
                    onChange={onSelectedTitle}
                  />
                </Grid>
              </Grid>
              {/* {form.admin ? ( */}
              <Grid container spacing={6} style={{ marginTop: "10px" }}>
                <Grid item lg={6} md={6} sm={12} xs={12}>
                  <TextValidator
                    autoComplete="off"
                    className="mb-4 w-full"
                    label="Membership Title"
                    onChange={onChange}
                    type="text"
                    name="title"
                    value={title}
                    validators={["required", "maxStringLength: 50"]}
                    errorMessages={["this field is required"]}
                  />

                  <TextValidator
                    autocomplete="off"
                    className="mb-4 w-full"
                    label="Total Team Member (ex : 5)"
                    onChange={onChange}
                    type="number"
                    name="team"
                    value={team}
                    validators={["required"]}
                    errorMessages={["this field is required"]}
                  />
                </Grid>
                <Grid item lg={6} md={6} sm={12} xs={12}>
                  <TextValidator
                    autocomplete="password"
                    className="mb-4 w-full"
                    label="Total Managements (ex : 5)"
                    onChange={onChange}
                    name="management"
                    type="number"
                    value={management}
                    validators={["required"]}
                    errorMessages={["this field is required"]}
                  />
                  <TextValidator
                    autoComplete="confirmPassword"
                    className="mb-4 w-full"
                    label="Total Admins (ex : 5)"
                    onChange={onChange}
                    name="admin"
                    type="number"
                    value={admin}
                    validators={["required"]}
                    errorMessages={["this field is required"]}
                  />
                </Grid>
              </Grid>
              {/* ) : (
                <Loader
                  type="ThreeDots"
                  color="#00BFFF"
                  height={80}
                  width={80}
                />
              )} */}
              <Button
                color="primary"
                variant="contained"
                type="submit"
                style={{ height: "25px" }}
              >
                <span className="pl-2 small">Create</span>
              </Button>
            </ValidatorForm>
          </div>
        </Paper>
      </Grid>
    </>
  );
}

const mapStateToProps = (state) => ({
  updateUserProfile: PropTypes.func.isRequired,
  user: state.user,
});

export default connect(mapStateToProps, { updateUserProfile })(ProfileForm);
