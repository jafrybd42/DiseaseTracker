/* eslint-disable no-dupe-keys */
import React from "react";

const ReportRoutes = [
  {
    path: "/report/list",
    component: React.lazy(() => import("./reportList/sa")),
  },
  {
    path: "/report/shared",
    component: React.lazy(() => import("./createdReport/sa")),
  }
];

export default ReportRoutes;