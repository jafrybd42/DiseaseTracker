import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import DataTable from "./DataTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../../auth/api";
import { Button, Card } from "@material-ui/core";
import history from "../../../../history";

function Sa(props) {
  const [items, setItems] = useState([]);

  const getItems = () => {
    axios({
      method: "GET",
      url: myApi + "/api/v1/report/individualReportList",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        setItems(
          Array.isArray(response.data.message) ? response.data.message : []
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };
  const reportList = (event) => {
    event.persist();
    history.push("/report/shared");
  };

  useEffect(() => {
    getItems();
  }, []);

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row style={{ marginBottom: "20px" }}>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Reports", path: "/report/list" },
                  { name: "Reports list" },
                ]}
              />
            </Col>
          </Row>
          <br></br>
          <Row>
            <Button className="rightFlex" onClick={reportList}>
              Shared Report
            </Button>
          </Row>
          <br></br>
          <Row>
            <Card className="px-6 pt-2 pb-4">
              <Col>
                <DataTable
                  items={items}
                  updateState={updateState}
                  deleteItemFromState={deleteItemFromState}
                />
              </Col>
            </Card>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Sa;
