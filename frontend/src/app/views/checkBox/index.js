import React from 'react';

export const CheckBox = props => {
  return (

    <>
      <input
        className="checkBoxText"
        style={{ margin: '10px' }}
        key={props.id}
        onClick={props.handleCheckChieldElement}
        type="checkbox"
        checked={props.isChecked}
        value={props.value}
      />
      {props.value}
      &nbsp;&nbsp;&nbsp;
    </>
  );
};

export default CheckBox;
