import React, { useState, useEffect, useRef } from "react";
import { Container, Row, Col } from "reactstrap";
import DataTablee from "./DataTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";
import { Button, Card, Grid, Icon } from "@material-ui/core";
import Select from "react-select";
import { ValidatorForm } from "react-material-ui-form-validator";
import CheckBox from "../checkBox/index.js";
import localStorageService from "app/services/localStorageService";
import Swal from "sweetalert2";
import ModalForm from "./Modal";
import ModalForm2 from "./ModalUpload";
import ShareForm from "./ShareModal";
import { useReactToPrint } from "react-to-print";
import Loader from "react-loader-spinner";
import DataTable from "./teamTable";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Redirect, withRouter } from "react-router-dom";
import jsPDF from "jspdf";
import "jspdf-autotable";
import moment from "moment";
import history from "../../../history";

function Sa(props) {
  const [selectedSubDivNew, setSelectedSubDivNew] = useState([]);
  const [isLoading, setIsLoading] = useState("");
  const componentRef = useRef();
  const [items, setItems] = useState([]);
  const [responseFilter, setResponseFilter] = useState([]);
  const [filter, setFilter] = useState([]);
  const [selectedDiv, setSelectedDiv] = useState([]);
  const [selectedSubDiv, setSelectedSubDiv] = useState([]);
  const [selectedUpazila, setSelectedUpazila] = useState([]);
  const [locationDivission, setLocationDivision] = useState([]);
  const [form, setValues] = useState({
    selectRef: "",
    selectRefDiv: "",
    selectRefSubDiv: "",
    value: "",
    isDivisionSubmitted: false,
    selectedSubDivisionMap: [],
    selectedDivisionOld: [],
    selectedDivisionNew: [],
    selectedUpazillaNew: [],
    selectedSubDivisionNew: [],
    selectedSubLocation: [],
    selectedMapSubDiv: [],
    selectedSubDivNew: [],
    selectedDivision: [],
    selectedSubDivision: [],
    selectedUpazilla: [],
    propitem: [],
    selectedLocation: [],
    locationDivission: [],
    locationSubDivission: [],
    locationUpazilla: [],
    diseaseList: [],
    seasons: [
      { id: 1, value: "Spring", isChecked: false },
      { id: 2, value: "Summer", isChecked: false },
      { id: 3, value: "Autumn", isChecked: false },
      { id: 4, value: "Winter", isChecked: false },
      { id: 5, value: "Rainy", isChecked: false },
    ],
    selectedSeason: [],
    subDivisonValue: [],
    searchResult: [],
    totalPatient: 0,
    updateGenderChart: [],
    timelineData: [],
    selectedDisease: "",
    diseaseName: "",
  });
  const getItems = () => {

    //get Disease List
    axios
      .get(myApi + "/api/v1/diseaseList")
      .then((response) => {
        Array.isArray(response.data.data) &&
          setValues({
            ...form,
            diseaseList:
              Array.isArray(response.data.data) &&
              response.data.data.map((e) => ({
                value: e.id,
                label: e.disease_name,
                status: e.status,
              })),
          });

        if (form.diseaseList) {
          axios
            .get(myApi + "/api/v1/divisionList")
            .then((response) => {
              setLocationDivision(
                Array.isArray(response.data.data)
                  ? response.data.data.map((e) => ({
                      value: e.id,
                      label: e.name,
                      bangla_label: e.bn_name,
                    }))
                  : []
              );
            })
            .catch((error) => {
              console.log(error);
            });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };
  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };
  const handleCheckChieldElement = (event) => {
    let seasons = form.seasons;
    seasons.forEach((season) => {
      if (season.value === event.target.value)
        season.isChecked = event.target.checked;
    });

    setValues({
      ...form,
      selectedSeason: seasons.map((e) => e.isChecked === true && e.value),
    });
  };
  const onChangeDisease = (value) => {
    if (value !== null && value.length !== 0) {
      //store disease id
      setValues({ ...form, selectedDisease: value?.value });
    }

    if (value === null || value.length === 0) {
      if (form.selectRefDiv) {
        form.selectRefDiv.select.clearValue();
      }
      setValues({
        ...form,
        selectedDisease: "",
      });
    }
  };
  const onChangeDivision = (value) => {
    if (value !== null && value.length !== 0) {
      //get districtList
      axios
        .post(myApi + "/api/v1/districtListByDivisionID", {
          division_id: Array.isArray(value) ? value.map((x) => x.value) : [],
        })
        .then((response) => {
          // eslint-disable-next-line no-unused-expressions
          response.data.data.length !== 0 && Array.isArray(response.data.data)
            ? setValues({
                ...form,
                locationSubDivission: response.data.data.map((e) => ({
                  value: e.id,
                  label: e.name,
                  bangla_label: e.bn_name,
                  division_id: e.division_id,
                })),
                isDivisionSubmitted: true,
                selectedLocation: Array.isArray(value)
                  ? value.map((x) => x.value + "_0_0")
                  : [],
                selectedDivision: Array.isArray(value)
                  ? value.map((x) => x.value)
                  : [],
                selectedDivisionOld: Array.isArray(value)
                  ? value.map((x) => ({
                      value: x.value,
                      label: x.label,
                      division_id: x.division_id,
                    }))
                  : [],
              })
            : [];

          setSelectedDiv(Array.isArray(value) ? value.map((x) => x.value) : []);
        })
        .catch((error) => {
          console.log(error);
        });
    }

    if (value === null) {
      setValues({
        ...form,
        selectedLocation: [],
        locationSubDivission: [],
        isDivisionSubmitted: false,
        selectedSubLocation: [],
        selectedSubDivisionNew: [],
      });
      setSelectedDiv([]);
      if (form.selectRef) {
        form.selectRef.select.clearValue();
      }
      if (form.selectRefSubDiv) {
        form.selectRefSubDiv.select.clearValue();
      }
    }
  };
  const onChangeSubDivision = (value) => {
    if (value !== null && value.length !== 0) {
      let filterArray = form?.locationSubDivission?.map((k) =>
        value
          .map(
            (e) =>
              e !== false &&
              e.value === k.value &&
              k.division_id + "_" + k.value + "_0"
          )
          .filter(Boolean)
          .toString()
      );
      // get upazilla List
      axios
        .post(myApi + "/api/v1/upazilaListByDistrictID", {
          district_id: Array.isArray(value) ? value.map((x) => x.value) : [],
        })
        .then((response) => {
          // eslint-disable-next-line no-unused-expressions
          Array.isArray(response.data.data)
            ? setValues({
                ...form,
                locationUpazilla: response.data.data.map((e) => ({
                  value: e.id,
                  label: e.name,
                  bangla_label: e.bn_name,
                  district_id: e.district_id,
                })),
                selectedSubDivision: Array.isArray(value)
                  ? value.map((x) => x.value)
                  : [],
                selectedSubLocation: filterArray.filter(
                  (e) => e !== false && e
                ),
              })
            : [];

          console.log(form);
        })
        .catch((error) => {
          console.log(error);
        });

      setSelectedSubDiv(Array.isArray(value) ? value.map((x) => x.value) : []);
      setSelectedSubDivNew(
        Array.isArray(value)
          ? value.map((x) => ({
              value: x.value,
              label: x.label,
              // division_id: x.division_id,
            }))
          : []
      );
      console.log(selectedSubDivNew);
    }

    if (value === null) {
      setValues({
        ...form,
        selectedSubDivision: [],
        selectedSubLocation: [],
        locationUpazilla: [],
      });
      setSelectedSubDiv([]);
      if (form.selectRef) {
        form.selectRef.select.clearValue();
      }
    }
  };
  const onChangeUpazila = (value) => {
    if (value !== null) {
      setValues({
        ...form,
        selectedUpazilla: Array.isArray(value) ? value.map((x) => x.value) : [],
        selectedUpazillaNew: Array.isArray(value)
          ? value.map((x) => ({
              value: x.value,
              label: x.label,
              district_id: x.district_id,
            }))
          : [],
      });
      setSelectedUpazila(Array.isArray(value) ? value.map((x) => x.value) : []);
    }
    if (value === null) {
      setValues({ ...form, selectedUpazilla: [], selectedUpazillaNew: [] });
      setSelectedUpazila([]);
    }
  };
  const handleAllChecked = (event) => {
    let seasons = form.seasons;
    seasons.forEach((season) => (season.isChecked = event.target.checked));
    setValues({ ...form, seasons: seasons });
    setValues({
      ...form,
      selectedSeason: form.seasons.map((e) => e.isChecked === true && e.value),
    });
  };
  const handleFilterAction = (event) => {
    event.persist();
    if (form.selectedDisease) {
      setIsLoading(true);
    }
    let filterSeason = form?.selectedSeason.filter((e) => e !== false);

    let filterData = {
      disease_id: form.selectedDisease,
      division_id: selectedDiv,
      district_id: selectedSubDiv,
      upazila_id: selectedUpazila,
      season_name: filterSeason,
    };

    if (filterData.division_id.length === 0) {
      delete filterData.division_id;
      delete filterData.district_id;
      delete filterData.upazila_id;
    }

    //API calling --> filter
    axios
      .post(myApi + "/api/v1/dashboard/dashboardData", filterData, {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((response) => {
        setItems(
          Array.isArray(response.data.data)
            ? response.data.data.map((index) => index)
            : []
        );

        setIsLoading(false);

        if (response.data.count === 0) {
          Swal.fire({
            icon: "error",
            title: "No data available!",
            showConfirmButton: false,
            timer: 1500,
          });
        }
        if (response.data.count !== 0) {
          setResponseFilter(
            Array.isArray(response.data.data) ? response.data : []
          );
          // props.DataTable(response);
          setFilter(filterData);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  const exportPDF = () => {
    const filterPosition = [90, 103, 116, 129, 142, 155];
    let filterPositionPointer = 0;

    const locateValueById = (types, value) => {
      let item = types.find((it) => it.value === Number(value));
      return item;
    };
    const addFooters = (doc) => {
      const pageCount = doc.internal.getNumberOfPages();
      doc.setFont("helvetica", "italic");
      doc.setFontSize(8);
      for (var i = 1; i <= pageCount; i++) {
        doc.setPage(i);
      }
    };

    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 40;
    const doc = new jsPDF(orientation, unit, size);
    doc.setFontSize(15);

    const title = "Data Table Report";
    const filterDisease =
      form.selectedDisease !== ""
        ? locateValueById(form.diseaseList, form.selectedDisease) &&
          locateValueById(form.diseaseList, form.selectedDisease).label
        : "";
    const filterDivision =
      selectedDiv && selectedDiv?.length !== 0
        ? form.selectedDivisionOld.map(function (item, index) {
            return (index ? ", " : "") + item.label;
          })
        : "";
    const filterDistrict =
      selectedSubDivNew && selectedSubDivNew?.length !== 0
        ? selectedSubDivNew.map(function (item, index) {
            return (index ? ", " : "") + item.label;
          })
        : "";
    const filterUpazila =
      form.selectedUpazillaNew && form.selectedUpazillaNew?.length !== 0
        ? form.selectedUpazillaNew.map(function (item, index) {
            return (index ? ", " : "") + item.label;
          })
        : "";
    doc.setFont("helvetica", "strong");
    doc.setFontSize(11);
    doc.text("Search By : ", marginLeft, 70);
    doc.setFont("helvetica", "regular");
    doc.setFontSize(9);
    doc.text(
      "Generated On : " + moment(new Date()).format("YYYY-MM-DD"),
      marginLeft,
      50
    );

    doc.setFont("helvetica", "normal");
    doc.setFontSize(10);
    form.selectedDisease &&
      doc.text(
        "\r\nDisease : " + filterDisease,
        marginLeft,
        filterPosition[filterPositionPointer++]
      );
    selectedDiv &&
      selectedDiv.length !== 0 &&
      doc.text(
        "\r\nDivision : " + filterDivision,
        marginLeft,
        filterPosition[filterPositionPointer++]
      );

    selectedSubDiv &&
      selectedSubDiv.length !== 0 &&
      doc.text(
        "\r\nDistrict : " + filterDistrict,
        marginLeft,
        filterPosition[filterPositionPointer++]
      );

    selectedUpazila &&
      selectedUpazila.length !== 0 &&
      doc.text(
        "\r\nUpazila : " + filterUpazila,
        marginLeft,
        filterPosition[filterPositionPointer++]
      );

    const headers = [
      [
        "SL",
        "Disease",
        "Division",
        "District",
        "Upazila",
        "Age Range	",
        "Season",
        "Duration",
        "Year",
        "Cases (Total)	",
      ],
    ];

    const data = items.map((ap, i) => [
      i + 1,
      ap.diseases_name,
      ap.division_name,
      ap.district_name,
      ap.upazilas_name,
      ap.age_range,
      ap.season,
      ap.duration,
      ap.date,
      ap.total_case,
    ]);

    doc.setFont("helvetica", "bold");
    doc.setFontSize(15);
    doc.text(title, marginLeft, 35);
    doc.setFont("helvetica", "normal");
    doc.setFontSize(10);

    //end
    let content = {
      startY: 30 + filterPosition[filterPositionPointer++],
      head: headers,
      body: data,
      margin: {
        bottom: 120,
        top: 100,
      },
    };
    doc.autoTable(content);

    doc.setTextColor(100);
    doc.setFontSize(10);

    addFooters(doc);

    doc.save(
      "data_table_report(" + moment(new Date()).format("YYYY-MM-DD") + ").pdf"
    );
  };
  useEffect(() => {
    getItems();
    setIsLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row style={{ marginBottom: "20px" }}>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "BHM", path: "/" },
                  { name: "Data Table" },
                ]}
              />
            </Col>
          </Row>
          <br></br>
          <Row>
            <div style={{ display: "flex", width: "100%" }}>
              <div style={{ width: "50%" }}></div>
              <div style={{ width: "50%" }}>
                {items.length !== 0 && (
                  <ShareForm
                    type={Number(2)}
                    items={items}
                    filter={filter}
                    count={responseFilter.count}
                    message={responseFilter.message}
                    isSuccess={responseFilter.success}
                    buttonLabel="Share"
                  />
                )}
                {items.length !== 0 && (
                  <Button className="downloadBtn" onClick={() => exportPDF()}>
                    <Icon fontSize="small">get_app</Icon>&nbsp;Download Report
                  </Button>
                )}
                {items.length !== 0 && (
                  <Button className="shareBtn">
                    <Icon fontSize="small">print</Icon>&nbsp;Print
                  </Button>
                )}
                {items.length !== 0 ? (
                  <ModalForm buttonLabel="Upload Data" />
                ) : (
                  <ModalForm2 buttonLabel="Upload Data" />
                )}
              </div>
            </div>
          </Row>
          <br></br>
          <Row>
            <Card className="px-6 pt-2 pb-4">
              <div className="tableBox" style={{ height: "210px" }}>
                <ValidatorForm
                  style={{ width: "90%", display: "block" }}
                  onSubmit={handleFilterAction}
                  onError={(errors) => errors.null}
                >
                  {/* location field (divission, sub-divission, upazila) */}
                  <div className="textStyleFilter">Filter</div>
                  <Grid
                    item
                    xs={12}
                    lg={12}
                    className="flexItem"
                    style={{ display: "flex" }}
                  >
                    <Select
                      className="marginSelect"
                      placeholder="Select Disease"
                      options={form.diseaseList}
                      onChange={onChangeDisease}
                      isClearable="true"
                    />
                    {form.selectedDisease && locationDivission?.length !== 0 ? (
                      <Select
                        ref={(ref) => {
                          form.selectRefDiv = ref;
                        }}
                        className="marginSelect"
                        placeholder="Select Division"
                        options={locationDivission}
                        onChange={onChangeDivision}
                        isClearable="true"
                        isMulti
                      />
                    ) : (
                      <Select
                        className="marginSelect"
                        placeholder="Select Division"
                        isDisabled
                      />
                    )}
                    {form.selectedLocation.length !== 0 &&
                    form.isDivisionSubmitted ? (
                      <Select
                        ref={(ref) => {
                          form.selectRefSubDiv = ref;
                        }}
                        className="marginSelect"
                        classNamePrefix="select"
                        placeholder="Select District"
                        options={form.locationSubDivission}
                        onChange={onChangeSubDivision}
                        isMulti
                        isClearable="true"
                      />
                    ) : (
                      <Select
                        className="marginSelect"
                        value={null}
                        placeholder="Select District"
                        options={form.locationSubDivission}
                        isClearable="true"
                        isDisabled
                      />
                    )}
                    {form.selectedSubLocation.length !== 0 ? (
                      <Select
                        ref={(ref) => {
                          form.selectRef = ref;
                        }}
                        className="marginSelect"
                        placeholder="Select Upazila"
                        options={form.locationUpazilla}
                        onChange={onChangeUpazila}
                        isClearable="true"
                        isMulti
                      />
                    ) : (
                      <Select
                        className="marginSelect"
                        placeholder="Select Upazila"
                        // options={form.locationUpazila}
                        isClearable="true"
                        isMulti
                        isDisabled
                      />
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    lg={12}
                    className="flexItem"
                    style={{ display: "flex" }}
                  >
                    <div className="textStyleFilter">Seasons :</div>
                    <div className="checkBoxMargin checkBoxText">
                      <input
                        style={{ margin: "10px", marginTop: "-10px" }}
                        className="checkBoxText"
                        type="checkbox"
                        onClick={handleAllChecked}
                        value="checkedall"
                      />
                      Select All
                      <ul className="checkBoxText">
                        {form.seasons.map((season) => {
                          return (
                            <CheckBox
                              className="checkBoxText"
                              handleCheckChieldElement={
                                handleCheckChieldElement
                              }
                              {...season}
                            />
                          );
                        })}
                      </ul>
                    </div>
                    <Button
                      className="rightAlignn"
                      variant="contained"
                      type="submit"
                      color="primary"
                      onClick={handleFilterAction}
                    >
                      Filter
                    </Button>
                  </Grid>
                </ValidatorForm>
              </div>
              <Col>
                {isLoading ? (
                  <Loader
                    type="ThreeDots"
                    color="#00BFFF"
                    height={80}
                    width={80}
                  />
                ) : (
                  <DataTablee
                    items={items}
                    updateState={updateState}
                    deleteItemFromState={deleteItemFromState}
                  />
                )}
              </Col>
            </Card>
          </Row>
        </Container>
      </div>
    </div>
  );
}

// export default Sa;
const mapStateToProps = (state) => ({
  DataTable: PropTypes.func.isRequired,
});

export default withRouter(connect(mapStateToProps, { DataTable })(Sa));
