/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const DataManagementRoutes = [
  {
    path: "/data/filter",
    component: React.lazy(() => import("./sa")),
  },
  {
    path: "/data/input",
    component: React.lazy(() => import("./singleData/sa")),
  },

];

export default DataManagementRoutes;
