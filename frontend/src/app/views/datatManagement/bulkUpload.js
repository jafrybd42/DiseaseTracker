import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import myApi from "../../auth/api";
import localStorageService from "app/services/localStorageService";
import LinearProgress from "@material-ui/core/LinearProgress";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { Button } from "@material-ui/core";
import Swal from "sweetalert2";
import ProgressBar from "@ramonak/react-progress-bar";
import Loader from "react-loader-spinner";

function AddEditForm(props) {
  const [file, setFile] = useState(""); // storing the uploaded file
  const [myValue, mySetValue] = useState(""); // storing the uploaded file
  const [isLoading, setIsLoading] = useState("");
  const el = useRef(); // accesing input element

  const handleChange = (e) => {
    const file = e.target.files[0]; // accessing file
    setFile(file); // storing file
  };

  const uploadFile = () => {
    setIsLoading(true);
    const formData = new FormData();
    formData.append("update_bulk_File", file); // appending file
    axios
      .post(myApi + "/api/v1/health_data/bulk_upload", formData, {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        // console.log(res);
        if (res?.data?.success) {
          setIsLoading(false);
          mySetValue(Number(100));
          Swal.fire({
            icon: "success",
            title: "Upload Successfully Done",
            showConfirmButton: false,
            timer: 1500,
          });
          props.toggle();
        } else {
          setIsLoading(false);
          Swal.fire({
            icon: "error",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1500,
          });
        }
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    setIsLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <div className="file-upload">
        <label>
          <font className="uploadTxt">Upload</font>
        </label>
        <input
          className="mb-4 w-full uploadInput"
          type="file"
          ref={el}
          onChange={handleChange}
          accept=".xls,.xlsx"
        />
        <label for="file-upload" class="file-upload" color="red">
          ** file must be .xlsx format
        </label>
        <br />

        {isLoading ? (
          <Loader type="ThreeDots" color="#00BFFF" height={80} width={80} />
        ) : (
          <Button
            className="uploadButton"
            onClick={uploadFile}
            color="primary"
            variant="contained"
            type="submit"
          >
            Upload
          </Button>
        )}
        <hr />
      </div>
    </div>
  );
}

export default AddEditForm;
