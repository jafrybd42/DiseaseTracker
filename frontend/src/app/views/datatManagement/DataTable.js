import React, { useState, useRef } from "react";
import axios from "axios";
import myApi from "../../auth/api";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import Swal from "sweetalert2";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import { useReactToPrint } from "react-to-print";
import { Alert, Pagination } from "@material-ui/lab";

export default function DataTable(props) {
  const [form, setValues] = useState({
    name: "",
    status: "",
  });
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  // const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const [pageSize, setPageSize] = useState(3);
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };
  const getRequestParams = (searchTitle, page, pageSize) => {
    let params = {};

    if (searchTitle) {
      params["title"] = searchTitle;
    }

    if (page) {
      params["page"] = page - 1;
    }

    if (pageSize) {
      params["size"] = pageSize;
    }

    return params;
  };
  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  // const handlePageChange = (event, value) => {
  //   setPage(value);
  // };

  // const handlePageSizeChange = (event) => {
  //   setPageSize(event.target.value);
  //   setPage(1);
  // };

  return (
    <>
      {props.items.length !== 0 && (
        <TableContainer
          component={Paper}
          style={{
            boxShadow:
              "0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)",
            backgroundColor: "#fff",
          }}
        >
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center" className="px-0">
                  SL.
                </TableCell>
                <TableCell align="center" className="px-0">
                  Disease
                </TableCell>
                <TableCell align="center" className="px-0">
                  Division
                </TableCell>
                <TableCell align="center" className="px-0">
                  District
                </TableCell>
                <TableCell align="center" className="px-0">
                  Upazila
                </TableCell>
                <TableCell align="center" className="px-0">
                  Age Range
                </TableCell>
                <TableCell align="center" className="px-0">
                  Season
                </TableCell>
                <TableCell align="center" className="px-0">
                  Duration
                </TableCell>
                <TableCell align="center" className="px-0">
                  Year
                </TableCell>
                <TableCell align="center" className="px-0">
                  Cases (Total)
                </TableCell>
                <TableCell align="center" className="px-0">
                  Male (Total)
                </TableCell>
                <TableCell align="center" className="px-0">
                  Female (Total)
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.items &&
                props.items
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((item, id) => (
                    <TableRow key={id}>
                      <TableCell className="px-0 capitalize" align="center">
                        {page * rowsPerPage + id + 1}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.diseases_name}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.division_name}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.district_name}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.upazilas_name}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.age_range}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.season}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.duration}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.date}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.total_case}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.total_male}
                      </TableCell>
                      <TableCell className="px-0 capitalize" align="center">
                        {item.total_female}
                      </TableCell>
                      {/* )} */}
                    </TableRow>
                  ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
      {/* {props.items.length === 0 && (
        <Alert severity="info">No data available !</Alert>
      )} */}
      {props?.items?.length > 10 && (
        <TablePagination
          style={{
            maxWidth: 400,
            overflowX: "hidden",
            padding: "0px!important",
            display: "contents",
          }}
          className="px-4"
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.items.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page",
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page",
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />

        // <Pagination
        //   className="my-3"
        //   count={Math.round(props.items.length / 10)}
        //   page={page}
        //   siblingCount={1}
        //   boundaryCount={1}
        //   variant="outlined"
        //   shape="rounded"
        //   onChange={handleChangePage}
        // />
      )}
    </>
  );
}
