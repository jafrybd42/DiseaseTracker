import React, { useEffect, useState } from "react";
import { Button } from "reactstrap";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../auth/api";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
  Checkbox,
  Icon,
} from "@material-ui/core";
import localStorageService from "app/services/localStorageService";
// import Swal from "sweetalert2";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import SearchBar from "material-ui-search-bar";
import { Link } from "react-router-dom";
import CheckBox from "../checkBox";
import Swal from "sweetalert2";

export default function AdminTable(props) {
  console.log(props);
  const [organizationList, setOrganizationList] = useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [appData, setData] = React.useState([]);
  const [checkedData, setCheckedData] = React.useState([]);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };
  useEffect(() => {
    setData(props?.items);
    // getting clientType
    axios
      .get(myApi + "/api/v1/organization/allList", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let orgsFromApi = res.data.data.map((org) => {
          return {
            id: org.id,
            type: org.name,
          };
        });
        setOrganizationList([].concat(orgsFromApi));
        setOrganizationList(
          [
            {
              id: 0,
              type: "Organization List",
            },
          ].concat(orgsFromApi)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }, false);

  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();
  const locateValueById = (types, id) => {
    let item = types.find((it) => it.id === Number(id));
    return item;
  };
  const checkOne = (id) => {
    const tempData = appData.map((el) => {
      if (el.id === id) {
        return { ...el, checked: !el.checked };
      }
      return el;
    });
    setData(tempData);
    setCheckedData(
      appData
        .map((el) => {
          if (el.id === id) {
            return { ...el, checked: !el.checked };
          }
          return el;
        })
        .map((e) => e.checked === true && e.id)
    );
  };
  const selectAll = () => {
    const tempData = appData.map((el) => ({ ...el, checked: true }));
    setData(tempData);
    setCheckedData(
      appData
        .map((el) => ({ ...el, checked: true }))
        .map((e) => e.checked === true && e.id)
    );
    console.log(checkedData);
  };
  const onShareAction = (event) => {
    event.persist();

    let data = {
      report_type: 2,
      shared_management: [],
      shared_team_member: [],
      shared_admin: checkedData?.filter((e) => e !== false),
      filter: props.filter,
      data: {
        success: true,
        message: props.message,
        count: props.count,
        data: props.data,
      },
    };

    //posting to API
    axios({
      method: "POST",
      url: myApi + "/api/v1/report/share_reports",
      data,
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        console.log(response);
        if (response.data.success) {
          props.toggle();

          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500,
          });
          console.log(true);
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500,
          });
          console.log(false);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <>
      <Button className="upBtnSecond" color="success" onClick={onShareAction}>
        <Icon fontSize="small" style={{ marginBottom: "-6px" }}>
          share
        </Icon>
        &nbsp; Share
      </Button>
      <TableContainer
        component={Paper}
        style={{
          boxShadow:
            "0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)",
          backgroundColor: "#fff",
        }}
      >
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "5%" }}
              >
                SL.
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "20%" }}
              >
                Name
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "15%" }}
              >
                Phone
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "15%" }}
              >
                Email
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "15%" }}
              >
                Status
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "20%" }}
              >
                Organization
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                <input
                  type="checkbox"
                  checked={appData.every((el) => el.checked)}
                  onChange={selectAll}
                />{" "}
                Select all
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {appData.length !== 0 &&
              appData
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, id) => (
                  <TableRow key={id}>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "5%" }}
                    >
                      {page * rowsPerPage + id + 1}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "20%" }}
                    >
                      {item.name}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "15%" }}
                    >
                      {item.phone_number}
                    </TableCell>
                    <TableCell
                      className="px-0"
                      align="center"
                      style={{ width: "15%" }}
                    >
                      {item.email}
                    </TableCell>
                    <TableCell
                      className="px-0"
                      align="center"
                      style={{ width: "15%" }}
                    >
                      {item.status === 0 ? (
                        <font color="green">Active</font>
                      ) : (
                        <font color="red">Deactivated</font>
                      )}
                    </TableCell>

                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "20%" }}
                    >
                      {locateValueById(
                        organizationList,
                        item.organization_id
                      ) &&
                        locateValueById(organizationList, item.organization_id)
                          .type}
                    </TableCell>
                    {/* {item.status === 0 ? ( */}
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "10%" }}
                    >
                      <input
                        type="checkbox"
                        checked={item.checked}
                        onChange={() => checkOne(item.id)}
                      />
                    </TableCell>
                    {/* )} */}
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </>
  );
}
