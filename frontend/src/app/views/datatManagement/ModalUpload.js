import { Icon } from "@material-ui/core";
import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import AddEditForm from "./bulkUpload";

function ModalForm(props) {
  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };

  const closeBtn = (
    <button className="close" onClick={toggle}>
      &times;
    </button>
  );
  const label = props.buttonLabel;

  let button = "";
  let title = "";

  if (label === "Edit") {
    button = (
      <Button
        color="warning"
        onClick={toggle}
        style={{ float: "left", marginRight: "10px" }}
      >
        {label}
      </Button>
    );
    title = "Edit Type";
  } else {
    button = (
      <Button
        className="downloadBtn"
        color="success"
        onClick={toggle}
        style={{
          width: "100%",
          position: "absolute",
          right: "9%",
          marginTop: "-30px",
        }}
      >
        {label}
      </Button>
    );
    title = "Upload File";
  }

  return (
    <>
      {button}
      <Modal
        isOpen={modal}
        toggle={toggle}
        className={props.className}
        style={{ maxWidth: "450px" }}
      >
        {/* <ModalHeader toggle={toggle} close={closeBtn}>
          {title}
        </ModalHeader> */}
        <ModalBody>
          <AddEditForm
            addItemToState={props.addItemToState}
            updateState={props.updateState}
            toggle={toggle}
            item={props.item}
          />
        </ModalBody>
      </Modal>
    </>
  );
}

export default ModalForm;
