import React from "react";
import { Container, Row, Col } from "reactstrap";
import ProfileForm from "./Form";
import { Card } from "@material-ui/core";
import { Breadcrumb } from "matx";

function Sa(props) {
  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <div className="mb-sm-30">
            <Breadcrumb
              routeSegments={[
                { name: "Dashboard", path: "/" },
                { name: "Single Data Entry" },
              ]}
            />
          </div>
          <Row style={{ marginTop: "40px" }}>
            <Card className="px-6 pt-2 pb-4 width50">
              <Col>
                <ProfileForm />
              </Col>
            </Card>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Sa;
