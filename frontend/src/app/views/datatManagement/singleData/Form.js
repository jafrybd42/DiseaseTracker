import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import myApi from "../../../auth/api";
import { Button } from "@material-ui/core";
import Swal from "sweetalert2";
import Grid from "@material-ui/core/Grid";
import localStorageService from "app/services/localStorageService";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateUserProfile } from "app/redux/actions/UserActions";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import Select from "react-select";
import DateFnsUtils from "@date-io/date-fns";

import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import moment from "moment";

function ProfileForm(props) {
  const districtRef = useRef();
  const divisionRef = useRef();
  const upazilaRef = useRef();
  const seasonRef = useRef();
  const genderRef = useRef();
  const diseaseRef = useRef();

  let { user } = props;
  const [diseaseList, setDiseaseList] = useState([]);
  const [divisionList, setDivisionList] = useState([]);
  const [form, setValues] = useState({
    name: user.displayName,
    diseaseList: [],
    divisionList: [],
    selectedDisease: "",
    selectedDivision: "",
    selectedDistrict: "",
    selectedUpazila: "",
    districtList: "",
    upazilaList: "",
    isDivisionSubmitted: false,
    isUpazilaSubmitted: false,
    patientName: "",
    patientAge: "",
    patientGender: "",
    gender: [
      { value: "M", label: "Male" },
      { value: "F", label: "Female" },
    ],
    patientDuration: "",
    date: new Date(),
    season: [
      { value: "Spring", label: "Spring" },
      { value: "Summer", label: "Summer" },
      { value: "Autumn", label: "Autumn" },
      { value: "Winter", label: "Winter" },
      { value: "Rainy", label: "Rainy" },
    ],
    patientSeason: "",
  });
  const getDiseaseList = () => {
    axios
      .get(myApi + "/api/v1/diseaseList")
      .then((response) => {
        Array.isArray(response.data.data) &&
          setDiseaseList(
            Array.isArray(response.data.data) &&
              response.data.data.map((e) => ({
                value: e.id,
                label: e.disease_name,
                status: e.status,
              }))
          );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getDivisionList = () => {
    axios
      .get(myApi + "/api/v1/divisionList")
      .then((response) => {
        setDivisionList(
          Array.isArray(response.data.data)
            ? response.data.data.map((e) => ({
                value: e.id,
                label: e.name,
                bangla_label: e.bn_name,
                url: e.url,
              }))
            : []
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const diseaseAction = (value) => {
    if (value !== null && value.length !== 0) {
      //store disease id
      setValues({
        ...form,
        selectedDisease: value.value,
      });
      console.log(form.selectedDisease);
    }

    if (value === null || value.length === 0) {
      if (form.selectRefDiv) {
        form.selectRefDiv.select.clearValue();
      }
      setValues({
        ...form,
        selectedDisease: "",
      });
    }
  };
  const divisionAction = (value) => {
    if (value !== null && value.length !== 0) {
      axios
        .post(myApi + "/api/v1/districtListByDivisionID", {
          division_id: value ? [value.value] : [],
        })
        .then((response) => {
          // eslint-disable-next-line no-unused-expressions
          response.data.data.length !== 0 && Array.isArray(response.data.data)
            ? setValues({
                ...form,
                selectedDivision: value.value,
                districtList: response.data.data.map((e) => ({
                  value: e.id,
                  label: e.name,
                  bangla_label: e.bn_name,
                  division_id: e.division_id,
                })),
                isDivisionSubmitted: true,
              })
            : [];
        })
        .catch((error) => {
          console.log(error);
        });
    }

    if (value === null || value === 0) {
      if (districtRef) {
        districtRef.current.select.clearValue();
      }
      if (upazilaRef) {
        upazilaRef.current.select.clearValue();
      }
      setValues({
        ...form,
        districtList: [],
        selectedDivision: "",
        selectedDistrict: "",
        selectedUpazila: "",
        isDivisionSubmitted: false,
        upazilaList: [],
        isUpazilaSubmitted: false,
      });
    }
  };
  const districtAction = (value) => {
    if (value !== null && value !== 0) {
      // get upazilla List
      if (value !== null && value !== 0) {
        axios
          .post(myApi + "/api/v1/upazilaListByDistrictID", {
            district_id: value ? [value.value] : [],
          })
          .then((response) => {
            // eslint-disable-next-line no-unused-expressions
            Array.isArray(response.data.data)
              ? setValues({
                  ...form,
                  selectedDistrict: value.value,
                  upazilaList: response.data.data.map((e) => ({
                    value: e.id,
                    label: e.name,
                    bangla_label: e.bn_name,
                    district_id: e.district_id,
                  })),
                  selectedUpazila: value.value,
                  isUpazilaSubmitted: true,
                })
              : [];

            console.log(form);
          })
          .catch((error) => {
            console.log(error);
          });
      }
    }

    if (value === null || value === 0) {
      if (upazilaRef) {
        upazilaRef.current.select.clearValue();
      }
      setValues({
        ...form,
        upazilaList: [],
        selectedDistrict: "",
        selectedUpazila: "",
        isUpazilaSubmitted: false,
      });
    }
  };
  const upazilaAction = (value) => {
    if (value !== null && value.length !== 0) {
      //store disease id
      setValues({
        ...form,
        selectedUpazila: value.value,
      });
      console.log(form);
    }

    if (value === null || value.length === 0) {
      if (form.selectRefDiv) {
        form.selectRefDiv.select.clearValue();
      }
      setValues({
        ...form,
        selectedUpazila: "",
      });
    }
  };
  const handleChange = (event) => {
    event.persist();
    setValues({ ...form, [event.target.name]: event.target.value });
  };
  const handleDateChange = (date) => {
    date = moment(date).format("YYYY-MM-DD");
    setValues({ ...form, date });
  };
  useEffect(() => {
    getDiseaseList();
    getDivisionList();
  }, []);
  const genderAction = (value) => {
    if (value !== null && value.length !== 0) {
      setValues({
        ...form,
        patientGender: value.value,
      });
    }

    if (value === null || value.length === 0) {
      if (form.selectRefDiv) {
        form.selectRefDiv.select.clearValue();
      }
      setValues({
        ...form,
        patientGender: "",
      });
    }
  };
  const seasonAction = (value) => {
    if (value !== null && value.length !== 0) {
      setValues({
        ...form,
        patientSeason: value.value,
      });
    }

    if (value === null || value.length === 0) {
      if (form.selectRefDiv) {
        form.selectRefDiv.select.clearValue();
      }
      setValues({
        ...form,
        patientSeason: "",
      });
    }
  };
  const handleSubmit = (event) => {
    axios
      .post(
        myApi + "/api/v1/health_data/add",
        {
          disease_id: form.selectedDisease,
          division_id: form.selectedDivision,
          district_id: form.selectedDistrict,
          upazila_id: form.selectedUpazila,
          name: form.patientName,
          age: form.patientAge,
          gender: form.patientGender,
          duration: form.patientDuration,
          date: moment(form.date).format("YYYY-MM-DD"),
          season: form.patientSeason,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
          },
        }
      )
      .then((res) => {
        //console.log(res)
        if (res.data.message) {
          if (res.data.success) {
            Swal.fire({
              icon: "success",
              title: "Data has been Added Successfully",
              showConfirmButton: false,
              timer: 1000,
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000,
            });
          }
          if (res.data.success === true) {
            if (districtRef) {
              districtRef.current.select.clearValue();
            }
            if (divisionRef) {
              divisionRef.current.select.clearValue();
            }
            if (upazilaRef) {
              upazilaRef.current.select.clearValue();
            }
            if (seasonRef) {
              seasonRef.current.select.clearValue();
            }
            if (genderRef) {
              genderRef.current.select.clearValue();
            }
            if (diseaseRef) {
              diseaseRef.current.select.clearValue();
            }
            setValues({
              ...form,
              selectedDisease: "",
              selectedDivision: "",
              selectedDistrict: "",
              selectedUpazila: "",
              patientAge: "",
              patientName: "",
              patientDuration: "",
              patientSeason: "",
              date: new Date(),
              patientGender: "",
            });
          }
        }
      });
  };
  let { date, patientName, patientDuration, patientAge } = form;
  return (
    <>
      <ValidatorForm onSubmit={handleSubmit} onError={(errors) => null}>
        <Grid container spacing={6} style={{ padding: "15px" }}>
          <Grid item lg={6} md={6} sm={12} xs={12} style={{ marginTop: "0px" }}>
            <div style={{ marginBottom: "15px", marginTop: "10px" }}>
              <strong>
                <font color="black">Select Disease</font>
              </strong>
            </div>
            <Select
              ref={diseaseRef}
              isClearable="true"
              name="selectedClientIndividual"
              options={diseaseList}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={diseaseAction}
            />
            <div style={{ marginBottom: "15px", marginTop: "35px" }}>
              <strong>
                <font color="black">Select District</font>
              </strong>
            </div>
            <Select
              ref={districtRef}
              isClearable="true"
              name="selectedDiscussionType"
              options={form.districtList}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={districtAction}
            />
            <div style={{ marginBottom: "-5px", marginTop: "60px" }}></div>
            <TextValidator
              autoComplete="off"
              className="mb-4 w-full"
              label="Patient Name"
              onChange={handleChange}
              type="text"
              name="patientName"
              value={patientName}
              validators={["required"]}
              errorMessages={["this field is required"]}
            />
            <div style={{ marginBottom: "10px", marginTop: "35px" }}>
              <strong>
                <font color="black">Select Gender</font>
              </strong>
            </div>
            <Select
              ref={genderRef}
              isClearable="true"
              name="genderAction"
              options={form.gender}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={genderAction}
            />
            <div style={{ marginBottom: "-5px", marginTop: "60px" }}></div>
            <TextValidator
              autoComplete="off"
              className="mb-4 w-full"
              label="Duration"
              onChange={handleChange}
              type="number"
              name="patientDuration"
              value={patientDuration}
              validators={["required"]}
              errorMessages={["this field is required"]}
            />
          </Grid>
          <Grid item lg={6} md={6} sm={12} xs={12}>
            <div style={{ marginBottom: "15px", marginTop: "10px" }}>
              <strong>
                <font color="black">Select Division</font>
              </strong>
            </div>
            <Select
              ref={divisionRef}
              isClearable="true"
              name="divisionList"
              options={divisionList}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={divisionAction}
            />

            <div style={{ marginBottom: "15px", marginTop: "35px" }}>
              <strong>
                <font color="black">Select Upazila</font>
              </strong>
            </div>
            <Select
              ref={upazilaRef}
              isClearable="true"
              name="selectedVehicleTypeCorporate"
              options={form.upazilaList}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={upazilaAction}
            />
            <div style={{ marginBottom: "-5px", marginTop: "60px" }}></div>
            <TextValidator
              autoComplete="off"
              className="mb-4 w-full"
              label="Age"
              onChange={handleChange}
              type="number"
              name="patientAge"
              value={patientAge}
              validators={["required"]}
              errorMessages={["this field is required"]}
            />
            <div style={{ marginBottom: "10px", marginTop: "35px" }}>
              <strong>
                <font color="black">Select Season</font>
              </strong>
            </div>
            <Select
              ref={seasonRef}
              isClearable="true"
              name="seasonAction"
              options={form.season}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={seasonAction}
            />
            <div style={{ marginBottom: "-5px", marginTop: "35px" }}>
              <strong>
                <font color="black">Select Date</font>
              </strong>
            </div>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                style={{ marginTop: "20px" }}
                className="mb-4 w-full"
                margin="none"
                id="mui-pickers-date"
                inputVariant="standard"
                type="text"
                autoOk={true}
                value={date}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
              />
            </MuiPickersUtilsProvider>
          </Grid>
        </Grid>
        <Button
          style={{ marginLeft: "15px", width: "auto", textAlign: "center" }}
          color="primary"
          variant="contained"
          type="submit"
        >
          <span className="pl-2 capitalize">Submit</span>
        </Button>
      </ValidatorForm>
    </>
  );
}

const mapStateToProps = (state) => ({
  updateUserProfile: PropTypes.func.isRequired,
  user: state.user,
});

export default connect(mapStateToProps, { updateUserProfile })(ProfileForm);
