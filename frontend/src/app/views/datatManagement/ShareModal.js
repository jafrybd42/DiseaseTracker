import { Icon } from "@material-ui/core";
import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import AddEditForm from "./shareData";

function ModalForm(props) {
  console.log(props);
  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };

  const closeBtn = (
    <button className="close" onClick={toggle}>
      &times;
    </button>
  );
  const label = props.buttonLabel;

  let button = "";
  let title = "";

  if (label === "Edit") {
    button = (
      <Button
        color="warning"
        onClick={toggle}
        style={{ float: "left", marginRight: "10px" }}
      >
        {label}
      </Button>
    );
    title = "Edit Type";
  } else {
    button = (
      <Button
        className="shareBtn"
        color="success"
        onClick={toggle}
        // style={{ float: "center", marginRight: "10px" }}
      >
        <Icon fontSize="small" style={{ marginBottom: "-5px" }}>
          share
        </Icon>
        &nbsp; {label}
      </Button>
    );
    title = "Share";
  }

  return (
    <>
      {button}
      <Modal
        type={props.type}
        response={props.response}
        filter={props.filter}
        count={props.count}
        message={props.message}
        isSuccess={props.success}
        items={props.items}
        isOpen={modal}
        toggle={toggle}
        className={props.className}
        style={{ maxWidth: "70%" }}
      >
        {/* <ModalHeader toggle={toggle} close={closeBtn}>
          {title}
        </ModalHeader> */}
        <ModalBody
          type={props.type}
          response={props.response}
          style={{ padding: "0rem" }}
          filter={props.filter}
          count={props.count}
          message={props.message}
          isSuccess={props.success}
          items={props.items}
        >
          <AddEditForm
            type={props.type}
            response={props.response}
            toggle={toggle}
            // item={props.item}
            filter={props.filter}
            count={props.count}
            message={props.message}
            isSuccess={props.success}
            items={props.items}
          />
        </ModalBody>
      </Modal>
    </>
  );
}

export default ModalForm;
