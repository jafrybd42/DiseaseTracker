import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import SearchBar from "material-ui-search-bar";
import { Button, Icon } from "@material-ui/core";
import TeamTable from "./teamTable";
import AdminTable from "./adminTable";
import ManagementTable from "./managementTable";
import axios from "axios";
import myApi from "app/auth/api";
import Loader from "react-loader-spinner";

function TabPanel(props) {
  console.log(props);
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    borderRadius: "10px 10px 0px 0px",
  },
}));

export default function AddEditForm(props) {
  console.log(props);

  const [teamList, setTeamList] = useState([]);
  const [managementList, setManagementList] = useState([]);
  const [adminList, setAdminList] = useState([]);
  const [isLoading, setIsLoading] = useState("");
  const [form, setForm] = useState({
    value: "",
  });
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const getItems = () => {
    setIsLoading(true);
    //get Team List
    axios({
      method: "GET",
      url: myApi + "/api/v1/user/list/2",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        setTeamList(
          Array.isArray(response.data.data)
            ? response.data.data.map((e) => ({
                id: e.id,
                name: e.name,
                email: e.email,
                organization_id: e.organization_id,
                phone_number: e.phone_number,
                roles_id: e.roles_id,
                status: e.status,
                checked: false,
              }))
            : []
        );
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });

    //get Management List
    setIsLoading(true);
    axios({
      method: "GET",
      url: myApi + "/api/v1/user/list/3",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        setManagementList(
          Array.isArray(response.data.data)
            ? response.data.data.map((e) => ({
                id: e.id,
                name: e.name,
                email: e.email,
                organization_id: e.organization_id,
                phone_number: e.phone_number,
                roles_id: e.roles_id,
                status: e.status,
                checked: false,
              }))
            : []
        );
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });

    //get Admin List
    axios({
      method: "GET",
      url: myApi + "/api/v1/user/list/5",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        setAdminList(
          Array.isArray(response.data.data)
            ? response.data.data.map((e) => ({
                id: e.id,
                name: e.name,
                email: e.email,
                organization_id: e.organization_id,
                phone_number: e.phone_number,
                roles_id: e.roles_id,
                status: e.status,
                checked: false,
              }))
            : []
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };
  useEffect(() => {
    setIsLoading(true);
    getItems();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className={classes.root}>
      <AppBar position="static" className="popupHeader">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="Tabs"
          style={{ width: "60%" }}
          TabIndicatorProps={{
            style: {
              background: "black",
              height: "3px",
              width: "130px",
              marginLeft: "15px",
            },
          }}
        >
          <Tab label="Team Members" {...a11yProps(0)} />
          <Tab label="Management" {...a11yProps(1)} />
          <Tab label="Admin" {...a11yProps(2)} />
        </Tabs>
        <div style={{ width: "30%", display: "flex" }}>
          <SearchBar
            className="popSearch"
            value={form.value}
            // onChange={(newValue) => setValues({ ...form, value: newValue })}
            // onRequestSearch={() => doSomethingWith(this.state.value)}
          />
        </div>
        {/* <Button className="upBtnSecond" color="success">
          <Icon fontSize="small" style={{ marginBottom: "-2px" }}>
            share
          </Icon>
          &nbsp; Share
        </Button> */}
      </AppBar>
      <TabPanel value={value} index={0}>
        {isLoading ? (
          <Loader type="ThreeDots" color="#00BFFF" height={80} width={80} />
        ) : (
          <TeamTable
            type={props.type}
            response={props.response}
            items={teamList}
            toggle={props.toggle}
            data={props.items}
            filter={props.filter}
            count={props.count}
            message={props.message}
            isSuccess={props.success}
          />
        )}
      </TabPanel>
      <TabPanel value={value} index={1}>
        {isLoading ? (
          <Loader type="ThreeDots" color="#00BFFF" height={80} width={80} />
        ) : (
          <ManagementTable
            items={managementList}
            toggle={props.toggle}
            data={props.items}
            filter={props.filter}
            count={props.count}
            message={props.message}
            isSuccess={props.success}
          />
        )}
      </TabPanel>
      <TabPanel value={value} index={2}>
        {isLoading ? (
          <Loader type="ThreeDots" color="#00BFFF" height={80} width={80} />
        ) : (
          <AdminTable
            items={adminList}
            toggle={props.toggle}
            data={props.items}
            filter={props.filter}
            count={props.count}
            message={props.message}
            isSuccess={props.success}
          />
        )}
      </TabPanel>
    </div>
  );
}
