import React, { Component } from "react";
import { Card, Grid, Button, CircularProgress } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withRouter, Redirect, Link } from "react-router-dom";
import { loginPhonePass } from "../../redux/actions/LoginActions";

const styles = (theme) => ({
  wrapper: {
    position: "relative",
  },

  buttonProgress: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
});

class SignIn extends Component {
  state = {
    email: "",
    password: "",
    agreement: "",
    path: "",
  };

  handleChange = (event) => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFormSubmit = (event) => {
    event.preventDefault();

    this.props.loginPhonePass({ ...this.state });
  };
  render() {
    let { email, password } = this.state;
    let { classes } = this.props;

    let successRedirect = null;

    if (this.props.login.path) {
      successRedirect = <Redirect to={this.props.login.path} />;
    }

    return (
      <div
        className="signup flex justify-center w-full h-full-screen"
        style={{ backgroundColor: "white" }}
      >
        {successRedirect}

        <div className="loginBackSide">
          <div className="loginBackGrey">
          {/* <div className='membershipPlan'>Membership Plans</div> */}

            <div className="loginUpperLeft">
              <div className="leftUpperBox">
                <div className="displayFlex">
                  <span className="basicTxt">Basic</span>
                  <span className="basicTxt2">
                    <span className="usd">USD</span>&nbsp;
                    <span className="amount">100</span>
                  </span>
                </div>
                <div className="boxLeft">
                  <p style={{ marginBottom: "0.2rem" }}>
                    Number of Users : 4 1 Admin,{" "}
                  </p>
                  <p style={{ marginBottom: "0.2rem" }}>
                    2 Team Members, 1 Management
                  </p>
                  <p style={{ marginBottom: "0.2rem" }}>
                    Number of Reports/month : 10 Unlimited Participant
                  </p>
                </div>
              </div>

              <div className="leftUpperBox">
                <div className="displayFlex">
                  <span className="basicTxt">Basic</span>
                  <span className="basicTxt2">
                    <span className="usd">USD</span>&nbsp;
                    <span className="amount">100</span>
                  </span>
                </div>
                <div className="boxLeft">
                  <p style={{ marginBottom: "0.2rem" }}>
                    Number of Users : 4 1 Admin,{" "}
                  </p>
                  <p style={{ marginBottom: "0.2rem" }}>
                    2 Team Members, 1 Management
                  </p>
                  <p style={{ marginBottom: "0.2rem" }}>
                    Number of Reports/month : 10 Unlimited Participant
                  </p>
                </div>
              </div>

              <div className="leftUpperBox">
                <div className="displayFlex">
                  <span className="basicTxt">Basic</span>
                  <span className="basicTxt2">
                    <span className="usd">USD</span>&nbsp;
                    <span className="amount">100</span>
                  </span>
                </div>
                <div className="boxLeft">
                  <p style={{ marginBottom: "0.2rem" }}>
                    Number of Users : 4 1 Admin,{" "}
                  </p>
                  <p style={{ marginBottom: "0.2rem" }}>
                    2 Team Members, 1 Management
                  </p>
                  <p style={{ marginBottom: "0.2rem" }}>
                    Number of Reports/month : 10 Unlimited Participant
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="loginBackWhite">
            <div className="loginUpperRight">
              <div className="loginTxt">Login</div>
              <div className="p-9 h-full  position-relative loginField">
                <ValidatorForm
                  ref="form"
                  noValidate
                  autoComplete="off"
                  autoCorrect="off"
                  spellCheck="off"
                  onSubmit={this.handleFormSubmit}
                >
                  <TextValidator
                    autoComplete="false"
                    className="mb-6 w-full"
                    variant="outlined"
                    label="Phone/Email"
                    onChange={this.handleChange}
                    type="email"
                    name="email"
                    value={email}
                    validators={["required"]}
                    errorMessages={[
                      "this field is required",
                      "email is not valid",
                    ]}
                  />
                  <TextValidator
                    autoComplete="off"
                    className="mb-3 w-full"
                    label="Password"
                    variant="outlined"
                    onChange={this.handleChange}
                    name="password"
                    type="password"
                    value={password}
                    validators={["required"]}
                    errorMessages={["this field is required"]}
                  />

                  <div
                    className="flex flex-wrap items-center mb-4 rightAlign"
                    style={{ marginTop: "10px" }}
                  >
                    <Link
                      className="text-primary"
                      onClick={() =>
                        this.props.history.push("/session/forgot-password")
                      }
                    >
                      Forgot Password ?
                    </Link>
                    <div
                      className={classes.wrapper}
                      style={{
                        right: "35px",
                        position: "absolute",
                        color: "white",
                      }}
                    >
                      <Button
                        className="loginClr white"
                        variant="contained"
                        disabled={this.props.login.loading}
                        type="submit"
                      >
                        Login
                      </Button>
                      {this.props.login.loading && (
                        <CircularProgress
                          size={24}
                          className={classes.buttonProgress}
                        />
                      )}
                    </div>
                  </div>
                </ValidatorForm>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  loginPhonePass: PropTypes.func.isRequired,
  login: state.login,
});

export default withStyles(styles, { withTheme: true })(
  withRouter(connect(mapStateToProps, { loginPhonePass })(SignIn))
);
