import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import DataTable from "./DataTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";
import { Button, Card } from "@material-ui/core";
import history from "../../../history";
import ModalForm from "../datatManagement/Modal";
import localStorageService from "app/services/localStorageService";

function Sa(props) {
  const [items, setItems] = useState([]);

  const getItems = () => {
    if (localStorageService?.getItem("auth_user")?.role_id === 1) {
      axios({
        method: "GET",
        url: myApi + "/api/v1/upload_file_history/allFileList",
        headers: {
          "x-access-token": localStorage.getItem("jwt_token"),
        },
      })
        .then((response) => {
          setItems(Array.isArray(response.data.data) ? response.data.data : []);
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      axios({
        method: "GET",
        url: myApi + "/api/v1/upload_file_history/allFileListByUser",
        headers: {
          "x-access-token": localStorage.getItem("jwt_token"),
        },
      })
        .then((response) => {
          setItems(Array.isArray(response.data.data) ? response.data.data : []);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };
  const organizationCreate = (event) => {
    event.persist();
    history.push("/organization/create");
  };

  useEffect(() => {
    getItems();
  }, []);

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row style={{ marginBottom: "20px" }}>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Dashboard", path: "/" },
                  { name: "Upload History" },
                ]}
              />
            </Col>
          </Row>
          <br></br>
          <Row>
            <div style={{ display: "flex", width: "100%" }}>
              <div style={{ width: "80%" }}></div>
              <div style={{ width: "10%" }}>
                <ModalForm buttonLabel="Upload Data" />
              </div>
            </div>
          </Row>
          <br></br>
          <Row>
            <Card className="px-6 pt-2 pb-4">
              <Col>
                <DataTable items={items} />
              </Col>
            </Card>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Sa;
