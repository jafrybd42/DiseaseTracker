/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const UploadHistoryRoutes = [
  {
    path: "/uploads/history",
    component: React.lazy(() => import("./sa")),
  },
];

export default UploadHistoryRoutes;
