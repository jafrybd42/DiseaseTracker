import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService";
import axios from "axios";
import Swal from "sweetalert2";
import { Button, Icon, Grid } from "@material-ui/core";
import "date-fns";
import myApi from "app/auth/api";
import history from "../../../history";

class CreateForm extends Component {
  constructor() {
    super();
    this.state = {
      organizationName: "",
      isOrganizationList: false,
      isOrganizationCreate: true,
    };
  }

  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", (value) => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleChange = (event) => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = (event) => {
    try {
      axios
        .post(
          myApi + "/api/v1/organization/add",
          { name: this.state.organizationName },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token,
            },
          }
        )
        .then((res) => {
          if (res.data) {
            if (res.data.success) {
              Swal.fire({
                icon: "success",
                title: res.data.message,
                showConfirmButton: false,
                timer: 1000,
              });
            } else {
              Swal.fire({
                icon: "error",
                title: res.data.message,
                showConfirmButton: false,
                timer: 1000,
              });
            }
            if (res.data.success === true) {
              this.setState({
                organizationName: "",
              });
            }
          }
        });
    } catch (err) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Something went wrong!",
      });
    }
  };

  organizationList = (event) => {
    event.persist();
    history.push("/organization/list");
  };

  organizationCreate = (event) => {
    event.persist();
    this.setState({
      isOrganizationList: false,
      isOrganizationCreate: true,
    });
  };
  render() {
    let { organizationName } = this.state;
    console.log(this.state.isOrganizationList);

    return (
      <div>
        {this.state.isOrganizationCreate === true && (
          <Button
            className="rightFlex"
            onClick={this.organizationList}
            style={{ marginTop: "-30px" }}
          >
            Organization List
          </Button>
        )}
        {this.state.isOrganizationList === true && (
          <Button className="rightFlex" onClick={this.organizationCreate}>
            Organization Create
          </Button>
        )}
        {this.state.isOrganizationCreate && (
          <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmit}
            onError={(errors) => null}
            style={{
              marginLeft: "20px",
              marginRight: "20px",
            }}
          >
            <Grid container spacing={6}>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextValidator
                  autoComplete="off"
                  className="mb-4 w-full"
                  label="Organization Name"
                  onChange={this.handleChange}
                  type="text"
                  name="organizationName"
                  value={organizationName}
                  validators={["required", "maxStringLength: 50"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>
            </Grid>
            <Button color="primary" variant="contained" type="submit">
              <Icon>send</Icon>
              <span className="pl-2 capitalize">Create</span>
            </Button>
          </ValidatorForm>
        )}
      </div>
    );
  }
}

export default CreateForm;
