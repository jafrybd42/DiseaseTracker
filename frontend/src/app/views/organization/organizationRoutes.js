/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const OrganizationRoutes = [
  {
    path: "/organization/create",
    component: React.lazy(() => import("./createOrganization")),
    auth: authRoles.mtAdmin
  },
  // {
  //   path: "/organization/manage",
  //   component: React.lazy(() => import("./manageorganization")),
  //   auth: authRoles.admin,
  //   auth: authRoles.editor
  // },
  {
    path: "/organization/list",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.mtAdmin
  }
];

export default OrganizationRoutes;
