/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import axios from "axios";
import myApi from "../../auth/api";
import history from "../../../history";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
function AddEditForm(props) {
  const [item, setItem] = useState([]);
  const [form, setValues] = useState({
    id: 0,
    name: "",
    status: 0,
  });

  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const submitFormAdd = (e) => {
    e.preventDefault();
  };

  const submitFormEdit = (e) => {
    e.preventDefault();

    axios
      .post(
        myApi + "/api/v1/organization/update",
        {
          id: form.id,
          name: form.name,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )

      .then((response) => {
        if (response.data.success) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500,
          });
        }
        // Swal.fire('Updated Successfully')
        if (response.data.success === true) {
          props.updateState(form);
        }
        props.toggle();
      })

      .catch((err) => console.log(err));
  };

  useEffect(() => {
    if (props.item) {
      const {
        id,
        name,
        last,
        email,
        phone_number,
        location,
        hobby,
        status,
      } = props.item;
      setValues({
        id,
        name,
        last,
        email,
        phone_number,
        location,
        hobby,
        status,
      });
    }
  }, false);

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="name">Organization Name</Label>
        <Input
          type="text"
          name="name"
          id="name"
          onChange={onChange}
          value={form.name === null ? "" : form.name}
          minLength="3"
          required
        />
      </FormGroup>

      <Button>Submit</Button>
    </Form>
  );
}

export default AddEditForm;
