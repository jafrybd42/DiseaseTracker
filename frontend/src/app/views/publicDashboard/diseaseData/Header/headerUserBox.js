import React, { useState, Fragment, useEffect } from "react";
import axios from "axios";
import Select from "react-select";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import myApi from "app/auth/api";

// import { connect } from 'react-redux';
export default function HeaderUserbox(props) {
  console.log(props);

  const history = useHistory();

  const [diseaseList, setDiseaseList] = React.useState([]);
  const [selectedDisease, setSelectedDisease] = React.useState([]);
  useEffect(() => {
    //total disease
    axios
      .get(myApi + "/api/v1/diseaseList")
      .then((response) => {
        response &&
          response.data &&
          response.data.data &&
          setDiseaseList(
            response.data.data.map((e) => ({
              value: e.id,
              label: e.disease_name,
              status: e.status,
            }))
          );
      })
      .catch((error) => {
        console.log(error);
      });
  }, false);
  const onChangeDiseaseSelect = (value) => {
    if (value !== null) {
      setSelectedDisease(value);
      history.push({
        pathname: "/public/disease/" + value.value,
        data: value,
        state: value,
      });
      // window.location.reload(false);
    }
    console.log(selectedDisease);
  };

  return (
    // Header Disease Selection
    <Fragment className="headerRight">
      <div className="dropdown_style">
        <Select
          // value={
          //   selectedDisease
          //     ? diseaseList.map((e) => e.value === props.selectedData)
          //     : props.selectedData
          // }
          placeholder="Search disease here"
          isClearable
          options={diseaseList}
          onChange={onChangeDiseaseSelect}
        />
      </div>
    </Fragment>
  );
}
