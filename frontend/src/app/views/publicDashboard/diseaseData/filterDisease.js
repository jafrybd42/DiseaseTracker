/* eslint-disable no-unused-vars */
/* eslint-disable no-unused-expressions */
import React, { Fragment } from "react";
import { Grid, Card, Avatar } from "@material-ui/core";
import Bangladesh from "../inc/svg-maps/bd-divission/index.js";
import SubDivisionMapBD from "../inc/svg-maps/bd-sub-divission/index.js";
import { CheckboxSVGMap } from "../inc/react-svg-map/src/index";
import "react-svg-map/lib/index.css";
import ApexChartsColumn from "./genderChart";
import ApexChartsArea from "./timelineGraph";
import { WrapperSimple } from "./layout-component";
import PerfectScrollbar from "react-perfect-scrollbar";
import SeasonData from "./season";
import maleIcon from "../../../../../src/assets/img/maleIcon.svg";
import femaleIcon from "../../../../../src/assets/img/femaleIcon.svg";
import Select from "react-select";
import myApi from "../../../auth/api.js";
import axios from "axios";
import CheckBox from "../../checkBox/index.js";
import { Button } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import CloudDownloadTwoToneIcon from "@material-ui/icons/CloudDownloadTwoTone";
import Swal from "sweetalert2";
import HeaderLogo from "./Header/headerLogo";
import SearchBar from "material-ui-search-bar";
import { ReactSearchAutocomplete } from "react-search-autocomplete";

class MAP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      diseaseList: [],
      value: "",
      isDivisionSubmitted: false,
      isSubDivisionSubmitted: false,
      selectedSubDivisionMap: [],
      selectedDivisionOld: [],
      selectedDivisionNew: [],
      selectedUpazillaNew: [],
      selectedSubDivisionNew: [],
      selectedSubLocation: [],
      selectedMapSubDiv: [],
      selectedSubDivNew: [],
      selectedDivision: [],
      selectedSubDivision: [],
      selectedUpazilla: [],
      propitem: [],
      selectedLocation: [],
      locationDivission: [],
      locationSubDivission: [],
      locationUpazilla: [],

      seasons: [
        { id: 1, value: "Spring", isChecked: false },
        { id: 2, value: "Summer", isChecked: false },
        { id: 3, value: "Autumn", isChecked: false },
        { id: 4, value: "Winter", isChecked: false },
        { id: 5, value: "Rainy", isChecked: false },
      ],
      selectedSeason: [],
      subDivisonValue: [],
      searchResult: [],
      totalPatient: 0,
      updateGenderChart: [],
      timelineData: [],
      selectedDisease:
        this.props?.location?.state?.disease_id ||
        this.props.location?.data?.id ||
        this.props.location?.state?.id,
      diseaseName:
        this.props?.location?.state?.disease_name ||
        this.props.location?.data?.name ||
        this.props.location?.state?.name,
    };
  }

  componentDidMount() {
    console.log(this.props.location);
    this.setState({
      selectedDisease:
        this.props?.location?.state?.disease_id ||
        this.props.location?.data?.id,
    });
    //get diseaseList
    axios
      .get(myApi + "/api/v1/diseaseList")
      .then((response) => {
        response &&
          response.data &&
          response.data.data &&
          this.setState({
            diseaseList: response.data.data.map((e) => ({
              id: e.id,
              name: e.disease_name,
              status: e.status,
            })),
          });
      })
      .catch((error) => {
        console.log(error);
      });
    if (this.state.selectedDisease) {
      //getting filter data
      axios
        .post(myApi + "/api/v1/searchHealthData", {
          disease_id: this.state?.selectedDisease,
        })
        .then((response) => {
          this.setState({
            updateGenderChart: response ? response?.data?.ageGenderChart : [],
            searchResult: response ? response.data : [],
            gender_charts: response?.data?.ageGenderChart,
            totalPatients: response?.data?.Gender,
            timelineData: response?.data?.monthlyAffected,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    }

    //get Divission List
    axios
      .get(myApi + "/api/v1/divisionList")
      .then((response) => {
        Array.isArray(response.data.data)
          ? this.setState({
              locationDivission: response.data.data.map((e) => ({
                value: e.id,
                label: e.name,
                bangla_label: e.bn_name,
              })),
            })
          : [];
      })
      .catch((error) => {
        console.log(error);
      });

    //get districtList
    this.state.selectedDivision.length !== 0 &&
      this.setState({
        locationSubDivission: [],
      });

    // get upazilla List
    this.state.selectedSubDivision.length !== 0 &&
      this.setState({
        locationUpazilla: [],
      });
  }

  mapDivisionChange = (props) => {
    this.setState(
      {
        isDivisionSubmitted: true,
        isSubDivisionSubmitted: false,
        selectedDivisionOld: [],
        selectedLocation: props?.map((e) => e.id),
        selectedDivision: props?.map((e) => e.id),
        selectedDivisionNew: props?.map((e) => e.id + "_" + e.ariaLabel),
      },
      () => {
        let division, subDiv, upazilla, area;
        let filteredArray,
          filteredDivision = [];
        filteredArray = this.state.selectedDivision?.map(
          (e) => ([division, subDiv, upazilla] = e.split("_"))
        );
        filteredDivision = this.state.selectedDivisionNew?.map(
          (e) => ([division, subDiv, upazilla, area] = e.split("_"))
        );
        this.setState(
          {
            selectedLocation: this.state.selectedLocation?.map(
              (e) => ([division, subDiv, upazilla] = e.split("_"))
            ),
            selectedDivision: filteredArray.map((e) => Number(e[0])),
            selectedDivisionOld: filteredDivision.map((e) => ({
              value: Number(e[0]),
              label: e[3],
            })),
          },
          () => {
            this.setState({
              selectedLocation: this.state.selectedLocation?.map((e) =>
                Number(e[0])
              ),
            });

            //filter based on selected division from MAP
            axios
              .post(myApi + "/api/v1/searchHealthData", {
                disease_id: this.state.selectedDisease,
                division_id: this.state.selectedDivision,
              })
              .then((response) => {
                this.setState({
                  selectedSeason: this.state?.selectedSeason.filter(
                    (e) => e !== false
                  ),
                  updateGenderChart: response
                    ? response?.data?.ageGenderChart
                    : [],
                  searchResult: response ? response.data : [],
                  gender_charts: response?.data?.ageGenderChart,
                  totalPatients: response?.data?.Gender,
                  timelineData: response?.data?.monthlyAffected,
                });
              })
              .catch((error) => {
                console.log(error);
              });
          }
        );
      }
    );
  };

  mapSubDivisionChange = (props) => {
    this.setState(
      {
        // selectedLocation: props?.map(e => e.id),
        // selectedDivision: props?.map(e => e.id),

        selectedSubDivisionMap: props?.map((e) => e.id + "_" + e.ariaLabel),
      },
      () => {
        let division, subDiv, upazilla, area;
        let filteredArray,
          filteredSubDivision = [];
        // filteredArray = this.state.selectedDivision?.map(
        //   e => ([division, subDiv, upazilla] = e.split('_'))
        // );
        filteredSubDivision = this.state.selectedSubDivisionMap?.map(
          (e) => ([division, subDiv, upazilla, area] = e.split("_"))
        );
        this.setState(
          {
            // selectedLocation: this.state.selectedLocation?.map(
            //   e => ([division, subDiv, upazilla] = e.split('_'))
            // ),
            // selectedDivision: filteredArray.map(e => Number(e[0])),
            selectedSubDivisionNew: [],
            // filteredSubDivision.map((e) => ({
            //   value: Number(e[0]),
            //   label: e[3],
            //   division_id: Number(e[1]),
            // })),
            selectedSubDivNew: filteredSubDivision.map((e) => ({
              value: Number(e[0]),
              label: e[3],
              division_id: Number(e[1]),
            })),
          },
          () => {
            this.setState({
              // selectedLocation: this.state.selectedLocation?.map(e =>
              //   Number(e[0])
              // )
            });

            //filter based on selected division from MAP
            axios
              .post(myApi + "/api/v1/searchHealthData", {
                disease_id: this.state.selectedDisease,
                division_id: this.state.selectedSubDivNew.map(
                  (e) => e.division_id
                ),
                district_id: this.state.selectedSubDivNew.map((e) => e.value),
              })
              .then((response) => {
                this.setState({
                  selectedSeason: this.state?.selectedSeason.filter(
                    (e) => e !== false
                  ),
                  updateGenderChart: response
                    ? response?.data?.ageGenderChart
                    : [],
                  searchResult: response ? response.data : [],
                  gender_charts: response?.data?.ageGenderChart,
                  totalPatients: response?.data?.Gender,
                  timelineData: response?.data?.monthlyAffected,
                });
              })
              .catch((error) => {
                console.log(error);
              });
          }
        );
      }
    );
  };

  onChangeDivision = (value) => {
    if (value !== null && value?.length !== 0) {
      this.setState(
        {
          isDivisionSubmitted: true,
          selectedLocation: [],
          selectedDivision: Array.isArray(value)
            ? value.map((x) => x.value)
            : [],
          selectedDivisionOld: Array.isArray(value)
            ? value.map((x) => ({
                value: x.value,
                label: x.label,
                division_id: x.division_id,
              }))
            : [],
          isSubDivisionSubmitted: false,
        },
        () => {
          this.setState({
            selectedLocation: Array.isArray(value)
              ? value.map((x) => x.value + "_0_0")
              : [],
            selectedDivision: Array.isArray(value)
              ? value.map((x) => x.value)
              : [],
          });
          console.log(this.state);
          //get districtList
          axios
            .post(myApi + "/api/v1/districtListByDivisionID", {
              division_id: this.state.selectedDivision,
            })
            .then((response) => {
              response.data.data.length !== 0 &&
              Array.isArray(response.data.data)
                ? this.setState({
                    locationSubDivission: response.data.data.map((e) => ({
                      value: e.id,
                      label: e.name,
                      bangla_label: e.bn_name,
                      division_id: e.division_id,
                    })),
                  })
                : [];

              console.log(this.state);
            })
            .catch((error) => {
              console.log(error);
            });
        }
      );
      console.log(this.state);
    }

    if (value?.length === 0 || value === null) {
      this.setState({
        selectedLocation: [],
        isDivisionSubmitted: false,
        isSubDivisionSubmitted: false,
        selectedDivisionOld: [],
        selectedSubDivisionNew: [],
        selectedUpazillaNew: [],
        selectedSubLocation: [],
      });

      if (this.state.selectedUpazillaNew.length !== 0) {
        this.setState({
          selectedUpazillaNew: [],
        });
        console.log(this.state);
      }
    }
    if (value?.length === 0) {
      if (this.state.selectedUpazillaNew.length !== 0) {
        this.setState({
          selectedUpazillaNew: [],
        });
        console.log(this.state);
      }
    }
  };

  onChangeSubDivision = (value) => {
    if (value !== null) {
      let filterArray = this.state?.locationSubDivission?.map((k) =>
        value
          .map(
            (e) =>
              e !== false &&
              e.value === k.value &&
              k.division_id + "_" + k.value + "_0"
          )
          .filter(Boolean)
          .toString()
      );
      this.setState(
        {
          selectedSubLocation: [],
          selectedSubDivision: Array.isArray(value)
            ? value.map((x) => x.value)
            : [],
          selectedSubDivisionNew: Array.isArray(value)
            ? value.map((x) => ({
                value: x.value,
                label: x.label,
                division_id: x.division_id,
              }))
            : [],
          isSubDivisionSubmitted: true,
          isDivisionSubmitted: true,
        },
        () => {
          this.setState({
            selectedSubLocation: filterArray.filter((e) => e !== false && e),
            selectedSubDivisionNew: Array.isArray(value)
              ? value.map((x) => ({
                  value: x.value,
                  label: x.label,
                  division_id: x.division_id,
                }))
              : [],

            selectedMapSubDiv: Array.isArray(value)
              ? value.map((x) => x.value)
              : [],
          });

          // get upazilla List
          axios
            .post(myApi + "/api/v1/upazilaListByDistrictID", {
              district_id: this.state.selectedSubDivision,
            })
            .then((response) => {
              Array.isArray(response.data.data)
                ? this.setState({
                    locationUpazilla: response.data.data.map((e) => ({
                      value: e.id,
                      label: e.name,
                      bangla_label: e.bn_name,
                      district_id: e.district_id,
                    })),
                  })
                : [];
            })
            .catch((error) => {
              console.log(error);
            });
        }
      );
    }
    if (value?.length === 0) {
      this.setState({
        selectedSubDivisionNew: [],
        selectedUpazillaNew: [],
        isSubDivisionSubmitted: false,
        selectedSubLocation: [],
      });
    }

    if (value === null) {
      this.setState({
        selectedSubDivisionNew: [],
        selectedUpazillaNew: [],
        selectedSubLocation: [],
      });
    }
  };
  onChangeUpazilla = (value) => {
    if (value !== null) {
      this.setState({
        selectedUpazilla: Array.isArray(value) ? value.map((x) => x.value) : [],
        selectedUpazillaNew: Array.isArray(value)
          ? value.map((x) => ({
              value: x.value,
              label: x.label,
              district_id: x.district_id,
            }))
          : [],
      });
    }
    if (value === null) {
      // if (this.upazilaRef) {
      //   this.upazilaRef.select.clearValue();
      // }
      this.setState({
        selectedUpazillaNew: [],
        // selectedUpazilla: [],
      });
    }
  };
  handleAllChecked = (event) => {
    let seasons = this.state.seasons;
    seasons.forEach((season) => (season.isChecked = event.target.checked));
    this.setState({ seasons: seasons });
    this.setState({
      selectedSeason: this.state.seasons.map(
        (e) => e.isChecked === true && e.value
      ),
    });
  };

  handleCheckChieldElement = (event) => {
    let seasons = this.state.seasons;
    seasons.forEach((season) => {
      if (season.value === event.target.value)
        season.isChecked = event.target.checked;
    });

    this.setState({
      selectedSeason: seasons.map((e) => e.isChecked === true && e.value),
    });
  };

  handleFilterAction = (event) => {
    event.preventDefault();
    // console.log(this.state.selectedDisease);
    let filterSeason = this.state?.selectedSeason.filter((e) => e !== false);

    let filterData = {
      disease_id: this.state.selectedDisease || this.props.location?.data?.id,
      division_id: this.state.selectedDivision,
      district_id: this.state.selectedSubDivision,
      upazila_id: this.state.selectedUpazilla,
      season_name: filterSeason,
    };

    if (filterData.division_id.length === 0) {
      delete filterData.division_id;
      delete filterData.district_id;
      delete filterData.upazila_id;
      delete filterData.season_name;
    }

    //API calling --> filter
    // Array.isArray(filterData) !== 0 &&
    axios
      .post(myApi + "/api/v1/searchHealthData", filterData)
      .then((response) => {
        this.setState({
          selectedSeason: this.state?.selectedSeason.filter((e) => e !== false),
          updateGenderChart: response ? response?.data?.ageGenderChart : [],
          searchResult: response ? response.data : [],
          gender_charts: response?.data?.ageGenderChart,
          totalPatients: response?.data?.Gender,
          timelineData: response?.data?.monthlyAffected,
        });

        if (response?.data?.count === 0) {
          Swal.fire({
            icon: "info",
            title: "No data available",
            showConfirmButton: false,
            timer: 1300,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  getTotalPatient = (data = []) => {
    let tempTotalPatients = 0;
    data.forEach((e) => {
      tempTotalPatients += Number(e.total_patient);
    });
    return tempTotalPatients;
  };
  handleOnSearch = (string, results) => {
    console.log(string, results);
  };
  handleOnSelect = (item) => {
    // console.log(item);
    if (item && item.id) {
      this.setState({ selectedDisease: item?.id }, () => {
        if (this.state.selectedDisease) {
          //getting filter data
          axios
            .post(myApi + "/api/v1/searchHealthData", {
              disease_id: this.state?.selectedDisease,
            })
            .then((response) => {
              this.setState({
                diseaseName: item?.name,
                updateGenderChart: response
                  ? response?.data?.ageGenderChart
                  : [],
                searchResult: response ? response.data : [],
                gender_charts: response?.data?.ageGenderChart,
                totalPatients: response?.data?.Gender,
                timelineData: response?.data?.monthlyAffected,
              });
              if (response?.data?.count === 0) {
                Swal.fire({
                  icon: "info",
                  title: "No data available",
                  showConfirmButton: false,
                  timer: 1300,
                });
              }
            })
            .catch((error) => {
              console.log(error);
            });
        }
      });
    } else {
      this.setState({ selectedDisease: "" });
    }
  };
  handleOnFocus = () => {
    console.log("Focused");
  };
  render() {
    return (
      <Fragment>
        <div
          style={{
            width: "100%",
            height: "90px",
            background: "#C1C1C1",
            // position: "fixed",
          }}
        >
          <div style={{ flexGrow: "1" }}>
            <Grid container spacing={4}>
              <Grid item xs={3}>
                <div className="topBarFlex">
                  <HeaderLogo />
                </div>
              </Grid>
              <Grid item xs={5}>
                <div className="topBarFlex" style={{ marginTop: "15px" }}>
                  <ReactSearchAutocomplete
                    className="topSearch"
                    style={{ width: "300px", maxWidth: "300px" }}
                    placeholder={"Search Disease"}
                    items={this.state.diseaseList}
                    onSearch={this.handleOnSearch}
                    onSelect={this.handleOnSelect}
                    onFocus={this.handleOnFocus}
                    styling={{ zIndex: 2 }} // To display it on top of the search box below
                    autoFocus
                  />
                </div>
              </Grid>

              <Grid item xs={3}>
                <div className="topBarFlex"></div>
              </Grid>
            </Grid>
          </div>
        </div>

        {/* //end header */}
        <Grid container spacing={4} className="content">
          <Grid item xs={12} lg={7}>
            <div className="scroll-area rounded  shadow-overflow">
              <div className="diseaseDiv">
                {/* disease name goes here */}
                <span className="diseaseText">{this.state.diseaseName}</span>
                {/* report button goes here */}
                <Button className="dReportButton">
                  <span>Download Report</span>&nbsp;
                  <span className="btn-wrapper--icon">
                    <CloudDownloadTwoToneIcon className="font-size-xl" />
                  </span>
                </Button>
              </div>
              <PerfectScrollbar style={{ overflow: "hidden" }}>
                <Card className="card-box mb-4" style={{ overflow: "hidden" }}>
                  <ValidatorForm
                    onSubmit={this.handleFilterAction}
                    onError={(errors) => errors.null}
                  >
                    <WrapperSimple sectionHeading="Filter">
                      {/* location field (divission, sub-divission, upazilla) */}
                      <div className="textStyleFilter">Location</div>
                      <Grid item xs={12} lg={12} className="flexItem">
                        <Select
                          ref={(ref) => {
                            this.divisionRef = ref;
                          }}
                          value={this.state.selectedDivisionOld.map((e) => ({
                            label: e.label,
                            value: e.value,
                          }))}
                          placeholder="Select Division"
                          options={this.state.locationDivission}
                          onChange={this.onChangeDivision}
                          isClearable="true"
                          isMulti
                        />
                        {this.state.selectedLocation.length !== 0 &&
                        this.state.isDivisionSubmitted ? (
                          <Select
                            ref={(ref) => {
                              this.district = ref;
                            }}
                            value={this.state.selectedSubDivisionNew.map(
                              (e) => ({
                                label: e.label,
                                value: e.value,
                              })
                            )}
                            classNamePrefix="select"
                            placeholder="Select District"
                            options={this.state.locationSubDivission}
                            onChange={this.onChangeSubDivision}
                            isMulti
                            isClearable="true"
                          />
                        ) : (
                          <Select
                            ref={(ref) => {
                              this.dirtrictRef = ref;
                            }}
                            value={null}
                            placeholder="Select District"
                            options={this.state.locationSubDivission}
                            isClearable="true"
                            isDisabled
                          />
                        )}
                        {this.state.selectedSubLocation.length !== 0 &&
                        this.state.isSubDivisionSubmitted ? (
                          <Select
                            value={this.state.selectedUpazillaNew.map((e) => ({
                              label: e.label,
                              value: e.value,
                            }))}
                            placeholder="Select Upazila"
                            options={this.state.locationUpazilla}
                            onChange={this.onChangeUpazilla}
                            isClearable="true"
                            isMulti
                          />
                        ) : (
                          <Select
                            value={this.state.selectedUpazillaNew.map((e) => ({
                              label: e.label,
                              value: e.value,
                            }))}
                            placeholder="Select Upazila"
                            options={this.state.locationUpazilla}
                            isClearable="true"
                            isMulti
                            isDisabled
                          />
                        )}
                      </Grid>
                      <div className="textStyleFilter">Seasons</div>

                      <div className="checkBoxMargin checkBoxText">
                        <input
                          style={{ margin: "10px", marginTop: "-10px" }}
                          className="checkBoxText"
                          type="checkbox"
                          onClick={this.handleAllChecked}
                          value="checkedall"
                        />
                        Select All
                        <ul className="checkBoxText">
                          {this.state.seasons.map((season) => {
                            return (
                              <CheckBox
                                className="checkBoxText"
                                handleCheckChieldElement={
                                  this.handleCheckChieldElement
                                }
                                {...season}
                              />
                            );
                          })}
                        </ul>
                      </div>
                      <Button
                        variant="contained"
                        type="submit"
                        className="btnFilter"
                        color="primary"
                        onClick={this.handleFilterAction}
                      >
                        Submit
                      </Button>
                    </WrapperSimple>
                  </ValidatorForm>
                </Card>
                <Card className="card-box mb-4">
                  <WrapperSimple sectionHeading="Timeline">
                    <ApexChartsArea
                      data={this.state.timelineData}
                      style={{ padding: "0px" }}
                    />
                  </WrapperSimple>
                </Card>
                <Grid container spacing={2}>
                  <Card
                    className="card-box mb-4"
                    style={{ width: "100%", margin: "10px" }}
                  >
                    <Grid container spacing={2}>
                      <Grid item xs={12} lg={5}>
                        <div className="card-header">
                          <div className="card-header--title">
                            <b>Season</b>
                          </div>
                        </div>
                        <div>
                          <SeasonData
                            data={this.state.selectedSeason}
                            seasonData={this.state.searchResult?.Season}
                          />
                        </div>
                        <div className="card-header">
                          <div className="card-header--title">
                            <b>Location</b>
                          </div>
                        </div>
                        <PerfectScrollbar
                          className="scroll-area-lg shadow-overflow autoHeight"
                          style={{ marginBottom: "10px" }}
                        >
                          <div
                            className="card-body px-0 pt-2 pb-3"
                            style={{ maxHeight: "300px" }}
                          >
                            <table className="table table-hover table-borderless table-alternate text-nowrap mb-0">
                              <thead>
                                <tr>
                                  <th className="text-center">SL.</th>
                                  <th className="text-left">Location</th>
                                  <th className="text-center">Cases</th>
                                </tr>
                              </thead>
                              <tbody
                                style={{
                                  minHeight: "100%",
                                  textAlign: "center",
                                }}
                              >
                                <>
                                  {this.state.searchResult?.data ? (
                                    this.state.searchResult?.data?.map(
                                      (e, i) => (
                                        <tr key={e.id}>
                                          <td>{i + 1}</td>
                                          <td className="locationResult">
                                            {e.name}
                                          </td>
                                          <td className="locationPercentage">
                                            {e.total_patient}
                                          </td>
                                        </tr>
                                      )
                                    )
                                  ) : (
                                    <tr>
                                      {" "}
                                      <td>No DATA</td>
                                    </tr>
                                  )}
                                </>
                              </tbody>
                            </table>
                          </div>
                        </PerfectScrollbar>
                      </Grid>

                      <Grid item xs={12} lg={7}>
                        <div className="card-header">
                          <div className="card-header--title">
                            <table width="100%" style={{ textAlign: "center" }}>
                              <tr style={{ margin: "10px" }}>
                                <th width="40%" align="left">
                                  <b>Age & Gender</b>
                                </th>
                                <th width="60%" align="right">
                                  Min:{" "}
                                  <span className="ageGender">
                                    {this.state.searchResult?.ageRange
                                      ? this.state.searchResult?.ageRange?.map(
                                          (e) => e.patient_min_age
                                        )
                                      : 0}
                                  </span>{" "}
                                  &nbsp;&nbsp;&nbsp;&nbsp; Max:{" "}
                                  <span className="ageGender">
                                    {this.state.searchResult?.ageRange
                                      ? this.state.searchResult?.ageRange?.map(
                                          (e) => e.patient_max_age
                                        )
                                      : 0}
                                  </span>
                                </th>
                              </tr>
                            </table>
                          </div>
                        </div>
                        <div style={{ width: "100%" }}>
                          <Grid
                            container
                            spacing={2}
                            style={{ marginTop: "55px", marginBottom: "55px" }}
                          >
                            <Grid item xs={12} lg={5}>
                              <table style={{ width: "100%" }}>
                                <tbody>
                                  <tr>
                                    <th
                                      className="textAlignRC"
                                      textAlign="center"
                                      rowSpan="2"
                                      scope="rowgroup"
                                      // align="right"
                                      width="70%"
                                    >
                                      {/* male image */}
                                      <Avatar
                                        className="marginIcon rcAlign"
                                        alt="..."
                                        src={maleIcon}
                                        style={{
                                          width: "16px",
                                          height: "auto",
                                        }}
                                      />
                                    </th>
                                    <th
                                      scope="row"
                                      width="30%"
                                      className="iconTxt"
                                    >
                                      Male
                                    </th>
                                  </tr>
                                  <tr>
                                    <th
                                      scope="row"
                                      width="30%"
                                      className="percentageTxt"
                                    >
                                      {this.state.searchResult?.Gender?.map(
                                        (e) =>
                                          e.gender === "M" &&
                                          Math.round(
                                            (e.total_patient /
                                              this.getTotalPatient(
                                                this.state.searchResult?.Gender
                                              )) *
                                              100
                                          ) + "%"
                                      )}
                                    </th>
                                  </tr>
                                </tbody>
                              </table>
                            </Grid>
                            <Grid item xs={12} lg={5}>
                              <table style={{ width: "100%" }}>
                                <tbody>
                                  <tr>
                                    <th
                                      scope="row"
                                      width="70%"
                                      className="iconTxt textAlignRC"
                                    >
                                      Female
                                    </th>
                                  </tr>
                                  <tr>
                                    <th
                                      scope="row"
                                      width="70%"
                                      className="percentageTxt textAlignRC"
                                    >
                                      {this.state.searchResult?.Gender?.map(
                                        (e) =>
                                          e.gender === "F" &&
                                          Math.round(
                                            (e.total_patient /
                                              this.getTotalPatient(
                                                this.state.searchResult?.Gender
                                              )) *
                                              100
                                          ) + "%"
                                      )}
                                    </th>
                                    <th
                                      rowSpan="2"
                                      scope="rowgroup"
                                      align="right"
                                      width="70%"
                                    >
                                      {/* female image */}
                                      <Avatar
                                        className="marginIcon lcAlign"
                                        alt="..."
                                        src={femaleIcon}
                                      />
                                    </th>
                                  </tr>
                                </tbody>
                              </table>
                            </Grid>
                          </Grid>
                        </div>
                        <div style={{ width: "100%" }}>
                          <ApexChartsColumn
                            data={this.state.updateGenderChart}
                          />
                        </div>
                      </Grid>
                    </Grid>
                  </Card>
                </Grid>
              </PerfectScrollbar>
            </div>
          </Grid>
          <Grid item xs={12} lg={5}>
            <Card className="card-box mb-4">
              {/* Showing Division Map */}
              <div className="px-4 px-xl-5 pb-1" style={{ margin: "10px" }}>
                {this.state.selectedLocation.length !== 0 &&
                  this.state.selectedSubLocation.length === 0 && (
                    <CheckboxSVGMap
                      map={Bangladesh}
                      onChange={this.mapDivisionChange}
                      selectedLocationIds={this.state.selectedLocation}
                      isLocationSelected={this.state.selectedLocation}
                    />
                  )}

                {/* Showing Sub Division Map */}
                {this.state.selectedLocation.length !== 0 &&
                  this.state.selectedSubLocation.length !== 0 && (
                    <CheckboxSVGMap
                      map={SubDivisionMapBD}
                      onChange={this.mapSubDivisionChange}
                      selectedLocationIds={this.state.selectedSubLocation}
                      isLocationSelected={this.state.selectedSubLocation}
                    />
                  )}

                {/* Showing Blank Division Map */}
                {this.state.selectedLocation.length === 0 &&
                  this.state.selectedSubLocation.length === 0 && (
                    <CheckboxSVGMap
                      map={Bangladesh}
                      onChange={this.mapDivisionChange}
                    />
                  )}
              </div>
            </Card>
          </Grid>
        </Grid>
      </Fragment>
    );
  }

  static propTypes = {
    history: PropTypes.object.isRequired,
  };
}

const mapStateToProps = (state) => ({
  selectedDisease: state.selectedDisease,
});

export default withRouter(connect(mapStateToProps)(MAP));
