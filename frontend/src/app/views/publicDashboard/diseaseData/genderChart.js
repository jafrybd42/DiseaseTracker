import React, { Fragment, useEffect } from "react";

import Chart from "react-apexcharts";

export default function LivePreviewExample(props) {
  console.log(
    props?.data
      ?.map((e) => e.gender === "M" && e !== false && e.COUNT)
      .filter(Boolean)
  );
  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false);
  const options = {
    dataLabels: {
      enabled: true,
    },
    chart: {
      toolbar: {
        show: false,
      },
    },
    xaxis: {
      categories: props?.data?.map((e) => e.rnge),
    },
  };

  const series = [
    {
      name: "Male",
      data: props?.data
        ?.map((e) => e.gender === "M" && e !== false && e.COUNT)
        .filter(Boolean),
    },
    {
      name: "Female",
      data: props?.data
        ?.map((e) => e.gender === "F" && e !== false && e.COUNT)
        .filter(Boolean),
    },
  ];

  return (
    <Fragment>
      <Chart options={options} series={series} type="bar" />
    </Fragment>
  );
}
