import React, { Fragment } from 'react';

import { Grid, Card } from '@material-ui/core';
import { Avatar } from '@material-ui/core';
import sun from '../../../../../src/assets/img/sun.svg';
import snowflake from '../../../../../src/assets/img/snowflake.svg';
import rainy from '../../../../../src/assets/img/rainy.svg';
import flower from '../../../../../src/assets/img/flower.svg';
import autumn from '../../../../../src/assets/img/autumn.svg';

export default function LivePreviewExample(props) {
  var spring,
    summer,
    autumnn,
    winterr,
    rainyy = false;

  const getTotalPatient = (data = []) => {
    let tempTotalPatients = 0;
    data.forEach(e => {
      tempTotalPatients += Number(e.total_patient);
    });
    return tempTotalPatients;
  };

  const getSpring = (data = []) => {
    data.forEach(e => {
      if (e === 'Spring') {
        spring = true;
      }
    });
    return spring;
  };
  const getSummer = (data = []) => {
    data.forEach(e => {
      if (e === 'Summer') {
        summer = true;
      }
    });
    return summer;
  };
  const getAutumn = (data = []) => {
    data.forEach(e => {
      if (e === 'Autumn') {
        autumnn = true;
      }
    });
    return autumnn;
  };
  const getWinter = (data = []) => {
    data.forEach(e => {
      if (e === 'Winter') {
        winterr = true;
      }
    });
    return winterr;
  };
  const getRainy = (data = []) => {
    data.forEach(e => {
      if (e === 'Rainy') {
        rainyy = true;
      }
    });
    return rainyy;
  };
  console.log(props);
  return (
    <Fragment>
      <Card
        className="p-4 mb-4"
        style={{
          boxShadow:
            '0px 2px 1px -1px rgb(250 250 250 / 20%), 0px 1px 1px 0px rgb(250 250 250 / 14%), 0px 1px 3px 0px rgb(250 250 250 / 12%)'
        }}>
        <Grid container spacing={4} className="d-flex align-items-center">
          <Grid
            item
            xs={12}
            md={12}
            style={{ width: '100%', marginLeft: '10px' }}>
            <div
              className="d-flex justify-content-center"
              style={{ width: '100%' }}>
              <div>
                <div className="text-center font-size-lg px-4">
                  <span className="font-weight-bold">
                    {getSpring(props?.data) === true ? (
                      <Avatar className="activeView" alt="..." src={flower} />
                    ) : (
                      <Avatar className="deactiveView" alt="..." src={flower} />
                    )}
                  </span>
                  <hr />
                  <small className="text-black-50 d-block">
                    {props?.seasonData?.map(
                      e =>
                        e.season === 'Spring' &&
                        Math.round(
                          (e.total_patient /
                            getTotalPatient(props?.seasonData)) *
                            100
                        ) + '%'
                    )}
                  </small>
                </div>
              </div>
              <div>
                <div className="text-center font-size-lg px-4">
                  <span className="font-weight-bold text-first">
                    {getSummer(props?.data) === true ? (
                      <Avatar className="activeView" alt="..." src={sun} />
                    ) : (
                      <Avatar className="deactiveView" alt="..." src={sun} />
                    )}
                  </span>
                  <hr />
                  <small className="text-black-50 d-block">
                    {props?.seasonData?.map(
                      e =>
                        e.season === 'Summer' &&
                        Math.round(
                          (e.total_patient /
                            getTotalPatient(props?.seasonData)) *
                            100
                        ) + '%'
                    )}
                  </small>
                </div>
              </div>
              <div>
                <div className="text-center font-size-lg px-4">
                  <span className="font-weight-bold">
                    {getAutumn(props?.data) === true ? (
                      <Avatar className="activeView" alt="..." src={autumn} />
                    ) : (
                      <Avatar className="deactiveView" alt="..." src={autumn} />
                    )}
                  </span>
                  <hr />
                  <small className="text-black-50 d-block">
                    {props?.seasonData?.map(
                      e =>
                        e.season === 'Autumn' &&
                        Math.round(
                          (e.total_patient /
                            getTotalPatient(props?.seasonData)) *
                            100
                        ) + '%'
                    )}
                  </small>
                </div>
              </div>
              <div>
                <div className="text-center font-size-lg px-4">
                  <span className="font-weight-bold">
                    {getWinter(props?.data) === true ? (
                      <Avatar
                        className="activeView"
                        alt="..."
                        src={snowflake}
                      />
                    ) : (
                      <Avatar
                        className="deactiveView"
                        alt="..."
                        src={snowflake}
                      />
                    )}
                  </span>
                  <hr />
                  <small className="text-black-50 d-block">
                    {props?.seasonData?.map(
                      e =>
                        e.season === 'Winter' &&
                        Math.round(
                          (e.total_patient /
                            getTotalPatient(props?.seasonData)) *
                            100
                        ) + '%'
                    )}
                  </small>
                </div>
              </div>
              <div>
                <div className="text-center font-size-lg px-4">
                  <span className="font-weight-bold">
                    {getRainy(props?.data) === true ? (
                      <Avatar className="activeView" alt="..." src={rainy} />
                    ) : (
                      <Avatar className="deactiveView" alt="..." src={rainy} />
                    )}
                  </span>
                  <hr />
                  <small className="text-black-50 d-block">
                    {props?.seasonData?.map(
                      e =>
                        e.season === 'Rainy' &&
                        Math.round(
                          (e.total_patient /
                            getTotalPatient(props?.seasonData)) *
                            100
                        ) + '%'
                    )}
                  </small>
                </div>
              </div>
            </div>
          </Grid>
        </Grid>
      </Card>
    </Fragment>
  );
}
