import React, { Fragment } from 'react';

import Chart from 'react-apexcharts';

export default function LivePreviewExample(props) {
  console.log(props);

  const options = {
    chart: {
      toolbar: {
        show: false
      },
    },
    xaxis: {
      categories: props?.data?.map(e => e.month)
    }
  };
  const series = [
    {
      name: 'Effected People',
      data: props?.data?.map(e => parseInt(e.total_patient))
    }
  ];

  return (
    <Fragment>
      <Chart options={options} series={series} type="area" height={250} />
    </Fragment>
  );
}
