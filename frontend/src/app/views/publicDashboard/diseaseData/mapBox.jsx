import * as React from 'react';
import { useState } from 'react';
import ReactMapGL from 'react-map-gl';

function Map() {
  const [viewport, setViewport] = useState({
    width: '70vh',
    height: '80vh',
    latitude: 23.684994,
    longitude: 90.356331,
    zoom: 6
  });
  var mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
  const [location, setLocation] = useState(null);
  // mapboxgl.accessToken =
  //   'pk.eyJ1IjoiamFmcnliZCIsImEiOiJja21hYW13dDIxNDFkMm9wOWRsZDcweXd1In0.GN9_1hQkSJvIYNpSx2OE4w';
  // var map = new mapboxgl.Map({
  //   container: "map",
  //   style: 'mapbox://styles/mapbox/streets-v11'
  // });
  const onClickController = (e) => {
    setLocation(e);
  };
  return (
    <div id="map">
      <ReactMapGL
        {...viewport}
        // style={"mapbox://styles/mapbox/streets-v9"}
        onClick={onClickController}
        minZoom={6}
        mapboxApiAccessToken="pk.eyJ1IjoiamFmcnliZCIsImEiOiJja21hYW13dDIxNDFkMm9wOWRsZDcweXd1In0.GN9_1hQkSJvIYNpSx2OE4w"
        onViewportChange={nextViewport => setViewport(nextViewport)}
      />
    </div>
  );
}
export default Map;
