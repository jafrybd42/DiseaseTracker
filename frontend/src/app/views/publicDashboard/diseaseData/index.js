import React, { Fragment } from "react";
import FilterDisease from "./filterDisease";

export default function dashboard() {
  return (
    <Fragment>
      <FilterDisease />
    </Fragment>
  );
}
