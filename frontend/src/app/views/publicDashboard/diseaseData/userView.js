import React, { Fragment } from "react";
import FilterDisease from "./userAccessDisease";

export default function dashboard() {
  return (
    <Fragment>
      <FilterDisease />
    </Fragment>
  );
}
