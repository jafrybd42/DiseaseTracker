/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, Fragment, useEffect } from 'react';
import { Avatar, Grid } from '@material-ui/core';
import Bangladesh from './inc/svg-maps/bd-divission/index.js';
import { CheckboxSVGMap } from './inc/react-svg-map-index/src/index'
// import './inc/react-svg-map-index/lib/index.css';
import axios from 'axios';
import { Link } from 'react-router-dom';
import myApi from 'app/auth/api.js';
import HeaderLogo from './Header/header'
import map from './inc/svg-maps/bd-divission/bangladesh.svg';

export default function indexpage() {
  const [diseaseList, setDiseaseList] = useState([]);

  const [totalEffected, setTotalEffected] = useState('');

  useEffect(() => {
    axios
      .get(myApi + '/api/v1/topTenDisease')
      .then(function(response) {
        response &&
          response.data &&
          setDiseaseList(
            Array.isArray(response.data.data) ? response.data.data : []
          );
      })
      .catch(function(error) {
        console.log(error);
      })
      .then(function() {
        // always executed
      });

    //total disease
    axios
      .get(myApi + '/api/v1/totalDiseasePeople')
      .then(response => {
        response &&
          response.data &&
          response.data.data &&
          setTotalEffected(
            response.data.data.map(e => e.NumberOfPeopleAffected)
          );
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  }, false);

  return (
    <Fragment>
      <HeaderLogo />
      <Grid container spacing={4} className='contentOld'>
        <Grid item xs={12} lg={7}>
          <div className="gridDashboard">
            <Grid container spacing={3}>
              <Grid item xs={12} lg={5} className="dataStat">
                <div className="topDiseaseList">
                  <p className="title">Top Diseases</p>

                  {diseaseList.length !== 0 && (
                    <>
                      {diseaseList.map(e => e?.disease_id && (
                        <Link
                          to={{
                            pathname: '/public/disease/'+e.disease_id,
                            state: e
                          }}>
                          <div className="diseaseBlock">{e.disease_name}</div>
                        </Link>
                      ))}
                    </>
                  )}
                </div>
              </Grid>
              <Grid item xs={12} lg={5} className="dataStat">
                <div className="stats statUpdate">
                  <p className="statsHead">Total Population</p>
                  <p className="statsInfo">163 million</p>

                  <p
                    style={{ textAlign: 'right', marginLeft: 'auto' }}
                    className="statsHead">
                    Population Density
                  </p>
                  <p style={{ textAlign: 'right' }} className="statsInfo">
                    1.26 K
                  </p>

                  <p className="statsHead">Total Effected (Overall)</p>
                  <p className="statsInfo">
                    {totalEffected > 1000
                      ? Number(totalEffected / 1000) + ' K'
                      : totalEffected}
                  </p>

                  <p
                    style={{ textAlign: 'right', marginLeft: 'auto' }}
                    className="statsHead">
                    Mortality Rate
                  </p>
                  <p style={{ textAlign: 'right' }} className="statsInfo">
                    25.6 K
                  </p>
                </div>
              </Grid>
            </Grid>
          </div>
        </Grid>
        <Grid item xs={12} lg={5} className="mapMedium">
          <div
            className="px-4 px-xl-5 pb-1"
            id="map"
            style={{ margin: '10px' }}>
            <CheckboxSVGMap
              map={Bangladesh}
              className='dgg'
            />
            {/* <img viewBox="0 0 1000 1295" alt="map" src={map}  /> */}
          </div>
        </Grid>
      </Grid>
    </Fragment>
  );
}

