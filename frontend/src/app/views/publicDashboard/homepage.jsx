import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../assets/img/logo.svg';
import map from '../../../assets/img/mapBG.svg';
import HeaderLogo from './Header/header'
import {
  Avatar
} from '@material-ui/core';
export default function home() {
  return (
    <Fragment>
      <HeaderLogo />
      <Avatar alt="map" src={map} id="mapBG" />

      <section id="indexInfo">
        <Avatar alt="logo" src={logo} id="indexLogo" />
        <p id="indexProName">Bangladesh Health Monitor</p>
        <p id="indexDescription">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est
          laborum
        </p>

        <Link to={{ pathname: '/public/index' }}>
          <button id="visitDashboardBtn">Visit Dashboard</button>
        </Link>
      </section>
    </Fragment>
  );
}
