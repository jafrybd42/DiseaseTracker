import SignIn from "./homepage";
import Index from "./index";
import filterDisease from "./diseaseData/index";

const settings = {
  activeLayout: "layout1",
  layout1Settings: {
    topbar: {
      show: false,
    },
    leftSidebar: {
      show: false,
      mode: "close",
    },
  },
  layout2Settings: {
    mode: "full",
    topbar: {
      show: false,
    },
    navbar: { show: false },
  },
  secondarySidebar: { show: false },
  footer: { show: true },
};

const publicDashboardRoutes = [
  {
    path: "/public/dashboard",
    component: SignIn,
    settings,
  },
  {
    path: "/public/index",
    component: Index,
    settings,
  },
  {
    path: "/public/disease/:id",
    component: filterDisease,
    settings,
  },
];

export default publicDashboardRoutes;
