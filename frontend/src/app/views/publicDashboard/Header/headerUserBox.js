import React, { useState, Fragment, useEffect } from "react";
import axios from "axios";
import Select from "react-select";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import myApi from "app/auth/api";
import ReactSearchAutocomplete from "react-search-autocomplete/dist/components/ReactSearchAutocomplete";
import Swal from "sweetalert2";

// import { connect } from 'react-redux';
export default function HeaderUserbox(props) {
  console.log(props);

  const history = useHistory();

  const [diseaseList, setDiseaseList] = React.useState([]);
  const [selectedDisease, setSelectedDisease] = React.useState([]);
  useEffect(() => {
    //total disease
    axios
      .get(myApi + "/api/v1/diseaseList")
      .then((response) => {
        response &&
          response.data &&
          response.data.data &&
          setDiseaseList(
            response.data.data.map((e) => ({
              id: e.id,
              name: e.disease_name,
              status: e.status,
            }))
          );
      })
      .catch((error) => {
        console.log(error);
      });
  }, false);
  const onChangeDiseaseSelect = (value) => {
    if (value !== null) {
      setSelectedDisease(value);
      history.push({
        pathname: "/public/disease/" + value.value,
        data: value,
        state: value,
      });
      // window.location.reload(false);
    }
    console.log(selectedDisease);
  };
  const handleOnSearch = (string, results) => {
    console.log(string, results);
  };
  const handleOnSelect = (item) => {
    // console.log(item);
    if (item && item.id) {
      setSelectedDisease(item);
      history.push({
        pathname: "/public/disease/" + item.id,
        data: item,
        state: item,
      });
      // this.setState({ selectedDisease: item?.id }, () => {
      //   if (this.state.selectedDisease) {
      //     //getting filter data
      //     axios
      //       .post(myApi + "/api/v1/searchHealthData", {
      //         disease_id: this.state?.selectedDisease,
      //       })
      //       .then((response) => {
      //         this.setState({
      //           diseaseName: item?.name,
      //           updateGenderChart: response
      //             ? response?.data?.ageGenderChart
      //             : [],
      //           searchResult: response ? response.data : [],
      //           gender_charts: response?.data?.ageGenderChart,
      //           totalPatients: response?.data?.Gender,
      //           timelineData: response?.data?.monthlyAffected,
      //         });
      //         if (response?.data?.count === 0) {
      //           Swal.fire({
      //             icon: "info",
      //             title: "No data available",
      //             showConfirmButton: false,
      //             timer: 1300,
      //           });
      //         }
      //       })
      //       .catch((error) => {
      //         console.log(error);
      //       });
      //   }
      // });
    } else {
      setSelectedDisease("");
    }
  };
  const handleOnFocus = () => {
    console.log("Focused");
  };
  return (
    // Header Disease Selection
    <Fragment className="headerRight">
      <div className="dropdown_style">
        <ReactSearchAutocomplete
          // className="topSearch"
          style={{ width: "300px", maxWidth: "300px", marginLeft: "-40%" }}
          placeholder={"Search Disease"}
          items={diseaseList}
          onSearch={handleOnSearch}
          onSelect={handleOnSelect}
          onFocus={handleOnFocus}
          styling={{ zIndex: 2 }} // To display it on top of the search box below
          autoFocus
        />
        {/* <Select
          // value={
          //   selectedDisease
          //     ? diseaseList.map((e) => e.value === props.selectedData)
          //     : props.selectedData
          // }
          placeholder="Search disease here"
          isClearable
          options={diseaseList}
          onChange={onChangeDiseaseSelect}
        /> */}
      </div>
    </Fragment>
  );
}
