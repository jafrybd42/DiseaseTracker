import React, { Fragment } from "react";

import clsx from "clsx";

import { IconButton, AppBar, Box, Tooltip } from "@material-ui/core";
import { connect } from "react-redux";
import HeaderLogo from "./headerLogo";
import HeaderUserbox from "./headerUserBox";

import MenuOpenRoundedIcon from "@material-ui/icons/MenuOpenRounded";
import MenuRoundedIcon from "@material-ui/icons/MenuRounded";
import poppins from "typeface-poppins";

const Header = (props) => {
  console.log(props);
  const toggleSidebarMobile = () => {
    setSidebarToggleMobile(!sidebarToggleMobile);
  };
  const {
    headerShadow,
    headerFixed,
    sidebarToggleMobile,
    setSidebarToggleMobile,
  } = props;

  return (
    <Fragment>
      <AppBar
        style={{ height: "70px!important", font: poppins }}
        className={clsx("app-header", {})}
        position={headerFixed ? "fixed" : "absolute"}
        elevation={headerShadow ? 11 : 3}
      >
        {!props.isCollapsedLayout && <HeaderLogo />}

        <Box className="d-flex align-items-center" style={{ width: "50%" }}>
          <HeaderUserbox selectedData={props.selectedData}/>
          <Box className="toggle-sidebar-btn-mobile">
            <Tooltip title="Toggle Sidebar" placement="right">
              <IconButton
                color="inherit"
                onClick={toggleSidebarMobile}
                size="medium"
              >
                {sidebarToggleMobile ? (
                  <MenuOpenRoundedIcon />
                ) : (
                  <MenuRoundedIcon />
                )}
              </IconButton>
            </Tooltip>
          </Box>
        </Box>
      </AppBar>
    </Fragment>
  );
};

const mapStateToProps = (state) => ({
  //   headerShadow: state.ThemeOptions.headerShadow,
  //   headerFixed: state.ThemeOptions.headerFixed,
  //   sidebarToggleMobile: state.ThemeOptions.sidebarToggleMobile
});

export default connect(mapStateToProps)(Header);
