import React, { Fragment } from 'react';

import clsx from 'clsx';
import { Link } from 'react-router-dom';

import { IconButton, Box } from '@material-ui/core';

import projectLogo from '../../../../assets/img/logo.svg';

const HeaderLogo = () => {
  return (
    <Fragment>
      <div className={clsx('app-header-logo', {})}>
        <Box
          className="header-logo-wrapper"
          title="Bangladesh Health Monitor">
          <Link to="/public/dashboard" className="header-logo-wrapper-link">
            <IconButton
              color="primary"
              size="medium"
              className="header-logo-wrapper-btn">
              <img
                className="app-header-logo-img"
                alt="Bangladesh Disease Tracking"
                src={projectLogo}
              />
            </IconButton>
          </Link>
          <Link to="/public/dashboard" className="header-logo-wrapper-link">
            <Box className="header-logo-text">Bangladesh Disease Tracking</Box>
          </Link>
        </Box>
      </div>
    </Fragment>
  );
};

export default HeaderLogo;
