/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import axios from "axios";
import myApi from "../../../auth/api";
import history from "../../../../history";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
function AddEditForm(props) {
  const [item, setItem] = useState([]);
  const [organizationList, setOrganizationList] = useState([]);
  const [form, setValues] = useState({
    id: 0,
    name: '',
    organization_id: "",
    phone_number: "",
    email: "",
    status: 0,
  });

  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const submitFormAdd = (e) => {
    e.preventDefault();
    axios
      .post(
        myApi + "/api/v1/organization/add",
        {
          // id: form.id,
          name: form.name,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )

      .then((response) => {
        if (response.data.success) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500,
          });
        }
        // Swal.fire('Updated Successfully')
        if (response.data.success === true) {
          props.addItemToState(form);
        }
        props.toggle();
      })

      .catch((err) => console.log(err));
  };

  const submitFormEdit = (e) => {
    e.preventDefault();

    axios
      .post(
        myApi + "/api/v1/user/updateProfile",
        {
          id: form.id,
          name: form.name,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )

      .then((response) => {
        if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500,
          });
        }
        // Swal.fire('Updated Successfully')
        if (response.data.error === false) {
          props.updateState(form);
        }
        props.toggle();
      })

      .catch((err) => console.log(err));
  };
  console.log(props.item);
  useEffect(() => {
    if (props.item) {
      const { id,name, organization_id, phone_number, email, status } = props.item;
      setValues({
        id,
        name,
        organization_id,
        phone_number,
        email,
        status,
      });
    }
    // }, false);

    // useEffect(() => {
    // getting clientType
    axios
      .get(myApi + "/api/v1/organization/allList", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let orgsFromApi = res.data.data.map((org) => {
          return {
            id: org.id,
            type: org.name,
          };
        });
        setOrganizationList([].concat(orgsFromApi));
        setOrganizationList(
          [
            {
              id: 0,
              type: "Organization List",
            },
          ].concat(orgsFromApi)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }, false);

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      {/* <FormGroup>
        <Label for="organization_id">Organization</Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="organization_id"
          value={form.organization_id || props.item.organization_id}
          onChange={onChange}
        >
          {organizationList.map((org) => (
            <option key={org.id} value={org.id}>
              {org.type}
            </option>
          ))}
        </select>
      </FormGroup> */}
      <FormGroup>
        <Label for="name">name</Label>
        <Input
          type="text"
          name="name"
          id="name"
          onChange={onChange}
          value={form.name === null ? "" : form.name}
          minLength="3"
          required
        />
      </FormGroup>
      {/* <FormGroup>
        <Label for="name">Email</Label>
        <Input
          type="email"
          name="email"
          id="email"
          onChange={onChange}
          value={form.email === null ? "" : form.email}
          minLength="3"
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for="name">Phone</Label>
        <Input
          type="text"
          name="phone_number"
          id="phone_number"
          onChange={onChange}
          value={form.phone_number === null ? "" : form.phone_number}
          minLength="11"
          required
        />
      </FormGroup> */}

      <Button>Submit</Button>
    </Form>
  );
}

export default AddEditForm;
