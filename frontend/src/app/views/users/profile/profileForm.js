import React, { useState } from "react";
import axios from "axios";
import myApi from "../../../auth/api";
import { Button, Icon } from "@material-ui/core";
import Swal from "sweetalert2";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import localStorageService from "app/services/localStorageService";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateUserProfile } from "app/redux/actions/UserActions";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

function ProfileForm(props) {
  let { user } = props;
  const [form, setValues] = useState({
    name: user.displayName,
    phone_number: "",
    email: "",
    organization_id: "",
    status: "",
    value: "",
    isProfile: true,
    isEdit: false,
    isChangePass: false,
    newPass: "",
    oldPass: "",
    confirmPass: "",
  });

  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const onChange = (e) => {
    ValidatorForm.addValidationRule("isPasswordMatch", (value) => {
      if (value !== form.newPass) {
        return false;
      }
      return true;
    });
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
    console.log(form);
  };

  const editAction = (e) => {
    setValues({
      ...form,
      isProfile: false,
      isEdit: true,
      isChangePass: false,
    });
  };

  const passwordAction = (e) => {
    setValues({
      ...form,
      isProfile: false,
      isEdit: false,
      isChangePass: true,
    });
  };
  const backAction = (e) => {
    setValues({
      ...form,
      isProfile: true,
      isEdit: false,
      isChangePass: false,
    });
  };

  const handleSubmit = (e) => {
    axios
      .post(
        myApi + "/api/v1/user/updateProfile",
        {
          name: form.name,
        },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )
      .then((res) => {
        if (res.data.error) {
          Swal.fire({
            position: "top-end",
            icon: "error",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
          throw new Error(res.data.message);
        } else {
          Swal.fire({
            position: "top-end",
            icon: "success",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
        }
        if (!res.data.error) {
          props.updateUserProfile(
            form.name,
            localStorageService.getItem("auth_user")?.email
          );
          setValues({
            ...form,
            isProfile: true,
            isEdit: false,
            isChangePass: false,
          });
        }
        if (!res.data.success) {
          props.updateUserProfile(
            localStorageService.getItem("auth_user")?.displayName,
            localStorageService.getItem("auth_user")?.email
          );
          setValues({
            ...form,
            isProfile: false,
            isEdit: true,
            isChangePass: false,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleChangePass = (e) => {
    axios
      .post(
        myApi + "/api/v1/user/passwordChange",
        {
          old_password: form.oldPass,
          new_password: form.newPass,
          confirm_password: form.confirmPass,
        },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )
      .then((res) => {
        if (!res.data.success) {
          Swal.fire({
            position: "top-end",
            icon: "error",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
          throw new Error(res.data.message);
        } else {
          Swal.fire({
            position: "top-end",
            icon: "success",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
        }
        if (res.data.success) {
          setValues({
            ...form,
            newPass: "",
            oldPass: "",
            confirmPass: "",
            isProfile: true,
            isEdit: false,
            isChangePass: false,
          });
        }
        if (!res.data.success) {
          setValues({
            ...form,
            isProfile: false,
            isEdit: false,
            isChangePass: true,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };
  let { oldPass, newPass, confirmPass } = form;
  return (
    <>
      <Grid container spacing={3} style={{ width: "100%" }}>
        <Grid item xs={4}>
          <Paper style={{ boxShadow: "none", marginTop: "30px" }}>
            <div className="profilePic">
              <img
                src={localStorageService.getItem("auth_user")?.photoURL}
                alt="profile pic"
              />
            </div>
          </Paper>
        </Grid>
        <Grid item xs={8} style={{ marginBottom: "20px" }}>
          <Paper style={{ boxShadow: "none", marginTop: "30px" }}>
            <div className="profileFlex">
              {form.isProfile ? (
                <Button className="editprofile" onClick={editAction}>
                  <Icon style={{ fontSize: "14px" }}>edit</Icon>&nbsp;&nbsp;Edit
                </Button>
              ) : (
                <Button className="editprofile" onClick={backAction}>
                  <Icon style={{ fontSize: "14px" }}>arrow_back</Icon>
                  &nbsp;&nbsp;Back
                </Button>
              )}

              <Button className="changePassword" onClick={passwordAction}>
                <Icon style={{ fontSize: "14px" }}>vpn_key</Icon>
                &nbsp;&nbsp;Change Password
              </Button>
            </div>
            {form.isProfile && (
              <div>
                <div className="editProfileLabel">Username</div>
                <div className="editProfileTxt">{form.name}</div>
                <div className="editProfileLabel">Email</div>
                <div className="editProfileTxt">
                  {localStorageService.getItem("auth_user")?.email}
                </div>
                {localStorageService.getItem("auth_user")?.phone !== " " && (
                  <div className="editProfileLabel">Phone Number</div>
                )}
                {localStorageService.getItem("auth_user")?.phone !== " " && (
                  <div className="editProfileTxt">
                    {localStorageService.getItem("auth_user")?.phone}
                  </div>
                )}
                {localStorageService.getItem("auth_user")?.role_name !==
                  " " && <div className="editProfileLabel">Role</div>}
                {localStorageService.getItem("auth_user")?.role_name !==
                  " " && (
                  <div className="editProfileTxt">
                    {localStorageService.getItem("auth_user")?.role_name}
                  </div>
                )}
              </div>
            )}

            {form.isEdit && (
              <div>
                <ValidatorForm
                  onSubmit={handleSubmit}
                  onError={(errors) => null}
                >
                  <Grid item lg={10} md={10} sm={12} xs={12}>
                    <TextValidator
                      autoComplete="off"
                      className="mb-4 w-full"
                      label="User Name"
                      onChange={onChange}
                      type="text"
                      name="name"
                      value={form.name}
                      validators={[
                        "required",
                        "minStringLength: 3",
                        "maxStringLength: 50",
                      ]}
                      errorMessages={["this field is required"]}
                    />
                  </Grid>
                  <Button
                    color="primary"
                    variant="contained"
                    type="submit"
                    style={{ height: "25px" }}
                  >
                    <span className="pl-2 small">Edit</span>
                  </Button>
                </ValidatorForm>
              </div>
            )}

            {form.isChangePass && (
              <div>
                <ValidatorForm
                  onSubmit={handleChangePass}
                  onError={(errors) => null}
                >
                  <Grid item lg={10} md={10} sm={12} xs={12}>
                    <TextValidator
                      autocomplete="off"
                      className="mb-4 w-full"
                      label="Current Password"
                      onChange={onChange}
                      type="password"
                      name="oldPass"
                      value={oldPass}
                      errorMessages={["this field is required"]}
                    />

                    <TextValidator
                      autocomplete="password"
                      className="mb-4 w-full"
                      label="Password (Min Length : 6)"
                      onChange={onChange}
                      name="newPass"
                      type="password"
                      value={newPass}
                      validators={["required"]}
                      errorMessages={["this field is required"]}
                    />
                    <TextValidator
                      autoComplete="confirmPassword"
                      className="mb-4 w-full"
                      label="Confirm Password"
                      onChange={onChange}
                      name="confirmPass"
                      type="password"
                      value={confirmPass}
                      validators={["required", "isPasswordMatch"]}
                      errorMessages={[
                        "this field is required",
                        "password didn't match",
                      ]}
                    />
                  </Grid>
                  <Button
                    color="primary"
                    variant="contained"
                    type="submit"
                    style={{ height: "25px" }}
                  >
                    <span className="pl-2 small">Change Password</span>
                  </Button>
                </ValidatorForm>
              </div>
            )}
          </Paper>
        </Grid>
      </Grid>
    </>
  );
}

const mapStateToProps = (state) => ({
  updateUserProfile: PropTypes.func.isRequired,
  user: state.user,
});

export default connect(mapStateToProps, { updateUserProfile })(ProfileForm);
