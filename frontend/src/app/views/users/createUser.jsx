import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import CreateForm from "./CreateForm";

class CreateOrganization extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "All Members", path: "/user/create" },
              { name: "Create" },
            ]}
          />
        </div>


        <Card className="px-6 pt-2 pb-4 createUser">
          <CreateForm />
        </Card>
      </div>
    );
  }
}

export default CreateOrganization;
