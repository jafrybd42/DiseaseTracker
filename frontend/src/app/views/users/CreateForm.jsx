import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService";
import axios from "axios";
import Swal from "sweetalert2";
import { Button, Icon, Grid } from "@material-ui/core";
import "date-fns";
import myApi from "app/auth/api";
import history from "../../../history";
import Select from "react-select";

class CreateForm extends Component {
  constructor() {
    super();
    this.state = {
      organizationName: "",
      isUserCreate: false,
      isOrganizationCreate: true,
      role: [
        { value: 2, label: "Team Member" },
        { value: 3, label: "Management" },
        { value: 4, label: "Participants" },
        { value: 5, label: "Admin" },
      ],
      organization_admin: [
        {
          value: localStorageService?.getItem("auth_user")?.org_id,
          label: localStorageService?.getItem("auth_user")?.org_name,
        },
      ],
      organizationList: "",
      selectedOrganization: "",
      selectedRole: "",
      userName: "",
      email: "",
      phone: "",
      newPassword: "",
      confirmPassword: "",
    };
  }

  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", (value) => {
      if (value !== this.state.newPassword) {
        return false;
      }
      return true;
    });

    //organization List
    axios({
      method: "GET",
      url: myApi + "/api/v1/organization/list",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        this.setState({
          organizationList: Array.isArray(response.data.data)
            ? response.data.data.map((e) => {
                return {
                  value: e.id,
                  label: e.name,
                };
              })
            : [],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleChange = (event) => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = (event) => {
    try {
      axios
        .post(
          myApi + "/api/v1/user/registration",
          {
            organization_id: this.state.selectedOrganization,
            roles_id: this.state.selectedRole,
            email: this.state.email,
            phone_number: this.state.phone,
            name: this.state.userName,
          },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token,
            },
          }
        )
        .then((res) => {
          if (res.data) {
            if (res.data.success) {
              Swal.fire({
                icon: "success",
                title: res.data.message,
                showConfirmButton: false,
                timer: 1000,
              });
            } else {
              Swal.fire({
                icon: "error",
                title: res.data.message,
                showConfirmButton: false,
                timer: 1000,
              });
            }
            if (res.data.success === true) {
              this.setState({
                userName: "",
                email: "",
                phone: "",
                newPassword: "",
                selectedOrganization: "",
                selectedRole: "",
              });
            }
          }
        });
    } catch (err) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Something went wrong!",
      });
    }
  };

  userList = (event) => {
    event.persist();
    history.push("/user/team");
  };

  organizationCreate = (event) => {
    event.persist();
    this.setState({
      isUserCreate: false,
      isOrganizationCreate: true,
    });
  };

  // Controller for Organization Select
  onSelectedOrganization = (value) => {
    if (value !== null) {
      this.setState({ selectedOrganization: value.value });
    }
    if (value === null) {
      this.setState({ selectedOrganization: "" });
    }
  };

  // Controller for Role Select
  onSelectedRole = (value) => {
    if (value !== null) {
      this.setState({ selectedRole: value.value });
    }
    if (value === null) {
      this.setState({ selectedRole: "" });
    }
  };

  render() {
    let {
      userName,
      phone,
      email,
      selectedOrganization,
      selectedRole,
    } = this.state;

    return (
      <div>
        {this.state.isOrganizationCreate === true && (
          <Button
            style={{ marginTop: "-70px" }}
            className="rightFlex"
            onClick={this.userList}
          >
            Members List
          </Button>
        )}

        {this.state.isOrganizationCreate && (
          <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmit}
            onError={(errors) => null}
            style={{
              marginLeft: "20px",
              marginRight: "20px",
            }}
          >
            <Grid container spacing={6} style={{ marginTop: "10px" }}>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <div style={{ marginBottom: "15px", marginTop: "-25px" }}>
                  <strong>
                    <font color="black">Select Organization</font>
                  </strong>
                </div>

                {localStorageService.getItem("auth_user")?.role_id === 1 ? (
                  <Select
                    value={
                      this.state.organizationList.length !== 0 &&
                      this.state.organizationList.filter(
                        (option) => option.value === selectedOrganization
                      )
                    }
                    isClearable="true"
                    name="selectedOrganization"
                    options={this.state.organizationList}
                    className="basic-multi-select rSelect"
                    classNamePrefix="select"
                    onChange={this.onSelectedOrganization}
                  />
                ) : (
                  <Select
                    value={
                      this.state.organization_admin.length !== 0 &&
                      this.state.organization_admin.filter(
                        (option) => option.value === selectedOrganization
                      )
                    }
                    isClearable="true"
                    name="selectedOrganization"
                    options={this.state.organization_admin}
                    className="basic-multi-select rSelect"
                    classNamePrefix="select"
                    onChange={this.onSelectedOrganization}
                  />
                )}
                <div style={{ marginBottom: "50px" }}></div>
                <TextValidator
                  autoComplete="off"
                  className="mb-4 w-full"
                  label="User Name"
                  onChange={this.handleChange}
                  type="text"
                  name="userName"
                  value={userName}
                  validators={["required", "maxStringLength: 50"]}
                  errorMessages={["this field is required"]}
                />
              </Grid>

              <Grid item lg={6} md={6} sm={12} xs={12}>
                <div style={{ marginBottom: "15px", marginTop: "-25px" }}>
                  <strong>
                    <font color="black">Select Role</font>
                  </strong>
                </div>
                <Select
                  isClearable="true"
                  value={
                    this.state.role.length !== 0 &&
                    this.state.role.filter(
                      (option) => option.value === selectedRole
                    )
                  }
                  name="selectedClient"
                  className="basic-multi-select rSelect"
                  classNamePrefix="select"
                  options={this.state.role}
                  onChange={this.onSelectedRole}
                />

                <div style={{ marginBottom: "50px" }}></div>
                {(this.state.selectedRole === 1 ||
                  this.state.selectedRole === 2 ||
                  this.state.selectedRole === 3 ||
                  this.state.selectedRole === 5) && (
                  <TextValidator
                    autoComplete="off"
                    className="mb-4 w-full"
                    label="Email"
                    onChange={this.handleChange}
                    type="text"
                    name="email"
                    value={email}
                    validators={["required", "isEmail"]}
                    errorMessages={["this field is required"]}
                  />
                )}
                {this.state.selectedRole === 4 && (
                  <TextValidator
                    autoComplete="off"
                    className="mb-4 w-full"
                    label="Phone Number"
                    onChange={this.handleChange}
                    type="number"
                    name="phone"
                    value={phone}
                    validators={["required"]}
                    errorMessages={["this field is required"]}
                  />
                )}
              </Grid>
            </Grid>
            <Button color="primary" variant="contained" type="submit">
              <Icon>send</Icon>
              <span className="pl-2 capitalize">Create</span>
            </Button>
          </ValidatorForm>
        )}
      </div>
    );
  }
}

export default CreateForm;
