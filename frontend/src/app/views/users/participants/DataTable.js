import React, { useEffect, useState } from "react";
import { Button } from "reactstrap";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../../auth/api";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import localStorageService from "app/services/localStorageService";
import Swal from "sweetalert2";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import SearchBar from "material-ui-search-bar";
import { Link } from "react-router-dom";

export default function DataTable(props) {
  const [organizationList, setOrganizationList] = useState([]);
  const [form, setValues] = useState({
    name: "",
    phone_number: "",
    email: "",
    organization_id: "",
    status: "",
    value: "",
  });
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };
  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };
  const deleteItem = (
    id,
    name,
    phone_number,
    email,
    organization_id,
    status
  ) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, deactive it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/api/v1/user/deactive",
            { id: id, role_id: 4 },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((res) => {
            Swal.fire({
              icon: "success",
              title: "Deactivated",
              showConfirmButton: false,
              timer: 1000,
            });
          })
          .then((item) => {
            props.updateState({
              id,
              name,
              phone_number,
              email,
              organization_id,
              status: 1,
            });
          })
          .catch((err) => console.log(err));
      }
    });
  };

  const activeItem = (
    id,
    name,
    phone_number,
    email,
    organization_id,
    status
  ) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#1a71b5",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, active it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/api/v1/user/active",
            { id: id, role_id: 4 },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((res) => {
            Swal.fire({
              icon: "success",
              title: "Activated",
              showConfirmButton: false,
              timer: 1000,
            });
          })
          .then((item) => {
            props.updateState({
              id,
              name,
              phone_number,
              email,
              organization_id,
              status: 0,
            });
          });
      }
    });
  };

  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();
  useEffect(() => {
    // getting clientType
    if (localStorageService.getItem("auth_user")?.role_id === 5) {
      let orgFromLocalStorage = [
        {
          id: localStorageService?.getItem("auth_user").org_id,
          type: localStorageService?.getItem("auth_user").org_name,
        }
      ];
      setOrganizationList(orgFromLocalStorage);
    } else {
      axios
        .get(myApi + "/api/v1/organization/allList", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          let orgsFromApi = res.data.data.map((org) => {
            return {
              id: org.id,
              type: org.name,
            };
          });
          setOrganizationList([].concat(orgsFromApi));
          setOrganizationList(
            [
              {
                id: 0,
                type: "Organization List",
              },
            ].concat(orgsFromApi)
          );
        })
        .catch((error) => {
          console.log(error);
        });
    }
    console.log(organizationList)
  }, false);
  const locateValueById = (types, id) => {
    let item = types.find((it) => it.id === Number(id));
    return item;
  };
  return (
    <>
      <div className="tableBox">
        <div style={{ width: "70%", display: "flex" }}>
          <Link to="/user/team" style={{ opacity: "1" }}>
            <div className="unselectedTxt"> Team Members</div>
          </Link>

          <Link to="/user/management" style={{ opacity: "1" }}>
            <div className="unselectedTxt">Management</div>
          </Link>
          <Link to="/user/admin" style={{ opacity: "1" }}>
            <div className="unselectedTxt">Admin</div>
          </Link>
          <div className="tableHeaderTxt" style={{ opacity: "1" }}>
            Participants
          </div>
        </div>
        <div style={{ width: "30%" }}>
          <SearchBar
            className="marginAdd"
            value={form.value}
            onChange={(newValue) => setValues({ ...form, value: newValue })}
            // onRequestSearch={() => doSomethingWith(this.state.value)}
          />
        </div>
      </div>
      <TableContainer
        component={Paper}
        style={{
          boxShadow:
            "0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)",
          backgroundColor: "#fff",
        }}
      >
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "5%" }}
              >
                SL.
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "20%" }}
              >
                Name
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "15%" }}
              >
                Phone
              </TableCell>

              <TableCell
                align="center"
                className="px-0"
                style={{ width: "15%" }}
              >
                Status
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "20%" }}
              >
                Organization
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.items &&
              props.items
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, id) => (
                  <TableRow key={id}>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "5%" }}
                    >
                      {page * rowsPerPage + id + 1}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "20%" }}
                    >
                      {item.name}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "15%" }}
                    >
                      {item.phone_number}
                    </TableCell>

                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "15%" }}
                    >
                      {item.status === 0 ? (
                        <font color="green">Active</font>
                      ) : (
                        <font color="red">Deactivated</font>
                      )}
                    </TableCell>

                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "20%" }}
                    >
                      {locateValueById(
                        organizationList,
                        item.organization_id
                      ) &&
                        locateValueById(organizationList, item.organization_id)
                          .type}
                    </TableCell>
                    {item.status === 0 ? (
                      <TableCell
                        className="px-0 capitalize"
                        align="center"
                        style={{ width: "10%" }}
                      >
                        <Button
                          color="danger"
                          style={{
                            backgroundColor: "#f41b35",
                            borderColor: "#f41b35",
                            width: "100px",
                          }}
                          onClick={() =>
                            deleteItem(
                              item.id,
                              item.name,
                              item.phone_number,
                              item.email,
                              item.organization_id,
                              item.status
                            )
                          }
                        >
                          Deactive
                        </Button>
                      </TableCell>
                    ) : (
                      <TableCell
                        className="px-0 capitalize"
                        align="center"
                        style={{ width: "10%" }}
                      >
                        <Button
                          name="status"
                          color="success"
                          style={{
                            backgroundColor: "#00aa33",
                            borderColor: "#00aa33",
                            width: "100px",
                          }}
                          onClick={() =>
                            activeItem(
                              item.id,
                              item.name,
                              item.phone_number,
                              item.email,
                              item.organization_id,
                              item.status
                            )
                          }
                        >
                          Active
                        </Button>
                      </TableCell>
                    )}
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>
      {props?.items?.length > 10 && (
        <TablePagination
          style={{
            maxWidth: 400,
            overflowX: "hidden",
            padding: "0px!important",
            display: "contents",
          }}
          className="px-4"
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.items.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page",
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page",
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      )}
    </>
  );
}
