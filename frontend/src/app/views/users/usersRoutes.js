/* eslint-disable no-dupe-keys */
import React from "react";

const UsersRoutes = [
  {
    path: "/user/create",
    component: React.lazy(() => import("./createUser")),
  },
  {
    path: "/user/team",
    component: React.lazy(() => import("./teamMembers/sa")),
  },
  {
    path: "/user/management",
    component: React.lazy(() => import("./managements/sa")),
  },
  {
    path: "/user/admin",
    component: React.lazy(() => import("./admins/sa")),
  },
  {
    path: "/user/participants",
    component: React.lazy(() => import("./participants/sa")),
  },
  {
    path: "/user/profile",
    component: React.lazy(() => import("./profile/sa")),
  },
];

export default UsersRoutes;
