import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/styles";
import FilterDisease from "../publicDashboard/diseaseData/userAccessDisease";

class Dashboard1 extends Component {
  state = {};

  render() {
    return (
      <Fragment>
        <FilterDisease />
      </Fragment>
    );
  }
}

Dashboard1.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  logoutUser: PropTypes.func.isRequired,
  user: state.user,
});

export default withStyles(
  {},
  { withTheme: true }
)(withRouter(connect(mapStateToProps, {})(Dashboard1)));
