/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Grid, Card, Icon, IconButton, Tooltip } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import Axios from "axios";
import myApi from "app/auth/api";
import localStorageService from "app/services/localStorageService";

// const totalAppointment = ""

const styles = theme => ({
  icon: {
    fontSize: "44px",
    opacity: 0.6,
    color: theme.palette.primary.main
  }
});

const StatCards = ({ classes }) => {

  const [salesPersonData, setSalesPersonData] = useState([])
  useEffect(() => {
    Axios.get(myApi + "/intraco/lpg/dashboard/data", {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user') && localStorageService.getItem('auth_user').token
      }
    }).then(response => {
      //  //console.log (response && response.data && response.data.data.prospestWiseTask.map(item => item ))
      //  response && response.data && (
      if (response.data && response.data.data) {
        setSalesPersonData(response.data.data)
      }

    })
  }, false)

  // //console.log(salesPersonData)
  return (


    <Grid container spacing={3} className="mb-3">
      <Grid item xs={12} md={6}>


        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <Icon className={classes.icon}>supervisor_account</Icon>
            <div className="ml-3">
              <font className="text-muted">Total Clients</font>
            </div>
          </div>
          <Tooltip title="View Details" placement="top">
            <IconButton>

              <h6 className="m-0 mt-1 text-primary font-medium">{salesPersonData.totalClients < 10 ? '0' + salesPersonData.totalClients : salesPersonData.totalClients}</h6>
            </IconButton>
          </Tooltip>
        </Card>

      </Grid>
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <Icon className={classes.icon}>alarm_on</Icon>
            <div className="ml-3">
              <font className="text-muted">Total Appointment</font>

            </div>
          </div>
          <Tooltip title="View Details" placement="top">
            <IconButton>
              <h6 className="m-0 mt-1 text-primary font-medium">{salesPersonData.totalAppointment < 10 ? '0' + salesPersonData.totalAppointment : salesPersonData.totalAppointment}</h6>
            </IconButton>
          </Tooltip>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <Icon className={classes.icon}>assignment</Icon>
            <div className="ml-3">
              <font className="text-muted">Pending Task</font>

            </div>
          </div>
          <Tooltip title="View Details" placement="top">
            <IconButton>
              <h6 className="m-0 mt-1 text-primary font-medium">
                {salesPersonData.totalPendingTask < 10 ? '0' + salesPersonData.totalPendingTask : salesPersonData.totalPendingTask}
              </h6>
            </IconButton>
          </Tooltip>
        </Card>
      </Grid>
      {/* <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <Icon className={classes.icon}>shopping_cart</Icon>
            <div className="ml-3">
              <small className="text-muted">{salesPersonData.prospestWiseTaskresponse.map(item => item )}</small>
              <h6 className="m-0 mt-1 text-primary font-medium">305 Orders</h6>
            </div>
          </div>
          <Tooltip title="View Details" placement="top">
            <IconButton>
              <Icon>arrow_right_alt</Icon>
            </IconButton>
          </Tooltip>
        </Card>
      </Grid> */}
    </Grid >
  );
};

export default withStyles(styles, { withTheme: true })(StatCards);
