/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Grid, Icon, IconButton, Tooltip } from "@material-ui/core";
import myApi from "../.../../../../auth/api";
import history from "../../../../history";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "app/redux/actions/UserActions";
import PropTypes from "prop-types";
import Moment from "moment";
import {
  Card,
} from "@material-ui/core";
import Swal from "sweetalert2";
import axios from "axios";
import localStorageService from "app/services/localStorageService";

const MDStatCard = (props) => {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [clients, setClients] = useState([]);
  let [MDStatCardList, setMDStatCardList] = React.useState("");
  const [counts, setCounts] = useState([]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const countToal_appointment = function (toal_appointment) {
    let newCounts = counts
    newCounts.push(toal_appointment);
    setCounts(newCounts);
  };



  const fetchData = React.useCallback(() => {
    // axios({
    //   method: "GET",
    //   url: myApi + "/intraco/lpg/dashboard/data",
    //   headers: {
    //     "x-access-token":
    //       localStorage.getItem("jwt_token") &&
    //       localStorage.getItem("jwt_token"),
    //   },
    // })
    //   .then((response) => {

    //     if (response.data.data) {
    //       // setMDStatCardList(response.data.data);
    //       countToal_appointment(response.data.data.total_appointment);
    //     }
    //     if (
    //       response.data.message &&
    //       response.data.message === "Timeout Login Fast"
    //     ) {
    //       props.logoutUser();
    //     }
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleCompleteTask = (id) => {
    axios
      .post(
        myApi + "/intraco/lpg/task/complete",
        {
          task_id: id,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )
      .then((response) => {

        Swal.fire("Task Complete Successfully !");

        history.push({
          pathname: "/",
        });

      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (

    <Grid container spacing={3} className="mb-3">
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <div className="ml-3">
              <span className="text-15 text-grey ml-1">Total Leads</span>
            </div>
          </div>
          <Tooltip title="View Details" placement="top">
            <h6 className="m-0 mt-1 text-primary font-medium">{MDStatCardList.totalLeads}</h6>

          </Tooltip>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <div className="ml-3">

              <span className="text-15 text-grey ml-1">Total Completed Appointment</span>
            </div>
          </div>
          <Tooltip title="View Details" placement="top">
            <h6 className="m-0 mt-1 text-primary font-medium">{MDStatCardList.totalCompleteAppointment}</h6>

          </Tooltip>
        </Card>
      </Grid>
    </Grid>

  );
};

MDStatCard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  logoutUser: PropTypes.func.isRequired,
});

export default withRouter(
  connect(mapStateToProps, { logoutUser })(MDStatCard)
);

