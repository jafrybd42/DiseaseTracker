/* eslint-disable no-unused-vars */
import { Grid, Icon, IconButton, Tooltip } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import myApi from "../.../../../../auth/api";
import history from "../../../../history";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "app/redux/actions/UserActions";
import PropTypes from "prop-types";

import Moment from "moment";
import {
  Card,
} from "@material-ui/core";
import Swal from "sweetalert2";
import axios from "axios";
import localStorageService from "app/services/localStorageService";

const MDStatCard = (props) => {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [clients, setClients] = useState([]);
  const [MDStatCardList, setMDStatCardList] = useState([]);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const items =
    MDStatCardList &&
    MDStatCardList.map((item, i) => {
      return (
        <tr key={item.id}>
          <td>{i + 1}</td>
          <td>{item.sales_name}</td>
          <td>{item.total_task}</td>
          <td>{item.total_complete_task}</td>
          <td>{item.total_appointment}</td>
          {/* <td>{Moment(item.call_date).format('YYYY-MM-DD')}</td> */}
          <td>{item.total_complete_appointment}</td>
        </tr>
      );
    });

  const fetchData = React.useCallback(() => {
    // axios({
    //   method: "GET",
    //   url: myApi + "/intraco/lpg/dashboard/data",
    //   headers: {
    //     "x-access-token":
    //       localStorage.getItem("jwt_token") &&
    //       localStorage.getItem("jwt_token"),
    //   },
    // })
    //   .then((response) => {
    //     if (response.data.data) {
    //       // setMDStatCardList(response.data.data);
    //     }
    //     if (
    //       response.data.message &&
    //       response.data.message === "Timeout Login Fast"
    //     ) {
    //       props.logoutUser();
    //     }
    //   })
    //   .catch((error) => {
    //   });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleCompleteTask = (id) => {
    // implementation
    axios
      .post(
        myApi + "/intraco/lpg/task/complete",
        {
          task_id: id,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )
      .then((response) => {

        Swal.fire("Task Complete Successfully !");
        history.push({
          pathname: "/",
        });
      })
      .catch((error) => {
      });
  };

  return (

    <Grid container spacing={3} className="mb-3">
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <div className="ml-3">
              <small className="text-muted">Total Task</small>
            </div>
          </div>
          <Tooltip title="View Details" placement="top">
            <h6 className="m-0 mt-1 text-primary font-medium">{MDStatCardList.map(points => points.total_task)
              .reduce((acc, curr) => acc + parseInt(curr, 10), 0)}</h6>

          </Tooltip>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <div className="ml-3">
              <small className="text-muted">Completed Task</small>
            </div>
          </div>
          <Tooltip title="View Details" placement="top">
            <h6 className="m-0 mt-1 text-primary font-medium">{MDStatCardList.map(points => points.total_complete_task)
              .reduce((acc, curr) => acc + parseInt(curr, 10), 0)}</h6>

          </Tooltip>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <div className="ml-3">
              <small className="text-muted">Total Appointment</small>

            </div>
          </div>
          <Tooltip title="View Details" placement="top">
            <h6 className="m-0 mt-1 text-primary font-medium">
              {MDStatCardList.map(points => points.total_appointment)
                .reduce((acc, curr) => acc + parseInt(curr, 10), 0)}
            </h6>
          </Tooltip>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <div className="ml-3">
              <small className="text-muted">Completed Appointment</small>
            </div>
          </div>
          <Tooltip title="View Details" placement="top">
            {/* <IconButton>
              <Icon>arrow_right_alt</Icon>
            </IconButton> */}
            <h6 className="m-0 mt-1 text-primary font-medium">{MDStatCardList.map(points => points.total_complete_appointment)
              .reduce((acc, curr) => acc + parseInt(curr, 10), 0)}</h6>

          </Tooltip>
        </Card>
      </Grid>
    </Grid>

  );
};

MDStatCard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  logoutUser: PropTypes.func.isRequired,
});

export default withRouter(
  connect(mapStateToProps, { logoutUser })(MDStatCard)
);

