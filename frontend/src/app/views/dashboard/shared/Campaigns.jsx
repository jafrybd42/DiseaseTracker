/* eslint-disable no-unused-vars */

import React, { useState, useEffect } from "react";
import myApi from "../../../auth/api";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "app/redux/actions/UserActions";
import PropTypes from "prop-types";

import Moment from "moment";
import {
  Card,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
} from "@material-ui/core";
import Swal from "sweetalert2";
import axios from "axios";
import localStorageService from "app/services/localStorageService";
const Campaigns = (props) => {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [clients, setClients] = useState([]);
  var [element, setElement] = useState([]);
  let [CampaignsList, setCampaignsList] = useState([]);
  var [a, b, c, d] = useState([]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };



  const fetchData = React.useCallback(() => {
    // axios({
    //   method: "GET",
    //   url: myApi + "/intraco/lpg/dashboard/data",
    //   headers: {
    //     "x-access-token":
    //       localStorage.getItem("jwt_token") &&
    //       localStorage.getItem("jwt_token"),
    //   },
    // })
    //   .then((response) => {
    //     if (response.data.data) {
    //       // setCampaignsList(response.data.data);
    //     }
    //     if (
    //       response.data.message &&
    //       response.data.message === "Timeout Login Fast"
    //     ) {
    //       props.logoutUser();

    //     }
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  React.useEffect(() => {
    fetchData();
  }, [fetchData]);


  console.log(CampaignsList &&
    CampaignsList.prospestWiseTask &&
    Object.keys(CampaignsList.prospestWiseTask[0]))
  return (
    <Card elevation={3} className="pt-5 mb-6">

      <div
      // className="overflow-auto"
      >
        <Table className="product-table">
          {CampaignsList.length === 0 ? (
            <div
              className="alert alert-info"
              role="alert"
              style={{ width: "100%", background: "white", border: "white" }}
            >
              No more Prospect Types !
            </div>
          ) : (
            <TableHead>
              <TableRow>
                <TableCell className="px-6" colSpan={3} align="left">
                  Prospect Types
                </TableCell>
                <TableCell className="px-0" colSpan={2} align="center">
                  Total
                </TableCell>
              </TableRow>
            </TableHead>
          )}

          {CampaignsList &&
            CampaignsList.prospestWiseTask &&
            Object.keys(CampaignsList.prospestWiseTask[0])
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((template_name, i) => {
                return (

                  <TableBody>
                    <TableRow key={i}>
                      <TableCell align="center" className="px-6" colSpan={3}>
                        <div className="flex items-center">

                          <div className="py-4 pr-4"><span className="material-icons MuiIcon-root MuiIcon-fontSizeLarge" aria-hidden="true" title="navigate_next">navigate_next</span></div>

                          <p className="m-0 ml-8">
                            {/* {([total, prospectType] = template_name.split("_")) && total ? prospectType :  total+' '+ prospectType } */}
                            {([a, b, c, d] = template_name.split("_")) && (a ? a : '') + ' ' + (b ? b : '') + ' ' + (c ? c : '') + ' ' + (d ? d : '')}
                          </p>
                        </div>
                      </TableCell>
                      <TableCell align="center" className="px-0" colSpan={2}>
                        {CampaignsList.prospestWiseTask[0][template_name]}
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )
              })}
        </Table>

        <TablePagination
          className="px-4"
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={CampaignsList &&
            CampaignsList.prospestWiseTask &&
            Object.keys(CampaignsList.prospestWiseTask[0]).map(i => i.length).length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page",
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page",
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </div>
    </Card>
  );
};

Campaigns.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  logoutUser: PropTypes.func.isRequired,
});

export default withRouter(
  connect(mapStateToProps, { logoutUser })(Campaigns)
);

