import React from "react";
import { authRoles } from "../../auth/authRoles";

const dashboardRoutes = [
  {
    path: "/dashboard/analytics",
    component: React.lazy(() => import("./Analytics")),
    auth: authRoles.admin
  },
  {
    path: "/dashboard/subAdmin",
    component: React.lazy(() => import("./subAdmin")),
    auth: authRoles.editor
  },
  {
    path: "/dashboard/sales",
    component: React.lazy(() => import("./salesPerson")),
    auth: authRoles.guest
  },
  {
    path: "/dashboard/mt/admin",
    component: React.lazy(() => import("./md")),
    auth: authRoles.mtAdmin
  },
  {
    path: "/dashboard/mt/admin",
    component: React.lazy(() => import("./mtAdmin")),
    auth: authRoles.sa
  }
];

export default dashboardRoutes;
