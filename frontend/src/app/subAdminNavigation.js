export const subAdminNavigation = [
  {
    name: "Dashboard",
    path: "/dashboard/subAdmin",
    icon: "dashboard",
  },
  {
    name: "Sales Person",
    icon: "person_outline",
    children: [
      {
        name: "Create Sales Person",
        path: "/salesPerson/create",
        iconText: "E",
      },
      {
        name: "Active Sales Person",
        path: "/salesPerson/edit",
        iconText: "F",
      },
      {
        name: "Deactive Sales Person",
        path: "/salesPerson/deactiveList",
        iconText: "F",
      },
    ],
  },

  {
    name: "Company",
    icon: "person_outline",
    children: [
      {
        name: "Create Company",
        path: "/company/create",
        iconText: "E",
      },
      {
        name: "Manage Company",
        path: "/company/edit",
        iconText: "F",
      },
      {
        name: "Add New Sister Concern",
        path: "/sisterconcern/create",
        iconText: "E",
      },
      {
        name: "Manage Sister Concern",
        path: "/sisterconcern/edit",
        iconText: "F",
      },
    ],
  },

  {
    name: "Client",
    icon: "person_outline",
    children: [
      {
        name: "Client List",
        path: "/client/list",
        iconText: "F",
      },
      {
        name: "Reassign Client",
        path: "/client/exchange",
        iconText: "H",
      },
    ],
  },
  {
    name: "Task",
    icon: "queue",
    children: [
      {
        name: "Task List",
        path: "/task/search",
        iconText: "F",
      },
      {
        name: "Exchange Task",
        path: "/task/exchange",
        iconText: "F",
      },
    ],
  },
  {
    name: "Appointment",
    icon: "access_time",
    children: [
      {
        name: "Search Appointment",
        path: "/appointment/search/active",
        iconText: "F",
      },
      {
        name: "Deactive Appointments",
        path: "/appointment/deactive",
        iconText: "F",
      },
    ],
  },

  {
    name: "Client Type",
    icon: "person_outline",
    children: [
      {
        name: "Create Client Type",
        path: "/clientType/create",
        iconText: "G",
      },
      {
        name: "Manage Client Type",
        path: "/clientType/edit",
        iconText: "H",
      },
    ],
  },
  {
    name: "Discussion Type",
    icon: "insert_comment",
    children: [
      {
        name: "Create Discussion Type",
        path: "/discussionType/create",
        iconText: "G",
      },
      {
        name: "Manage Discussion Type",
        path: "/discussionType/edit",
        iconText: "H",
      },
    ],
  },
  {
    name: "Prospect Type",
    icon: "help_outline",
    children: [
      {
        name: "Create Prospect Type",
        path: "/prospectType/create",
        iconText: "G",
      },
      {
        name: "Manage Prospect Type",
        path: "/prospectType/edit",
        iconText: "H",
      },
    ],
  },
  {
    name: "Vehicle Type",
    icon: "directions_car",
    children: [
      {
        name: "Create Vehicle Type",
        path: "/vehicleType/create",
        iconText: "G",
      },
      {
        name: "Manage Vehicle Type",
        path: "/vehicleType/edit",
        iconText: "H",
      },
    ],
  },

  {
    name: "Leads",
    icon: "assignment",
    children: [
      {
        name: "Create Lead",
        path: "/lead/create",
        iconText: "G",
      },
      {
        name: "Manage Leads",
        path: "/lead/edit",
        iconText: "H",
      },
    ],
  },
];
