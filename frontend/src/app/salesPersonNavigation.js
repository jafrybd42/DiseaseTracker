export const salesPersonNavigation = [
  {
    name: "Dashboard",
    path: "/dashboard/sales",
    icon: "dashboard",
  },
  {
    name: "Corporate Search",
    path: "/search",
    icon: "search",
  },
  {
    name: "Client",
    icon: "person_outline",
    children: [
      {
        name: "Create Individual Client",
        path: "/client/create",
        iconText: "E",
      },
      {
        name: "Manage Individual Client",
        path: "/client/edit",
        iconText: "F",
      },
      {
        name: "Manage Corporate Client",
        path: "/client/corporate",
        iconText: "F",
      },
    ],
  },
  {
    name: "Task",
    icon: "business_center",
    children: [
      {
        name: "Create Task",
        path: "/task/create",
        iconText: "G",
      },
      {
        name: "Manage Task",
        path: "/task/filter",
        iconText: "H",
      },
    ],
  },
  {
    name: "Appointment",
    icon: "contact_phone",
    children: [
      {
        name: "Create Appointment",
        path: "/appointment/create",
        iconText: "G",
      },
      {
        name: "Manage Appointment",
        path: "/appointment/manage",
        iconText: "H",
      },
    ],
  },
  {
    name: "Reminder",
    icon: "assistant",
    children: [
      {
        name: "Manage Reminders",
        path: "/reminder/manage",
        iconText: "G",
      }
    ],
  },
];
