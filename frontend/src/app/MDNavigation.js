export const MDNavigation = [
  {
    name: "Dashboard",
    path: "/dashboard/mt/admin",
    icon: "dashboard",
  },
  {
    name: "Appointment Report",
    path: "/report/appointment",
    icon: "dashboard",
  },
  {
    name: "Task Report",
    path: "/report/task",
    icon: "dashboard",
  },
];
