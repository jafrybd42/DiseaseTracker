import jwtAuthService from "../../services/jwtAuthService";
import FirebaseAuthService from "../../services/firebase/firebaseAuthService";
import { setUserData } from "./UserActions";
import { getNavigationByUser } from "./NavigationAction";
import { setLayoutSettings } from "./LayoutActions"

import history from "history.js";

export const LOGIN_ERROR = "LOGIN_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_LOADING = "LOGIN_LOADING";
export const RESET_PASSWORD = "RESET_PASSWORD";

export function loginPhonePass({ email, password }) {
  return dispatch => {
    dispatch({
      type: LOGIN_LOADING
    });

    jwtAuthService
      .loginPhonePass(email, password)
      .then(({ user, path }) => {

        /*
          Sets the user data, session, and user in local storage
        */
        dispatch(setUserData(user));
        jwtAuthService.setSession(user.token)
        jwtAuthService.setUser(user)

        /*
          Dispatches the success and user redirect path
        */
        dispatch({
          type: LOGIN_SUCCESS,
          successRedirect: path
        });

        /*
          Dispatches new layout
        */
        dispatch(setLayoutSettings(null, path))

      })
      .catch(error => {
        //console.log(error)
        return dispatch({
          type: LOGIN_ERROR,
          payload: error
        });
      });
  };
}

export function loginPin({ pin }) {
  return dispatch => {
    dispatch({
      type: LOGIN_LOADING
    });

    jwtAuthService
      .loginPin(pin)
      .then(({ user, path }) => {

        /*
          Sets the user data, session, and user in local storage
        */
        dispatch(setUserData(user));
        jwtAuthService.setSession(user.token)
        jwtAuthService.setUser(user)

        /*
          Dispatches the success and user redirect path
        */
        dispatch({
          type: LOGIN_SUCCESS,
          successRedirect: path
        });

        /*
          Dispatches new layout
        */
        dispatch(setLayoutSettings(null, path))

      })
      .catch(error => {
        //console.log(error)
        return dispatch({
          type: LOGIN_ERROR,
          payload: error
        });
      });
  };
}


export function loginToken() {
  return dispatch => {
    dispatch({
      type: LOGIN_LOADING
    });

    let redirectPath;

    jwtAuthService.loginWithToken()
      .then(({ user, path }) => {

        dispatch(setUserData(JSON.parse(user)))

        redirectPath = path;

        getNavigationByUser();
      })
      .then(() => {

        dispatch({
          type: LOGIN_SUCCESS,
          successRedirect: redirectPath
        });

      })
      .catch((e) => {

        dispatch({
          type: LOGIN_ERROR,
          payload: "No token"
        })
        //console.log('No token')
      })
  }
}

export function resetPassword({ email }) {
  return dispatch => {
    dispatch({
      payload: email,
      type: RESET_PASSWORD
    });
  };
}

export function firebaseLoginEmailPassword({ email, password }) {
  return dispatch => {
    FirebaseAuthService.signInWithEmailAndPassword(email, password)
      .then(user => {
        if (user) {
          dispatch(
            setUserData({
              userId: "1",
              role: "ADMIN",
              displayName: "Watson Joyce",
              email: "watsonjoyce@gmail.com",
              photoURL: "/assets/images/face-7.jpg",
              age: 25,
              token: "faslkhfh423oiu4h4kj432rkj23h432u49ufjaklj423h4jkhkjh",
              ...user
            })
          );

          history.push({
            pathname: "/"
          });

          return dispatch({
            type: LOGIN_SUCCESS
          });
        } else {
          return dispatch({
            type: LOGIN_ERROR,
            payload: "Login Failed"
          });
        }
      })
      .catch(error => {
        return dispatch({
          type: LOGIN_ERROR,
          payload: error
        });
      });
  };
}
