import React from "react";
import { withStyles, ThemeProvider } from "@material-ui/core/styles";
import { Button, Toolbar, AppBar } from "@material-ui/core";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Block } from "@material-ui/icons";

const Footer = ({ theme, settings }) => {
  const footerTheme = settings.themes[settings.footer.theme] || theme;
  return (
    <>
      <ThemeProvider
      // style={{background : 'red!important'}}
      // theme={footerTheme}
      >
        <AppBar
          color="btn btn-light"
          style={{
            paddingTop: "10px",
            width: "100%",
            left: "auto",
            bottom: 0,
            right: 0,
            position: "fixed",
            top: "auto",
            zIndex: "5",
          }}
        >
          <Toolbar className="footer flex items-center">
            {/* <p className="m-0" style={{ width: '15%' }}> */}
            {/* Powered by <b>Medina Tech</b> */}
            {/* </p> */}
            <p
              className="m-0"
              style={{
                textAlign: "center",
                width: "100%",
                display: "block",
                margin: "auto",
              }}
            >
              Developed by <b>Medina Tech</b>
            </p>
          </Toolbar>
        </AppBar>
      </ThemeProvider>

      {/* <div className="flex items-center container w-full" style={{ background: 'white!important' }}> */}
      {/* <a
              href="https://ui-lib.com/downloads/matx-react-material-design-admin-template/"
              className="mr-2"
            >
              <Button variant="contained">Download Matx</Button>
            </a>
            <a href="https://ui-lib.com/downloads/matx-pro-react-material-design-admin-template/">
              <Button variant="contained" color="secondary">
                Get MatX Pro
              </Button>
            </a> */}
      {/* <span className="m-auto"></span>
        <p className="m-0" style={{ position: 'relative', right: '10px' }}>
          Powered by <a href="https://medinatech.co">Medina Tech</a>
        </p>
      </div> */}
    </>
  );
};

Footer.propTypes = {
  settings: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  settings: state.layout.settings,
});

export default withStyles(
  {},
  { withTheme: true }
)(connect(mapStateToProps, {})(Footer));
