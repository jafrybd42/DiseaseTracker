const Layout1Settings = {
  leftSidebar: {
    show: false, //it was true
    mode: 'close', // full, close, compact, mobile,
    theme: 'slateDark1', // View all valid theme colors inside MatxTheme/themeColors.js
    // bgOpacity: .96, // 0 ~ 1
    bgImgURL: '/assets/images/sidebar/sidebar-bg-dark.jpg'
  },
  topbar: {
    show: true,
    fixed: true,
    theme: 'slateDark1' // View all valid theme colors inside MatxTheme/themeColors.js
  }
}

export default Layout1Settings;