/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { Redirect, withRouter } from "react-router-dom";
import { Icon, IconButton, MenuItem } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { logoutUser } from "app/redux/actions/UserActions";
import PropTypes from "prop-types";
import { MatxMenu, MatxSearchBox } from "matx";
import { isMdScreen, classList } from "utils";
import NotificationBar from "../SharedCompoents/NotificationBar";
import { Link } from "react-router-dom";
import ShoppingCart from "../SharedCompoents/ShoppingCart";
import history from "../../../history";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";

import clsx from "clsx";
import { Box } from "@material-ui/core";
import projectLogo from "../../../assets/img/logo.svg";

const styles = (theme) => ({
  topbar: {
    "& .topbar-hold": {
      backgroundColor: "#373737",
      // theme.palette.primary.main,
      height: "80px",
      "&.fixed": {
        boxShadow: theme.shadows[8],
        height: "64px",
      },
    },
  },
  menuItem: {
    display: "flex",
    alignItems: "center",
    minWidth: 185,
  },
});

class Layout1Topbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSubmitted: false,
      isOrganization: false,
    };
  }
  // state = {};
  updateSidebarMode = (sidebarSettings) => {
    let { settings, setLayoutSettings } = this.props;

    setLayoutSettings({
      ...settings,
      layout1Settings: {
        ...settings.layout1Settings,
        leftSidebar: {
          ...settings.layout1Settings.leftSidebar,
          ...sidebarSettings,
        },
      },
    });
  };

  handleSidebarToggle = () => {
    let { settings } = this.props;
    let { layout1Settings } = settings;

    let mode;
    if (isMdScreen()) {
      mode = layout1Settings.leftSidebar.mode === "close" ? "mobile" : "close";
    } else {
      mode = layout1Settings.leftSidebar.mode === "full" ? "close" : "full";
    }
    this.updateSidebarMode({ mode });
  };
  componentDidMount() {
    if (localStorage.getItem("jwt_token") === null) {
      this.props.logoutUser();
    }
    window.location.href.endsWith("analytics")
      ? this.setState({
          isSubmitted: true,
        })
      : this.setState({
          isSubmitted: false,
        });

    window.location.href.endsWith("/organization/create")
      ? this.setState({
          isOrganization: true,
        })
      : this.setState({
          isOrganization: false,
        });
  }
  handleSignOut = () => {
    Swal.fire({
      title: "Are you sure?",
      // text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, proceed!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        this.props.logoutUser();
      }
    });
  };
  handleHelpLine = () => {
    Swal.fire({
      icon: "info",
      title: "Available Soon",
      showConfirmButton: false,
    });
  };

  handleAdminProfile = () => {
    history.push({
      pathname: "/profile/admin",
    });
  };
  handleAdminUpdateProfile = () => {
    history.push({
      pathname: "/profile/admin/edit",
    });
  };
  handleAdminChangePass = () => {
    history.push({
      pathname: "/password/edit",
    });
  };

  handleSalesProfile = () => {
    history.push({
      pathname: "/account/profile",
    });
  };
  handleSalesUpdateProfile = () => {
    history.push({
      pathname: "/profile/salesperson/edit",
    });
  };
  handleSalesForgetPass = () => {
    history.push({
      pathname: "/setting/forgetpassword",
    });
  };
  handleSalesPassChange = () => {
    history.push({
      pathname: "/salesperson/passwordChange",
    });
  };

  handleSubAdminProfile = () => {
    history.push({
      pathname: "/profile/subadmin",
    });
  };
  handleSubAdminUpdateProfile = () => {
    history.push({
      pathname: "/profile/subadmin/edit",
    });
  };
  handleSubAdminChangePass = () => {
    history.push({
      pathname: "/subAdmin/passwordChange",
    });
  };
  handleProfile = () => {
    history.push({
      pathname: "/user/profile",
    });
  };
  handleMembership = () => {
    history.push({
      pathname: "/membership",
    });
  };
  getHelp = () => {
    Swal.fire({
      icon: "info",
      title: "Under Construction",
      showConfirmButton: false,
      timer: 1000,
    });
  };

  render() {
    let { classes, fixed, user } = this.props;

    return (
      <div className={`topbar ${classes.topbar}`}>
        <div className={classList({ "topbar-hold": true, fixed: fixed })}>
          <div className="flex justify-between items-center h-full">
            <div className="flex">
              {/* logo */}

              <Link
                to="/"
                className="header-logo-wrapper-link"
                style={{ backgroundColor: "transparent", marginRight: "10px" }}
              >
                <IconButton
                  color="primary"
                  size="small"
                  className="header-logo-wrapper-btn"
                >
                  <img
                    className="app-header-logo-img"
                    alt="Bangladesh Disease Tracking"
                    src={projectLogo}
                  />
                </IconButton>
              </Link>
              <Link to="/" className="logoUserPanel">
                Bangladesh Disease Tracking
              </Link>

              {/* <IconButton
                onClick={this.handleSidebarToggle}
                className="hide-on-pc"
              >
                <Icon>menu</Icon>
              </IconButton> */}
            </div>
            <div className="hide-on-mobile">
              {window.location.href.match("/dashboard") ? (
                <Link to="/" className="middleTxt" style={{ opacity: "1" }}>
                  Dashboard
                </Link>
              ) : (
                <Link to="/" className="middleTxt">
                  Dashboard
                </Link>
              )}
              {(localStorageService.getItem("auth_user")?.role_id === 1 ||
                localStorageService.getItem("auth_user")?.role_id === 2 ||
                localStorageService.getItem("auth_user")?.role_id === 3 ||
                localStorageService.getItem("auth_user")?.role_id === 5) &&
                (window.location.href.endsWith("/data/filter") ? (
                  <Link
                    to="/data/filter"
                    className="middleTxt"
                    style={{ opacity: "1" }}
                  >
                    Data Table
                  </Link>
                ) : (
                  <Link to="/data/filter" className="middleTxt">
                    Data Table
                  </Link>
                ))}
              {window.location.href.match("/uploads/history") ? (
                <Link
                  to="/uploads/history"
                  className="middleTxt"
                  style={{ opacity: "1" }}
                >
                  Upload History
                </Link>
              ) : (
                <Link to="/uploads/history" className="middleTxt">
                  Upload History
                </Link>
              )}
              {(localStorageService.getItem("auth_user")?.role_id === 1 ||
                localStorageService.getItem("auth_user")?.role_id === 5) &&
                (window.location.href.match("/user") ? (
                  <Link
                    to="/user/team"
                    className="middleTxt"
                    style={{ opacity: "1" }}
                  >
                    Members
                  </Link>
                ) : (
                  <Link to="/user/team" className="middleTxt">
                    Members
                  </Link>
                ))}

              {localStorageService.getItem("auth_user")?.role_id === 1 &&
                (window.location.href.endsWith("/organization/create") ||
                window.location.href.endsWith("/organization/list") ? (
                  <Link
                    to="/organization/list"
                    className="middleTxt"
                    style={{ opacity: "1" }}
                  >
                    Organizations
                  </Link>
                ) : (
                  <Link to="/organization/list" className="middleTxt">
                    Organizations
                  </Link>
                ))}

              {(localStorageService.getItem("auth_user")?.role_id === 1 ||
                localStorageService.getItem("auth_user")?.role_id === 2 ||
                localStorageService.getItem("auth_user")?.role_id === 3 ||
                localStorageService.getItem("auth_user")?.role_id === 5) &&
                (window.location.href.match("/report") ? (
                  <Link
                    to="/report/list"
                    className="middleTxt"
                    style={{ opacity: "1" }}
                  >
                    Reports
                  </Link>
                ) : (
                  <Link to="/report/list" className="middleTxt">
                    Reports
                  </Link>
                ))}
            </div>
            <div className="flex items-center" area-role="getHelp">
              <MenuItem className="helpText" onClick={this.getHelp}>
                <font color="white">Get Help</font>
              </MenuItem>{" "}
              <MatxMenu
                menuButton={
                  <img
                    className="mx-2 align-middle circular-image-small cursor-pointer"
                    src={user.photoURL}
                    alt="user"
                  />
                }
              >
                <MenuItem
                  onClick={this.handleProfile}
                  className={classes.menuItem}
                >
                  <Icon> person </Icon>
                  <span className="pl-4"> User Profile </span>
                </MenuItem>
                <MenuItem
                  onClick={this.handleMembership}
                  className={classes.menuItem}
                >
                  <Icon> loyalty </Icon>
                  <span className="pl-4"> Membership Plan</span>
                </MenuItem>
                <MenuItem
                  onClick={this.handleSignOut}
                  className={classes.menuItem}
                >
                  <Icon> power_settings_new </Icon>
                  <span className="pl-4"> Logout </span>
                </MenuItem>
              </MatxMenu>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Layout1Topbar.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: state.layout.settings,
});

export default withStyles(styles, { withTheme: true })(
  withRouter(
    connect(mapStateToProps, { setLayoutSettings, logoutUser })(Layout1Topbar)
  )
);
