import React, { useEffect } from "react";
import { MatxLayouts } from "./index";
import PropTypes from "prop-types";
import { withRouter, useLocation } from "react-router-dom";
import { connect } from "react-redux";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { merge } from "lodash";
import { isMdScreen } from "utils";
import { MatxSuspense } from "matx";

const MatxLayoutSFC = props => {

  /*
    Gets the current location for router. This is used to trigger 
    a layout change if the pathname changes.
  */
  let location = useLocation();

  /*
    Gets props from redux
  */
  const {
    settings,
    setLayoutSettings,
  } = props;


  useEffect(() => {
    if (!props.login.loading) {      
      let mode = isMdScreen() ? "close" : "full";

      setLayoutSettings(null, props.location.pathname, mode);
    }
  }, [location.pathname, props.location.pathname, props.login.loading, setLayoutSettings])

  useEffect(() => {
    /*
      Need to have this definition inside useEffect to stop infinite rerender
    */
    const listenWindowResize = () => {
      if (settings.layout1Settings.leftSidebar.show) {
        let mode = isMdScreen() ? "close" : "full";

        setLayoutSettings(
          merge({}, settings, { layout1Settings: { leftSidebar: { mode } } })
        );
      }
    };

    if (window) {
      // LISTEN WINDOW RESIZE
      window.addEventListener("resize", listenWindowResize);
    }
    return () => {
      if (window) {
        window.removeEventListener("resize", listenWindowResize);
      }
    };
  }, [settings, setLayoutSettings, props.location.pathname, props.login.loading]);

  const Layout = MatxLayouts[settings.activeLayout];

  return (
    <MatxSuspense>
      <Layout {...props} />
    </MatxSuspense>
  );
};

const mapStateToProps = state => ({
  setLayoutSettings: PropTypes.func.isRequired,
  settings: state.layout.settings,
  defaultSettings: state.layout.defaultSettings,
  login: state.login,
});

export default withRouter(
  connect(mapStateToProps, { setLayoutSettings })(
    MatxLayoutSFC
  )
);
