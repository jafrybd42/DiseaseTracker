import React from "react";
import { Redirect } from "react-router-dom";
import dashboardRoutes from "./views/dashboard/DashboardRoutes";
import sessionRoutes from "./views/sessions/SessionRoutes";

//new
import publicDashboardRoutes from './views/publicDashboard/publicDashboardRoutes'
import OrganizationRoutes from "./views/organization/organizationRoutes";
import UsersRoutes from "./views/users/usersRoutes";
import DataManagementRoutes from "./views/datatManagement/dataManagementRoutes";
import MembershipRoutes from "./views/package/packageRoutes";
import UploadHistoryRoutes from "./views/uploadHistory/uploadHistoryRoutes";
import ReportRoutes from './views/reports/reportRoutes'
const redirectRoute =
  [
    {

      path: "/",
      exact: true,
      component: () => <Redirect to="/signin" />

    },
    {

      path: "/public",
      exact: true,
      component: () => <Redirect to="/public/dashboard" />

    }
  ]


const errorRoute = [
  {
    component: () => <Redirect to="/session/404" />
  }
];

const routes = [
  ...sessionRoutes,
  ...dashboardRoutes,
  ...redirectRoute,

  //new
  ...publicDashboardRoutes,
  ...OrganizationRoutes,
  ...UsersRoutes,
  ...DataManagementRoutes,
  ...MembershipRoutes,
  ...UploadHistoryRoutes,
  ...ReportRoutes,
  ...errorRoute
];

export default routes;
